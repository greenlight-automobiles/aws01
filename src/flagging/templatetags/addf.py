from django import template

register = template.Library()

@register.filter(name='addf')
def addf(value, arg):
    """Adds the arg to the value."""
    return float(value) + float(arg)
