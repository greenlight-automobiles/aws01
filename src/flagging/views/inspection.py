'''
Created on Jan 1, 2016

@author: mjnasir
'''
import json
from django.conf import settings
from django.core import serializers
from django.http.response import JsonResponse, HttpResponse
from django.shortcuts import render
from django.template.loader import render_to_string, get_template
from django.db.models import Sum
from xhtml2pdf import pisa

import cStringIO as StringIO
from dealership.models import RoInspection, RO, InspectionCategories, \
    InspectionPackage
from flagging.services.RoServices import RoServices
from dealership.models import Appointment, CustomerVehicle


def inspection(request):

    params = {}
    ro = RoServices(request.session["dealer_id"])
    dealer_id = request.session.get("dealer_id")
    #                 ro = RO.objects.filter(ro_number=requestDict.get("roNumber"))
    if request.method == "POST":
        _request = request.POST
        ro.processInspectionFields(request)
        from dealership.factories import DealerShipServicesFactory
        dealer_factory = DealerShipServicesFactory()
        notificationservice = dealer_factory.get_instance("notification")

        appt = Appointment.objects.get(ro__ro_number=_request.get("ro_number", ""))
        customer_name = appt.customer.get_name_firstlast()
        customer_vehicle = "Undefined"
        cv = CustomerVehicle.objects.get(id=appt.vehicle.id)
        if cv:
            customer_vehicle = "{}, {}".format(cv.vehicle.make, cv.vehicle.model)
        message = "Inspection Completed for '{}' with Vehicle '{}'".format(customer_name, customer_vehicle)

        recipients = []
        sendtoadvisor = (_request.get("sendtoadvisor", "") == "sendtoadvisor")
        if sendtoadvisor:
            # Advisor is to be notified
            if appt.advisor:
                recipients.append(appt.advisor)

            if appt.ro.inspector:
                recipients.append(appt.ro.inspector)
                notificationservice.send_live_notification(sender=request.user, dealer=appt.dealer,
                                                           recipients=recipients,
                                                           message=message)
    else:
        _request = request.GET
        try:
            appt = Appointment.objects.get(ro__ro_number=_request.get("ro_number", ""))
        except:
            appt = None
            pass

    ro_number = _request.get("ro_number","")
    package = _request.get("package", None)
    params["ro_number"]= ro_number
    params["roID"]= ro.getROId(ro_number)
    params["name"] = "No Inspector Found"
    if appt:
        params["default_rate"] = appt.dealer.technician_default_hourly_rate
        if appt.ro.inspector:
            inspector = appt.ro.inspector
            params["name"] = "%s, %s" % (inspector.last_name, inspector.first_name)

    params["packages"] = ro.get_all_packages(ro_number)
    if params["packages"] and package is None:
        package = params["packages"]['packages'][0]['package']
    params["selected_package"] = package

    params["category_items"] = ro.get_inspection_data(ro_number,package,dealer_id)
    params["summary"] = ro.getSummaryDetailsByRoNumber(ro_number,package)
    params.update(params["summary"].aggregate(Sum('laborcost')))
    params.update(params["summary"].aggregate(Sum('partscost')))
    params.update(params["summary"].aggregate(Sum('laborhours')))
    laborcost_sum = params['laborcost__sum']
    if laborcost_sum is None:
        laborcost_sum = 0.0
    partscost_sum = params['partscost__sum']
    if partscost_sum is None:
        partscost_sum = 0.0
    params["recommendation_total"] = partscost_sum + laborcost_sum
    params["roDetails"] = ro.getROdetails(ro_number)
    response = render(request,"flagging_app/inspection.html",params)
    return response


def get_results_summary_pdf(request):

    def fetch_resources(uri, rel):
        import os
        path = os.path.join(settings.MEDIA_ROOT, uri.replace(settings.MEDIA_URL, ""))

        return path

    try:
        ro_number = request.GET.get("roNumber")
        package = request.GET.get("package",None)
        roService = RoServices(request.session["dealer_id"])
        summary = roService.getSummaryDetailsByRoNumber(ro_number,package)
        app_details = roService.getROdetails(ro_number)
        context = dict()
        context["result_summary"] = summary
        context["app_details"] = app_details
        context.update(summary.aggregate(Sum('laborcost')))
        context.update(summary.aggregate(Sum('partscost')))
        context["recommendation_total"] = context['laborcost__sum']+context['partscost__sum']

        template = get_template("flagging_app/inspection_pdf.html")
        url = settings.SITE_MAIN_URL
        html  = template.render(context)
        result = StringIO.StringIO()
        pdf = pisa.pisaDocument(StringIO.StringIO(html.encode("ISO-8859-1")), result, link_callback=fetch_resources)
        if not pdf.err:
            return HttpResponse(result.getvalue(), content_type='application/pdf')
    except Exception as e:
        return HttpResponse('We had some errors while creating the PFD')
    return HttpResponse('We had some errors while creating the PFD')


def get_results_summary(request):
    params = {}
    if "roNumber" in request.GET:
        roNumber = request.GET.get("roNumber")
        params["summary"] = RoServices().getSummaryDetailsByRoNumber(roNumber)
    return render(request,"flagging_app/result_summary.html",params)

def add_inspection_record(request):

    description = request.GET.get("description")
    type = request.GET.get("type")
    roNumber = request.GET.get("roNumber")
    ro = RO.objects.get(ro_number = roNumber)
    inspectionCategory = InspectionCategories()
    inspectionCategory.ro = ro
    inspectionCategory.description = description
    inspectionCategory.type = type
    inspectionCategory.save()
    params = {"id" : inspectionCategory.id,
              "type" : inspectionCategory.type,
              "description": inspectionCategory.description,
              "roNumber" : roNumber
              }
    return render(request,"flagging_app/add_inspection.html",params)

def inspection_test(request):
    dealer_id = request.session["dealer_id"]
    ro_number = request.GET.get("ro_number")
    package = request.GET.get("package")
    categories_items_dict = RoServices().get_inspection_data(ro_number,package,dealer_id)
    return render(request,"flagging_app/inspection.html",{"category_items":categories_items_dict})
