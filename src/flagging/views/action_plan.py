import os
import json


from django.core.urlresolvers import reverse
from django.http import HttpResponse
from django.http import JsonResponse
from django.shortcuts import render
from django.template import Context, Template
import pdfcrowd
from django.db.models import Sum
from django.shortcuts import get_object_or_404
from django.http import Http404

from customer.services.userservices import CUserService
from dealership.models import Appointment, Dealer
from dealership.services import userservices
from dealership.services.notificationservice import NotificationService
from flagging.services.RoServices import RoServices
from oauth2client.client import SETTINGS
from django.conf import settings
from dealership.models import *
from django.conf import settings



def action_plan(request):
    params ={}
    if request.method == "GET":
        if "roNumber" in request.GET:
            roNumber = request.GET.get("roNumber")

            ro = RoServices(request.session["dealer_id"])
            params['service'] = ro.getCustomerServiceRequest(roNumber)
            params['summary'] = ro.getSummaryDetailsByRoNumber(roNumber)
            params.update(params["summary"].aggregate(Sum('laborcost')))
            params.update(params["summary"].aggregate(Sum('partscost')))
            params.update(params["summary"].aggregate(Sum('laborhours')))
            laborcost_sum = params['laborcost__sum']
            if laborcost_sum is None:
                laborcost_sum = 0.0
            partscost_sum = params['partscost__sum']
            if partscost_sum is None:
                partscost_sum = 0.0
            params["recommendation_total"] = partscost_sum + laborcost_sum

            params['roDetails'] = ro.getROdetails(roNumber)
            params["roID"] = ro.getROId(roNumber)
            params['color'] = ro.getColorForRO(roNumber)
            params['roNumber'] = roNumber
            params['ro_url_hash'] = ro.getROHash(roNumber)
            params['recommendations'] = ro.getInspectionRecommendationSummary(roNumber)
            params["inspection"] = ro.getCustomerInspectionRecommendation(roNumber)
            params["action_plan"] = "true"
    return render(request,"flagging_app/action_plan.html",params)


def generate_pdf_view_debug(request):
    # import pdfkit
    from xhtml2pdf import pisa
    import cStringIO as StringIO

    try:
        # create an API client instance
        # client = pdfcrowd.Client("mjnasir", "a90dfc17a16777af1087a5b00e88b5c2")

        # convert a web page and store the generated PDF to a variable
        ro_number = request.GET.get("ro_number")
        dealer_id = request.session["dealer_id"]

        # url = request.GET.get("url")
        #        url_str = "https://greenlightautomotive.com/app/flagger/action_plan_pdf/?roNumber=" +str(ro_number)+"&dealer_code=infiniti-0134&dealer_id="+str(dealer_id)

        # url_str = settings.SITE_MAIN_URL+ reverse("flagging:action_plan_pdf") + "?roNumber=" +str(ro_number)+"&dealer_id="+str(dealer_id)
        # print url_str
        # pdf = pdfkit.from_url(url_str, False)
        #         render(request,"flagging_app/action_plan_pdf")
        # set HTTP response headers
        # response = HttpResponse(content_type="application/pdf")
        # response["Cache-Control"] = "max-age=0"
        # response["Accept-Ranges"] = "none"
        # response["Content-Disposition"] = "attachment; filename=action_plan.pdf"

        # send the generated PDF
        # response.write(pdf)
        # return response

        # def fetch_resources(uri, rel):
        #     """
        #     Callback to allow pisa/reportlab to retrieve Images,Stylesheets, etc.
        #     `uri` is the href attribute from the html link element.
        #     `rel` gives a relative path, but it's not used here.

        #     """
        #     if settings.MEDIA_URL in uri:
        #         path = os.path.join(settings.MEDIA_ROOT, uri.replace(settings.MEDIA_URL, ""))
        #     elif settings.STATIC_URL in uri:
        #         path = os.path.join(rel, uri[1:])

        #     return path

        return generate_action_view(request, ro_number, dealer_id, template_debug=True)

    except Exception as e:
        return HttpResponse('PDF could not be generated')

def generate_pdf_view(request):
    # import pdfkit
    from xhtml2pdf import pisa
    import cStringIO as StringIO

    try:
        # create an API client instance
        # client = pdfcrowd.Client("mjnasir", "a90dfc17a16777af1087a5b00e88b5c2")

        # convert a web page and store the generated PDF to a variable
        ro_number = request.GET.get("ro_number")
        dealer_id = request.session["dealer_id"]

        # url = request.GET.get("url")
#        url_str = "https://greenlightautomotive.com/app/flagger/action_plan_pdf/?roNumber=" +str(ro_number)+"&dealer_code=infiniti-0134&dealer_id="+str(dealer_id)

        # url_str = settings.SITE_MAIN_URL+ reverse("flagging:action_plan_pdf") + "?roNumber=" +str(ro_number)+"&dealer_id="+str(dealer_id)
        # print url_str
        # pdf = pdfkit.from_url(url_str, False)
#         render(request,"flagging_app/action_plan_pdf")
         # set HTTP response headers
        # response = HttpResponse(content_type="application/pdf")
        # response["Cache-Control"] = "max-age=0"
        # response["Accept-Ranges"] = "none"
        # response["Content-Disposition"] = "attachment; filename=action_plan.pdf"

        # send the generated PDF
        # response.write(pdf)
        # return response

        def fetch_resouces(uri, rel):
            """
            Convert HTML URIs to absolute system paths so xhtml2pdf can access those
            resources
            """
            # use short variable names
            sUrl = settings.STATIC_URL  # Typically /static/
            sRoot = settings.STATIC_ROOT  # Typically /home/userX/project_static/
            mUrl = settings.MEDIA_URL  # Typically /static/media/
            mRoot = settings.MEDIA_ROOT  # Typically /home/userX/project_static/media/

            # convert URIs to absolute system paths
            if uri.startswith(sUrl):
                if sRoot == None:
                    sRoot = settings.BASE_DIR
                path = os.path.join(sRoot, uri.replace(sUrl, ""))
            else:
                path = os.path.join(mRoot, uri.replace(mUrl, ""))


            # make sure that file exists
            if not os.path.isfile(path):
                return uri  # handle absolute uri (ie: http://some.tld/foo.png)

            if not os.path.isfile(path):
                raise Exception(
                'media URI must start with %s or %s' % (sUrl, mUrl)
                )

            return path



        html = generate_action_view(request, ro_number, dealer_id, template_only=True)
        result = StringIO.StringIO()
        pdf = pisa.pisaDocument(StringIO.StringIO(html.encode("utf-8")), dest=result,
                                    link_callback=fetch_resouces, encoding="utf-8")
        if not pdf.err:
            return HttpResponse(result.getvalue(), content_type='application/pdf')
        else:
            return HttpResponse('PDF could not be generated <pre> ' + pdf + '</pre>')

    except Exception as e:
        return HttpResponse('PDF could not be generated <pre> ' + str(e) + '</pre>')






def email_action_plan(request):
    try:
        userservice = CUserService()
        notfication_service = NotificationService()
        ro_number = request.GET.get("ro_number")
        dealer_id = request.session["dealer_id"]
        dealer = Dealer.objects.get(id=dealer_id)
        ro = RoServices(dealer_id)
        url = request.GET.get("url")
        appt = Appointment.objects.get(ro__ro_number = ro_number)

        final_url = settings.SITE_MAIN_URL + reverse("flagging:action_plan_pdf") + "?action_plan=" + ro.getROHash(ro_number) + "&dealer_code="+str(dealer.dealer_code)
        emails = userservice.get_profile_emails(appt.customer)

        #             userservices.UserService().sendEmailPDf(email, final_url)
        main_site_url = settings.SITE_MAIN_URL + reverse(
            'customer:main') + "?dealer_code=" + appt.dealer.dealer_code
        account_url = settings.SITE_MAIN_URL + reverse(
            'customer:accountsettings') + "?dealer_code=" + appt.dealer.dealer_code
        notification_url = settings.SITE_MAIN_URL + reverse(
            'customer:notifications') + "?dealer_code=" + appt.dealer.dealer_code

        if len(emails)>0:
            email = emails[0]
            # print final_url

            notification_settings = notfication_service.get_scheduled_settings(appt.customer)
            #send_text = True
            #if notification_settings["text"] == True:
            #    send_text=True

            advisor = appt.advisor.userprofile
            send_text = True
            if advisor:
                notification_settings = notfication_service.get_flagging_settings(advisor)
                if notification_settings.get("text",True) == False:
                    send_text=False

            context = {"name": appt.customer.get_name_firstlast(),
                       "main_site_url": main_site_url,
                       "account_url": account_url,
                       "notification_url": notification_url,
                       "final_url": final_url,
                       }
            from dealership.support import get_email_dealer_info
            context.update(get_email_dealer_info(dealer))

            notfication_service.send_dealer_based_notification(dealer,appt.customer,json.dumps(context),"action_plan",send_email=True,send_text=send_text)
            return HttpResponse("200")


        else:
            return HttpResponse("404")
    except Exception,e:
        return HttpResponse("400")

def action_plan_pdf(request):

        if request.method == "GET":
            # if request.method == "GET":
            if "action_plan" in request.GET:
                action_plan_hash = request.GET.get("action_plan")

                action_plan_ro = get_object_or_404(RO, ro_url_hash=action_plan_hash)
                roNumber = action_plan_ro.ro_number

                dealer = get_object_or_404(Dealer, dealer_code=request.GET.get("dealer_code"))
                dealer_id = dealer.id

                return generate_action_view(request, roNumber,dealer_id)

            #if "roNumber" in request.GET:
            #    roNumber = request.GET.get("roNumber")

            #    return generate_action_view(request, roNumber, request.GET.get("dealer_id"))

        raise Http404

def generate_action_view(request, roNumber, dealer_id, template_only=False, template_debug=False, params_only=False):
    params = {}
    from django.template.loader import get_template

    if roNumber:
        ro = RoServices(dealer_id)
        params['service'] = ro.getCustomerServiceRequest(roNumber)
        params['summary'] = ro.getSummaryDetailsByRoNumber(roNumber)
        params['roDetails'] = ro.getROdetails(roNumber)
        params['color'] = ro.getColorForRO(roNumber)
        params['roNumber'] = roNumber
        params['recommendations'] = ro.getInspectionRecommendationSummary(roNumber)
        params["inspection"] = ro.getCustomerInspectionRecommendation(roNumber)
        params["action_plan"] = "true"
        apt = Appointment.objects.get(ro__ro_number=roNumber)
        first_name = apt.customer.first_name if apt.customer.first_name else " "
        last_name = apt.customer.last_name if apt.customer.last_name else " "
        if hasattr(apt.vehicle, "vin_number"):
            params["vehicle_vin"] = apt.vehicle.vin_number
        else:
            params["vehicle_vin"] = "Not Available"
        params["customer"] = first_name + " " + last_name
        params["dealer_code"] = Dealer.objects.get(id=dealer_id).dealer_code

        appointment_id = apt.id
        try:
            appt_details = Appointment.objects.get(id=appointment_id)
        except Appointment.DoesNotExist:
            raise Http404
        flags = Flags.objects.filter(type=3, customer_facing=True, dealer_id=appt_details.dealer.id)
        recommendations = AppointmentRecommendation.objects.filter(appointment_id=appointment_id)

        total = 0
        approved_total = 0
        for obj in recommendations:
            total += obj.price
            if obj.status == "Accept":
                approved_total += obj.price

        appt_services = AppointmentService.objects.filter(appointment_id=appointment_id)
        if appt_details:
            userprofile = appt_details.customer
            #     userprofile =service.get_userprofile(request.user)
        service_total = 0
        for obj in appt_services:
            service_total += obj.price
        approved_recomm = AppointmentRecommendation.objects.filter(appointment_id=appointment_id, status="Accept")

        for obj in approved_recomm:
            service_total += obj.price
        dealer = appt_details.dealer
        dealer_code = None
        if dealer:
            dealer_code = dealer.dealer_code

        # If there are pending itmes approval still needs to be given by the customer.
        pending_recomm = AppointmentRecommendation.objects.filter(appointment_id=appointment_id, status="Pending")
        is_pending_recomm = True if pending_recomm.count() > 0 else False

        # if pending_recomm.count() > 0:
        #    appt_details.appointment_recommendation_status = False
        params.update(params["summary"].aggregate(Sum('laborcost')))
        params.update(params["summary"].aggregate(Sum('partscost')))

        laborcost_sum = params['laborcost__sum']
        if laborcost_sum is None:
            laborcost_sum = 0.0
        partscost_sum = params['partscost__sum']
        if partscost_sum is None:
            partscost_sum = 0.0
        params["inspection_total"] = partscost_sum + laborcost_sum

        inspection_items = params["summary"]
        num_fail = 0
        num_warn = 0
        num_pass = 0
        for s in inspection_items:
            if s.status == "fail":
                num_fail += 1
            if s.status == "warning":
                num_warn += 1
            if s.status == "pass":
                if s.observation or s.recommendations or s.partscost != 0 or s.laborcost != 0 or s.image:
                    num_pass += 1

        params.update({'flags': flags,
                       'appt': appt_details,
                       'recommendations': recommendations,
                       'total': total,
                       'appt_services': appt_services,
                       'approved_rec': approved_recomm,
                       's_total': service_total,
                       'approved_total': approved_total,
                       'is_pending_recomm': is_pending_recomm,
                       'num_fail': num_fail,
                       'num_warn': num_warn,
                       'num_pass': num_pass,
                        })

    if params_only:
        return params


    if template_debug:
        return render(request, "flagging_app/action_plan_generate_pdf.html", params)

    if template_only:
        template = get_template("flagging_app/action_plan_generate_pdf.html")
        return template.render(params)
    else:
        return render(request, "flagging_app/action_plan_customer.html", params)
