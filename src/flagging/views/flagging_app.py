'''
Created on Nov 29, 2015

@author: mjnasir
'''
#from aetypes import template

from datetime import date, datetime
import pytz

from django.contrib.auth.decorators import user_passes_test
from django.core.urlresolvers import reverse
from django.http.response import JsonResponse, HttpResponseRedirect, \
    HttpResponse
from django.shortcuts import render
from django.template import Context, Template
from flagging.forms import UpdateRODetails
from django.conf import settings

import dealership
from dealership.models import Flags, RO
from dealership.services.appointmentservices import AppointmentService
from flagging import Constants
from flagging.decorators import technician_group_check, flagger_access_check
from flagging.services.RoServices import RoServices
from django.core.paginator import  EmptyPage, PageNotAnInteger
from dealership.services.paginator import DiggPaginator as Paginator

#from pip._vendor.requests.models import json_dumps
# from flagging.factory import RoServicesFactory
#@user_passes_test(technician_group_check,login_url=Constants.REDIRECT_URL)
@flagger_access_check
def index(request):
    ro = RoServices(request.session["dealer_id"])
    template_params = ro.getAllFlagsDealer(request.session["dealer_id"])
    template_params["user"] = request.user
    template_params["dealer_code"]  =request.session["dealer_code"]
    template_params["refresh_rolist"] = settings.RO_LIST_REFRESH
    template_params["live_notifictions"] = settings.LIVE_NOTIFICATIONS
    template_params["live_notifictions_refresh_rate"] = settings.LIVE_NOTIFICATIONS_REFRESH_RATE
    return render(request,"flagging_app/base.html",template_params)


def ro_list_ajax(request):
#     return render(request,"flagging_app/search_header.html")
    return HttpResponseRedirect(reverse("flagging:search_ros"))

def search_ros_ajax(request):
    filter_dict = {}
    value = ""
    type = ""
    temp_params = {"isOrdered" : False}
    page = request.GET.get("page",1)
    try:
        filter_dict['status'] = request.GET.get("status","active")
        if "roNumber" in request.GET:
            value = filter_dict['ro_number'] = request.GET.get("roNumber")
            type = "roNumber"
        elif "customer" in request.GET:
            value = filter_dict['customer'] = request.GET.get("customer")
            type = "customer"
        elif "advisor" in request.GET:
            value = filter_dict['advisor'] = request.GET.get("advisor")
            type = "advisor"
        elif "flags" in request.GET:
            value = filter_dict['flags'] = request.GET.get("flags")
            type = "flags"
        temp_params["orderBy"] = filter_dict["orderBy"] = request.GET.get("orderBy", "ro_date")
        temp_params["isOrdered"] = True
        temp_params["order"] = filter_dict["order"] = request.GET.get("order","desc")
#         if page == 1:
        temp_params["order"] = "desc" if filter_dict["order"] == "desc" else "asc"
    #     if request.GET.get("order"):
    #         filter_dict["order"] = request.GET.get("order")
    #         temp_params["isOrdered"] = True
    #         temp_params["order"] = request.GET.get("order")
    #         if page == 1:
    #             temp_params["order"] = "asc" if request.GET.get("order") == "desc" else "desc"
        ro_services = RoServices(request.session["dealer_id"])
        ro = ro_services.getRos(filter_dict)

        paginator = Paginator(ro, 25)
        ro_list = paginator.page(page)


    except PageNotAnInteger:
        ro_list = paginator.page(1)
    except EmptyPage:
        ro_list = paginator.page(paginator.num_pages)
    if len(ro_list) == 1:
            temp_params["roDetails"] = ro_services.getROdetails(ro_list[0].ro.ro_number)
            temp_params["color"] = ro_services.getColorForRO(ro_list[0].ro.ro_number)
    temp_params["ro_list"] = ro_list
    temp_params["ro_number"] = value
    temp_params["status"] =  request.GET.get("status","active")
    temp_params["type"] = type
    temp_params["page"] = page
    return render(request,"flagging_app/ro_list.html",temp_params)



def update_flags(request):
    var = ""
    if request.method == "GET":
        appointment =  RoServices(request.session["dealer_id"]).get_updated_flags_appointment(request)
    return render(request,"flagging_app/partial/ro_row_data.html",{"appointment" : appointment})


def get_shop_notes(request):
    notes = None
    template_name ="flagging_app/shop_notes.html"
    roService = RoServices(request.session["dealer_id"])
    if request.method == "GET":
        notes,roObject = roService.getShopNotes(request.GET)


        if roObject !=None:
                roNumber = roObject.ro_number
        else :
            roNumber = request.GET.get("ro_number") if "ro_number" in request.GET else ""


#     if "ajaxRequest" in request.GET:
#         template_name = "flagging_app/shop_notes_ajax.html"

    return render(request,template_name,{"roID": roService.getROId(roNumber), "notes" : notes,"ro_number":roNumber,"ro" : roObject,"roDetails" : roService.getROdetails(roNumber)})


def add_note(request):
#     notes = None
#     ro_number =request.GET.get()
    roNumber = ""

    if request.method == "POST":

        notes,roObject = RoServices(request.session["dealer_id"]).addNote(request)
        roNumber = roObject.ro_number


    return HttpResponseRedirect( reverse('flagging:shop_notes')+"?ro_number=" +roNumber)


def editro(request):
    template_name ="flagging_app/editro.html"
    message = None
    roService = RoServices(request.session["dealer_id"])
    if request.method == "GET":
        roObject = roService.getROEditableDetails(request.GET)

        if roObject !=None:
                roNumber = roObject.ro_number
                rfid_tag = roObject.rfid_tag
        else :
            roNumber = request.GET.get("ro_number") if "ro_number" in request.GET else ""
            rfid_tag = None

    if request.method == "POST":
        roObject = roService.getROEditableDetails(request.REQUEST)
        roNumber = roObject.ro_number
        rfid_tag = roObject.rfid_tag
        roedit_form = UpdateRODetails(request.POST)
        if roedit_form.is_valid():
            if roObject:
                try:
                    if roedit_form.cleaned_data["roedit_rfid"]:
                        roObject.rfid_tag = roedit_form.cleaned_data["roedit_rfid"]
                    if roedit_form.cleaned_data["roedit_number"]:
                        roObject.ro_number = roedit_form.cleaned_data["roedit_number"]
                        roObject.save()
                        roNumber = roObject.ro_number
                        rfid_tag = roObject.rfid_tag
                        message = "Ro and Tag Updated!"
                except Exception as e:
                    message = "Failed to update RO or Tag"
        else:
            message = "RO or Tag Error"

    return render(request,template_name,
                  {"ro": roObject,
                   "roID": roService.getROId(roNumber),
                   "ro_number": roNumber, "roedit_number":roNumber,
                   "roedit_rfid" : rfid_tag,
                   "message": message})


def get_flag_to_update_type(request):
    json = {}
    if "roId" in request.GET:
        roId = int(request.GET.get("roId"))
        ro = RoServices(request.session["dealer_id"])

        json = {"nextFlag" : ro.getFlagToUpdateType(roId)}
    return JsonResponse(json)

#     return render(request,template_name,{"notes" : notes,"ro" : roObject})
def get_ro_details_ajax(request):
    if "roId" in request.GET:
        try:
            roService =RoServices(request.session["dealer_id"])
            ro = RO.objects.get(id=int(request.GET.get("roId")))
            roDetails = roService.getROdetails(ro.ro_number)
            color = roService.getColorForRO(ro.ro_number)
            return render(request,"flagging_app/partial/ro_details.html",{"roDetails" : roDetails})
        except Exception as e :
            return ""

def get_flag_notes(request):
#     print "asdfasd"
    notes = ""
    if request.method == "GET":
        id = int(request.GET.get("flagId"))
        notes = Flags.objects.get(id=id).notes
    return  HttpResponse(notes)

def mark_as_complete(request):
    ro_id = request.GET.get("ro_id")
    ro = RO.objects.get(id=ro_id)
    ro.ro_completed =  datetime.now(pytz.utc)
    ro.save()
    return HttpResponseRedirect(reverse("flagging:index"))

def add_recommendations(request):
    RoServices(request.session["dealer_id"]).addRecommendations(request)
    return HttpResponseRedirect(reverse("flagging:index"))
