function TechViewRecommendationsDiv(div){
	var self = this;
	self.statusRadio = $(div).find("input:radio");
	self.colorDiv = $(div);
	self.status = $(div).find("form").find(".result");
	self.bindEvents = function(){
		self.statusRadio.on("change",function(){
			var value = $(this).val();
			self.status.val(value);
			self.colorDiv.removeClass("alert-success").removeClass("alert-danger").removeClass("alert-warning").addClass(value=="Fail" ? "alert-danger" :"alert-warning" );
	    });
	}
}
