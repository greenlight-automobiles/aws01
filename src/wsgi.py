"""
WSGI config for BMW project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/1.7/howto/deployment/wsgi/
"""

import os
import logging

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "settings.development")



from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()

env_variables_to_pass = ['DJANGO_SETTINGS_MODULE', ]
# def application(environ, start_response):
#
#     # pass the WSGI environment variables on through to os.environ
#     for var in env_variables_to_pass:
#         os.environ[var] = environ.get(var, '')
#         # logging.warning(environ.get(var, ''))
#     # pass the WSGI environment variables on through to os.environ
#     return get_wsgi_application()(environ, start_response)
