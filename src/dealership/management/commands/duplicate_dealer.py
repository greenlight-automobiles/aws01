from copy import deepcopy
import sys
from django.core.management.base import BaseCommand, CommandError
from django.core.files.base import ContentFile
from dealership.models import (
    Dealer,
    Flags,
    InspectionPackage,
    InspectionItems,
    InspectionCategoriesItems,
    InspectionCategories,
    ServiceRepair,
    EmailTypes
)

import csv

class Command(BaseCommand):

    def add_arguments(self, parser):
        # Positional arguments
        parser.add_argument('sourcedealercode',help='Specify dealer code to copy FROM')
        parser.add_argument('destinationdealercode',help='Specify dealer code to copy TO')

        # Named (optional) arguments
        parser.add_argument('--copyflags', action="store_true", help='Copy the dealer flags')
        parser.add_argument('--copyservices',action="store_true",help='Copy the dealer services and repairs')
        parser.add_argument('--copyinspections',action="store_true",help='Copy the dealer inspection lists')
        parser.add_argument('--copyemailtypes',action="store_true",help='Copy the dealer email types')
        parser.add_argument('--copyall',action="store_true",help='Copy all setup data {flags, services and repairs, '
                                                                 'inspection lists, email events')
        parser.add_argument('--overwrite',action="store_true",help='Overwrite existing data in destination dealer if '
                                                                   'it has existing data')


    def handle(self, *args, **options):
        # ...

        source_dealer = options['sourcedealercode']
        dest_dealer = options['destinationdealercode']

        overwrite = options['overwrite']

        copy_flags = options['copyflags']
        copy_services = options['copyservices']
        copy_emailtypes = options['copyemailtypes']
        copy_inspections = options['copyinspections']
        copy_all = options['copyall']

        if copy_all:
            copy_flags = True
            copy_services = True
            copy_emailtypes = True
            copy_inspections = True


        print("Dealer From: {}".format(source_dealer))
        print("Dealer To: {}".format(dest_dealer))

        print("Overwrite: {}".format(overwrite))

        print("Copy Flags: {}".format(copy_flags))
        print("Copy Services: {}".format(copy_services))
        print("Copy Email Types: {}".format(copy_emailtypes))
        print("Copy Inspections: {}".format(copy_inspections))

        if overwrite:
            print("Overwriting existing data! If the destination dealer has existing data it will be deleted first")



        # dealer_from = Dealer.objects.get(dealer_code="template")
        dealer_from = Dealer.objects.get(dealer_code=source_dealer)
        if dealer_from is None:
            self.exit_error("Source Dealer not found")
        else:
            print("Source Dealer: {}".format(dealer_from))

        dealer_to = Dealer.objects.get(dealer_code=dest_dealer)
        if dealer_to is None:
            self.exit_error("Destination Dealer not found")
        else:
            print("Destination Dealer: {}".format(dealer_to))

        if copy_flags:
            self.copy_flags(dealer_from, dealer_to, overwrite=overwrite)

        if copy_inspections:
            self.copy_inspections(dealer_from, dealer_to, overwrite=overwrite)

        if copy_services:
            self.copy_services(dealer_from, dealer_to, overwrite=overwrite)

        if copy_emailtypes:
            self.copy_emailtypes(dealer_from, dealer_to, overwrite=overwrite)



    # Support Methods
    @staticmethod
    def copy_file(src):
        """
        Duplicating this object including copying the file is points to
        """
        try:
            new_file = ContentFile(src.read())
        except:
            return None
        new_file.name = src.name
        return new_file

    def exit_error(self, msg):
        self.stderr.write(msg)
        sys.exit(1)

    # Copy Methods
    def copy_flags(self, dealer_from, dealer_to, overwrite=False):
        print("Copying Flags:"),
        # Check to see if the destination dealer has existing flags
        flags_to = Flags.objects.filter(dealer=dealer_to)
        if flags_to.count()>0:
            if not overwrite:
                self.exit_error("Destination Dealer has Flags specified. Use '--overwrite' to overwrite existing "
                                "flags before copying")
            else:
                if overwrite:
                    flags_to.delete()
                print("Existing flags deleted: "),

        flags = Flags.objects.filter(dealer=dealer_from)

        for flag in flags:
            flag.pk = None
            flag.dealer = dealer_to
            flag.save()
        print("Copied")

    def copy_inspections(self, dealer_from, dealer_to, overwrite=False):
        print("Copying Inspection Data:"),
        # Check to see if the destination dealer has existing flags
        categories_items_to = InspectionCategoriesItems.objects.filter(category__package__dealer=dealer_to)
        categories_to = InspectionCategories.objects.filter(package__dealer=dealer_to)
        packages_to = InspectionPackage.objects.filter(dealer=dealer_to)

        if packages_to.count()>0 or categories_to.count()>0 or categories_items_to.count()>0:
            if not overwrite:
                self.exit_error("Destination Dealer has Inspection data specified. "
                                "Use '--overwrite' to overwrite existing "
                                "inspection categories and packages before copying")
            else:
                categories_items_to.delete()
                categories_to.delete()
                packages_to.delete()
                print("Existing inspection data deleted: "),

        packages_from = InspectionPackage.objects.filter(dealer=dealer_from)

        # Save packages
        for package in packages_from:
            package.pk = None
            package.dealer = dealer_to
            package.save()

        categories_from = InspectionCategories.objects.filter(package__dealer=dealer_from)

        # Save packages
        for category in categories_from:
            # Get the package
            package_name = category.package.package

            category.pk = None
            package_to = InspectionPackage.objects.get(package=package_name, dealer=dealer_to)
            category.package = package_to
            category.save()


        categories_items_from = InspectionCategoriesItems.objects.filter(category__package__dealer=dealer_from)

        # Save packages
        for category_item in categories_items_from:
            # Get the package
            package_name = category_item.category.package.package
            category_name = category_item.category.category

            category_item.pk = None
            inspection_category_to = InspectionCategories.objects.get(package__dealer=dealer_to,
                                                                      package__package=package_name,
                                                                      category=category_name)
            category_item.category = inspection_category_to
            category_item.save()

        print("Copied")

    # Copy Methods
    def copy_services(self, dealer_from, dealer_to, overwrite=False):
        print("Copying Services:"),
        # Check to see if the destination dealer has existing flags
        services_to = ServiceRepair.objects.filter(dealer=dealer_to)
        if services_to.count()>0:
            if not overwrite:
                self.exit_error("Destination Dealer has Services/Repairs specified. Use '--overwrite' to overwrite existing "
                                "services before copying")
            else:
                if overwrite:
                    services_to.delete()
                print("Existing services deleted: "),

        services_from = ServiceRepair.objects.filter(dealer=dealer_from)

        for service in services_from:
            service.pk = None
            service.dealer = dealer_to
            service.image = self.copy_file(service.image)
            service.user = None
            service.save()
        print("Copied")


    # Copy Methods
    def copy_emailtypes(self, dealer_from, dealer_to, overwrite=False):
        print("Copying Email Types:"),
        # Check to see if the destination dealer has existing flags
        emailtypes_to = EmailTypes.objects.filter(dealer=dealer_to)
        if emailtypes_to.count()>0:
            if not overwrite:
                self.exit_error("Destination Dealer has EmailTypes specified. Use '--overwrite' to overwrite existing "
                                "email types before copying")
            else:
                if overwrite:
                    emailtypes_to.delete()
                print("Existing email types deleted: "),

        emailtypes_from = EmailTypes.objects.filter(dealer=dealer_from)

        for email_type in emailtypes_from:
            # email_type.pk = None
            email_type.pk = None
            email_type.id = None
            email_type.dealer = dealer_to
            email_type.signature = dealer_to.name
            email_type.save()
        print("Copied")

