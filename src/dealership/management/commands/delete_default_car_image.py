from django.core.management.base import BaseCommand, CommandError
from django.db import transaction
from dealership.models import Vehicle, VinMake, VinModel, VinYear, VinTrim, DealersVehicle, CustomerVehicle, RO

import csv

class Command(BaseCommand):
    def add_arguments(self, parser):
        # Named (optional) arguments
        parser.add_argument(
            '--test',
            action="store_true",
            help='Do a test run',
        )

    def handle(self, *args, **options):
        # ...
        vehicles_with_image = Vehicle.objects.filter(mainimage__isnull=False)
        test = options['test']
        with transaction.atomic():
            for vehicle in vehicles_with_image:
                vehicle.mainimage = None
                if test:
                    print "Updating (Test): Make: {}, Model: {}, Year: {}".format(vehicle.make, vehicle.model, vehicle.year)
                else:
                    vehicle.save()
                    print "Updating: Make: {}, Model: {}, Year: {}".format(vehicle.make, vehicle.model, vehicle.year)

        print "Done"
        # ...
