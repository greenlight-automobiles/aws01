from django.core.management.base import BaseCommand, CommandError
from django.db import transaction
from dealership.models import Vehicle, VinMake, VinModel, VinYear, VinTrim, DealersVehicle, CustomerVehicle, RO

import csv

class Command(BaseCommand):
    def add_arguments(self, parser):
        # Positional arguments
        parser.add_argument('vehicles',
            help = 'Specify vehicle file in csv format',
        ),

        # Named (optional) arguments
        parser.add_argument(
            '--delete',
            action="store_false",
            help='Delete all vehicles before loading',
        )

        # Named (optional) arguments
        parser.add_argument(
            '--test',
            action="store_true",
            help='Do a test run',
        )

    def handle(self, *args, **options):
        # ...
        vehicle_file = options['vehicles']

        vehicles = Vehicle.objects.all()
        #for vehicle in vehicles:
        #    print "Make: {}, Val:{}, Model: {}, Val:{}, Year:{}, Val:{}".format(vehicle.make.name, vehicle.make.val,
        #                                                        vehicle.model.name, vehicle.model.val,
        #                                                        vehicle.year.name, vehicle.year.val)

        if options['delete']==False:
            print ("Deleting")
            RO.objects.all().delete()
            CustomerVehicle.objects.all().delete()
            DealersVehicle.objects.all().delete()
            Vehicle.objects.all().delete()
            VinMake.objects.all().delete()
            VinMake.objects.all().delete()
            VinMake.objects.all().delete()

        test = options['test']

        self.load_car_csv(vehicle_file, test)

        print "Done"
        # ...


    def load_car_csv(self, vehicle_file, test):
        import operator

        skip=False
        with transaction.atomic():
            with open(vehicle_file) as f:
                reader = csv.reader(f, delimiter=',')

                if skip==False:
                            # process makes
                    sortedlist = sorted(reader, key=operator.itemgetter(1))
                    last_make = None
                    alldb_makes = VinMake.objects.all()
                    for row in sortedlist:
                        if str(row[0])=="model_id":
                            continue
                        make = str(row[1])
                        if last_make!= make:
                            last_make = make
                            print "Processing Make: ", make
                            db_makes = alldb_makes.filter(val__iexact=make)
                            if db_makes.count() > 1:
                                if test:
                                    print "Deleting: " + make
                                else:
                                    db_makes.delete()

                            # Add Model to tables
                            db_make, created = alldb_makes.get_or_create(val=make.title())
                            if db_make.name != make.title() or db_make.val != make.title():
                                db_make.name = make.title()
                                db_make.val = make.title()
                                if test:
                                    print "Writing Make: " + make
                                else:
                                    db_make.save()

                    # Process models
                    f.seek(0)
                    reader = csv.reader(f)
                    sortedlist = sorted(reader, key=operator.itemgetter(2))
                    last_model = None
                    alldb_models = VinModel.objects.all()
                    for row in sortedlist:
                        if str(row[0]) == "model_id":
                            continue
                        model = str(row[2])
                        if last_model != model:
                            last_model = model

                            # print "Processing Model: ", model

                            db_models = alldb_models.filter(val=model)
                            if db_models.count() > 1:
                                if test:
                                    print "Deleting Models: " + model
                                else:
                                    db_models.delete()

                            db_model, created = alldb_models.get_or_create(val=model)
                            if db_model.name != model or db_model.val != model:
                                db_model.name = model
                                db_model.val = model
                                if test:
                                    print "Writing Model: " + model
                                else:
                                    db_model.save()

                    # Process trim
                    f.seek(0)
                    reader = csv.reader(f)

                    last_trim = None
                    sortedlist = sorted(reader, key=operator.itemgetter(3))
                    alldb_trims = VinTrim.objects.all()

                    for row in sortedlist:
                        if str(row[0]) == "model_id":
                            continue
                        trim = str(row[3])

                        if last_trim != trim:
                            last_trim = trim

                            # print "Processing trim: ", trim


                            db_trims = alldb_trims.filter(val=trim)
                            if db_trims.count() > 1:
                                if test:
                                    print "Deleting Trims: " + trim
                                else:
                                    db_trims.delete()

                            db_trim, created = alldb_trims.get_or_create(val=trim)
                            if db_trim.name != trim or db_trim.val != trim:
                                db_trim.name = trim
                                db_trim.val = trim
                                if test:
                                    print "Writing Trim: " + trim
                                else:
                                    db_trim.save()

                    # Process trim
                    f.seek(0)
                    reader = csv.reader(f)

                    # Process years
                    last_year = None
                    sortedlist = sorted(reader, key=operator.itemgetter(4))
                    alldb_years = VinYear.objects.all()

                    for row in sortedlist:
                        if str(row[0]) == "model_id":
                            continue

                        year = str(row[4])
                        if last_year != year:
                            last_year = year

                            # print "Processing Year: ", year

                            db_years = alldb_years.filter(val=year)
                            if db_years.count() > 1:
                                if test:
                                    print "Writing Years: " + year
                                else:
                                    db_years.delete()

                            db_year, created = alldb_years.get_or_create(val=year)
                            if db_year.name != year or db_year.val != year:
                                db_year.name = year
                                db_year.val = year
                                if test:
                                    print "Writing Year: " + year
                                else:
                                    db_year.save()




                # Add vehicles
                f.seek(0)
                reader = csv.reader(f)
                for row in reader:
                    if str(row[0]) == "model_id":
                        continue


                    make = str(row[1])
                    model = str(row[2])
                    trim = str(row[3])
                    #if str(row[3]):
                    #    model += " "+str(row[3]).title()
                    year = str(row[4])

                    # Sanity Check. If more then one entry remove all of them and start again

                    # print "{}, {}, {} ".format(make, model, year)

                    try:
                        makedb = VinMake.objects.get(val__iexact=make)
                    except VinMake.DoesNotExist:
                        print "Could not find Make", make
                        exit();

                    try:
                        modeldb = VinModel.objects.get(val=model)
                    except VinModel.DoesNotExist:
                        print "Could not find Model: ", model
                        exit();

                    try:
                        yeardb = VinYear.objects.get(val=year)
                    except VinYear.DoesNotExist:
                        print "Could not find Year ", year
                        exit();

                    if test:
                        try:
                            newvehicle = Vehicle.objects.get(make=makedb, model=modeldb, year=yeardb)
                        except:
                            print "Writing Vehicle: Make: {}, Model: {}, Year: {}".format(make, model, year)

                    else:
                        Vehicle.objects.get_or_create(make=makedb,model=modeldb, year=yeardb)
