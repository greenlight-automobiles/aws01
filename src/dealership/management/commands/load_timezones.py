import pytz

from django.core.management.base import BaseCommand
from dealership.models import TimeZones

class Command(BaseCommand):

    def handle(self, *args, **options):

        for app_tz in TimeZones.objects.all():
           app_tz.delete()

        for tz in pytz.all_timezones:
            if "US/" in tz or "America/" in tz or tz == "UTC":
                app_tz = TimeZones(name=tz, timezone=tz)
                app_tz.save()
