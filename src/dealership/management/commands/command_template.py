# Use this command as a template for other comands

from datetime import datetime
import re
import sys

from django.core.management.base import BaseCommand, CommandError
from django.db.models import Q
from django.core.exceptions import ObjectDoesNotExist

from dealership.models import Vehicle, VinMake, VinModel, VinYear, VinTrim, DealersVehicle, CustomerVehicle, RO, \
    UserProfile, Dealer, States

import csv


class Command(BaseCommand):
    """Use this command file to quickly dump and modify production data."""
    def add_arguments(self, parser):
        # Positional arguments
        parser.add_argument('--dealer', help="Dealer Code")
        parser.add_argument('--update', action='store_false', help="Update Database")


    def handle(self, *args, **options):
        # Import Vehicle models and year
        dealer_code = options['dealer']
        updatedb = options['update']

        customers = UserProfile.objects.all()
        for c in customers:
            print "{}, {}, 1: {}, 2: {}, 3: {}".format(c.last_name, c.first_name, c.phone_number_1, c.phone_number_2, c.phone_number_3)

        print "Done"


    def exit_error(self, msg):
        self.stderr.write(msg)
        sys.exit(1)
