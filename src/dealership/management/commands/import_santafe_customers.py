from datetime import datetime
import re
import sys

from django.core.management.base import BaseCommand, CommandError
from django.db.models import Q
from django.core.exceptions import ObjectDoesNotExist

from dealership.models import Vehicle, VinMake, VinModel, VinYear, VinTrim, DealersVehicle, CustomerVehicle, RO, \
    UserProfile, Dealer, States

import csv


class Command(BaseCommand):
    def add_arguments(self, parser):
        # Positional arguments
        parser.add_argument('dealer',
                            help="Dealer Code")

        parser.add_argument('customers',
            help = 'Specify customer file in csv format',),

        parser.add_argument('vin',
                            help="Specify vin lookup file in csv format")

        parser.add_argument('--make',default="BMW",
                            help="Specify make of car to import")

    def handle(self, *args, **options):
        # Import Vehicle models and year
        vehicle_file = options['customers']
        vin_file = options['vin']
        dealer_code = options['dealer']
        dealer = None
        if dealer_code is None:
            self.exit_error("Dealer Code is not specified")
        try:
            dealer = Dealer.objects.get(dealer_code=dealer_code)
        except Exception as e:
            self.exit_error("Dealer Not Found: {}".format(e.msg))

        #for vehicle in vehicles:
        #    print "Make: {}, Val:{}, Model: {}, Val:{}, Year:{}, Val:{}".format(vehicle.make.name, vehicle.make.val,
        #                                                        vehicle.model.name, vehicle.model.val,
        #                                                        vehicle.year.name, vehicle.year.val)
        make = options['make']
        self.load_car_csv(dealer, make, vehicle_file, self.load_vins_models(vin_file))
        print "Done"

    def load_vins_models(self, vin_file):
        vin_lookup = dict()
        with open(vin_file) as vinf:
            vin_reader = csv.DictReader(vinf, delimiter=',')
            for row in vin_reader:
                vin = row['VIN']
                car = (row['VehicleModel'], row['VehicleModelYear'])
                vin_lookup[vin] = car


        return vin_lookup

    def load_car_csv(self, dealer, make, vehicle_file, vin_lookup):

        if make:
            cars_query = Q(make__name__iexact=make)
        else:
            cars_query = Q()

        with open(vehicle_file) as f:
            f.next()  # skip header line

            reader = csv.reader(f, delimiter=',')
            sortedlist = sorted(sorted(reader, key =lambda x: datetime.strptime(x[4], "%m/%d/%Y %H:%M") , reverse=True), key=lambda x: (x[10], x[9]) )

            saved_last_name = None
            saved_first_name = None

            not_found_counter = 0
            customer_counter = 0
            for row in sortedlist:
                last_name = row[10]
                first_name = row[9]
                creation_date = row[4]

                make = row[22] if row[22] != "" else "None"
                model = row[23] if row[23] != "" else "None"
                model = model.split()[0]
                model = model.replace("-", " ")
                year = row[21] if row[21] != "" else "None"
                vin = row[24] if row[24] != "" else "None"
                if saved_first_name == first_name and saved_last_name == last_name:
                    # skip if already exists
                    continue

                customer_counter += 1
                saved_first_name = first_name
                saved_last_name = last_name


                if make != "None" and model != "None" and year != "None" and vin != "None":
                    # First try to find as it

                    try:
                        car = vin_lookup[vin]
                        found = True
                        model = car[0]
                        year = car[1]
                        vehicle = Vehicle.objects.filter(make__name__iexact=make,
                                                       model__name__iexact=model,
                                                       year__name__iexact=year)
                        print ": Looked Up and Replaced: Car: {}, {}".format(model, year),
                    except KeyError:
                        found = False


                    if not found:
                        vehicle = Vehicle.objects.filter(make__name__iexact=make,
                                                       model__name__iexact=model,
                                                       year__name__iexact=year)
                        found = vehicle.count() > 1

                    if not found:
                        # Next try to find
                        model = re.sub('[^0-9]', '', model)

                        vehicle = Vehicle.objects.filter(make__name__iexact=make,
                                                       model__name__icontains=model,
                                                       year__name__iexact=year)
                        found = vehicle.count() > 1



                    if not found:
                        print "{} {}: Creation Date: {}, Car {}, {}, {}".format(last_name, first_name, creation_date,
                                                                                make, model, year),
                        print ": Not Found. VIN: {}".format(vin),
                        not_found_counter += 1
                    else:
                        self.add_customer_and_car(dealer, vehicle[0], row, make, model, year, vin)


                else:
                    print ": Missing Car Info VIN: {}".format(vin),

                print ""



            print "Records Total: {} Cars Not Found: {}".format(customer_counter, not_found_counter)


    def add_customer_and_car(self, dealer, vehicle, row, make, model, year, vin):



        # Add Customer To DB
        last_name = row[10]
        first_name = row[9]
        address = row[12]
        city = row[13]
        state = row[14]
        zip_code = row[15]
        day_phone = row[16]
        night_phone = row[17]
        cell_phone = row[18]
        email = row[19]
        alt_email = row[20]

        # First create customer profile
        try:
            profile = UserProfile.objects.get(dealer=dealer, first_name__iexact=first_name, last_name__iexact=last_name, email_1=email)

        except ObjectDoesNotExist:
            profile = UserProfile()

        # if the profile doesn't exists then create it with a car
        profile.dealer = dealer
        profile.email_1 = email
        profile.first_name = first_name if first_name is not None else ""
        profile.last_name = last_name if last_name is not None else ""
        profile.active_email = email
        profile.email_2 =alt_email
        profile.phone_number_1 = cell_phone
        profile.phone_number_2 = night_phone
        profile.phone_number_3 = day_phone
        profile.zipcode = zip_code
        profile.address_line1 = address
        profile.city = city
        if state == "":
            state ="NM"
        profile.state_us = States.objects.get(state_abbr=state)
        profile.save()


        # Now create vehicle

        # Add the vehicle first
        try:
            cust_vehicle = CustomerVehicle.objects.get(vehicle=vehicle, user=profile, vin_number=vin)
        except ObjectDoesNotExist:
            cust_vehicle = CustomerVehicle(vehicle=vehicle, user=profile, vin_number=vin)
            cust_vehicle.save()


    def exit_error(self, msg):
        self.stderr.write(msg)
        sys.exit(1)
