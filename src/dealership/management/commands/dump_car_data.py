from django.contrib.admin.util import NestedObjects
from django.db import DEFAULT_DB_ALIAS

from copy import deepcopy
from django.db.models import Count, Q
from django.db.models.deletion import Collector

from django.core.management.base import BaseCommand, CommandError
from django.core.files.base import ContentFile
from dealership.models import UserProfile, Dealer, Flags, CustomerVehicle, Vehicle, VinMake, VinTrim, VinModel, VinYear
from django.contrib.auth.models import Group
import os
import sys

# class Vehicle(models.Model):
#     make = models.ForeignKey(VinMake,related_name="makevehicles") #max_length=255, null = False)
#     model = models.ForeignKey(VinModel,related_name="modelvehicles") #models.CharField(max_length=255, null = False)
#
#     year = models.ForeignKey(VinYear,related_name="yearvehicles")#models.CharField(max_length=255, null = False)
#     mainimage = models.ImageField(null=True)
#     def __str__(self):
#         return(self.make.name + self.model.name + str(self.year.name))

#
#
# class VinMake(models.Model):
#     name = models.CharField(max_length=200)
#     val = models.CharField(max_length=200)
#     def __str__(self):
#         return(self.name)
#
#
#
# class VinModel(models.Model):
#     name = models.CharField(max_length=200)
#     val = models.CharField(max_length=200)
#     def __str__(self):
#         return(self.name)
#
#
#
#
# class VinYear(models.Model):
#     name = models.CharField(max_length=200)
#     val = models.CharField(max_length=200)
#     def __str__(self):
#         return(self.name)
#
#
#
#
# class VinTrim(models.Model):
#     name = models.CharField(max_length=200)
#     val = models.CharField(max_length=200)
#     def __str__(self):
#         return(self.name)


class Command(BaseCommand):
    help = 'Show/Remove customers that have no vehicle'


    def add_arguments(self, parser):
        # Add positional arguments
        parser.add_argument('--make', nargs='*', default=["", ], type=str,
                            help="Specify make of car to dump")
        parser.add_argument('--dump', action='store_true', dest='dump', default=False,
                            help="Dump current database")


    def handle(self, *args, **options):
        make = options['make'][0]

        cars_query = Q()
        if make:
            cars_query = Q(make__name__iexact=make)

        cars = Vehicle.objects.filter(cars_query).order_by("make", "model", "year")
        if not cars:
            self.stderr.write("No Cars Found")
            sys.exit(0)

        print "Make, Model, Year"
        for car in cars:
            print "{}, {}, {}".format(car.make.name, car.model.name, car.year.name)


    def exit_error(self, msg):
        self.stderr.write(msg)
        sys.exit(1)
