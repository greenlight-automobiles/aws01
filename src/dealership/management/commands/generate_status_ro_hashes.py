from datetime import timedelta
import datetime
from django.db.models import Q

from django.core.management.base import BaseCommand, CommandError
from django.utils import timezone

from dealership.factories import DealerShipServicesFactory
from dealership.models import *


class Command(BaseCommand):

    def handle(self, *args, **options):
        appointments = Appointment.objects.filter(Q(status_url_hash=None) | Q(status_url_hash=str()))
        for app in appointments:
            app.save()

        ros = RO.objects.filter(Q(ro_url_hash=None) | Q(ro_url_hash=str()))
        for ro in ros:
            ro.save()
