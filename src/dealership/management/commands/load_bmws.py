from datetime import datetime
import re
import sys

from django.core.management.base import BaseCommand, CommandError
from django.db.models import Q
from django.core.exceptions import ObjectDoesNotExist

from dealership.models import Vehicle, VinMake, VinModel, VinYear, VinTrim, DealersVehicle, CustomerVehicle, RO, \
    UserProfile, Dealer, States

import csv


def find(f, seq):
  """Return first item in sequence where f(item) == True."""
  for item in seq:
    if f(item):
      return item

class Command(BaseCommand):
    def add_arguments(self, parser):
        # Positional arguments
        parser.add_argument('dealer', help="Dealer Code")
        parser.add_argument('bmws', help="BMW File")

        parser.add_argument('--make',default="BMW",
                            help="Specify make of car to import")

        #parser.add_argument('cars',
        #    help = 'Specify customer file in csv format',),


    def handle(self, *args, **options):
        # Import Vehicle models and year
        # vehicle_file = options['customers']
        bmw_file = options['bmws']
        dealer_code = options['dealer']
        dealer = None
        if dealer_code is None:
            self.exit_error("Dealer Code is not specified")
        try:
            dealer = Dealer.objects.get(dealer_code=dealer_code)
        except Exception as e:
            self.exit_error("Dealer Not Found: {}".format(e.msg))


        # Check models that are being used.



        #for vehicle in vehicles:
        #    print "Make: {}, Val:{}, Model: {}, Val:{}, Year:{}, Val:{}".format(vehicle.make.name, vehicle.make.val,
        #                                                        vehicle.model.name, vehicle.model.val,
        #                                                        vehicle.year.name, vehicle.year.val)
        make = options['make']
        self.load_bmw_models(dealer, make, bmw_file)
        print "Done"

    def load_bmw_models(self, dealer, make, bmw_file):
        model_lookup = []
        with open(bmw_file) as vinf:
            vin_reader = csv.reader(vinf, delimiter=',')
            years = next(vin_reader)
            for year in years:
                models_per_year = []
                model_lookup.append((year, models_per_year))


            for row in vin_reader:
                i = 0
                for model in row:
                    _, models_per_year = model_lookup[i]
                    models_per_year.append(model)
                    i += 1


            makedb = VinMake.objects.get(name__iexact=make)
            for vehicle in Vehicle.objects.filter(make=makedb):
                found = False
                print vehicle.model.name + ": " + vehicle.year.name,
                for year, models_per_year in model_lookup:
                    if year==vehicle.year.name:
                        for model in model_lookup:
                            if model==vehicle.model.name:
                                found = True

                if not found:
                    if vehicle.vehicle.all().count() > 0:
                        print " In Use",
                    else:
                        vehicle.delete()
                        print " Deleted",
                print ""


            for year, models_per_year in model_lookup:
                yeardb, created = VinYear.objects.get_or_create(name=year)
                if created:
                    yeardb.val = year
                    yeardb.save()
                for model in models_per_year:
                    if model:
                        print "Adding {}: {}: {}".format(make, model, year)
                        modeldb, created = VinModel.objects.get_or_create(name=model)
                        if created:
                            modeldb.val = model
                            modeldb.save()
                        vehicle, created = Vehicle.objects.get_or_create(make=makedb, model=modeldb, year=yeardb)
                        if created:
                            vehicle.mainimage= "default_car.jpg"
                            vehicle.save()



    def exit_error(self, msg):
        self.stderr.write(msg)
        sys.exit(1)

