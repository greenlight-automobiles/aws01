from datetime import timedelta
import datetime
# import the logging library
import logging
from django.core.management.base import BaseCommand, CommandError
from django.utils import timezone

from dealership.factories import DealerShipServicesFactory
from dealership.models import *

# Get an instance of a logger
logger = logging.getLogger('django')

class Command(BaseCommand):

    def handle(self, *args, **options):

        logger.info('Running Capacity Calculations')

        dealer_factory = DealerShipServicesFactory()
        capacityservice = dealer_factory.get_instance("capacity")
        dealers =Dealer.objects.all()
        now = datetime.datetime.now()
        start_date = now.date()
        end_date = start_date + timedelta(7)
        for dealer in dealers:
            capacityservice.save_tech_count_for_date_range(start_date,end_date,dealer)
            capacityservice.save_advisor_count_for_date_range(start_date, end_date, dealer)

        logger.info('Completed Capacity Calculations')
