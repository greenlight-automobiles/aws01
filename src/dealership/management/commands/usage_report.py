# Use this command as a template for other comands

from datetime import datetime
import re
import sys
import pytz
import csv
from django.utils import timezone

from django.core.management.base import BaseCommand, CommandError
from django.db.models import Q
from django.core.exceptions import ObjectDoesNotExist

from dealership.models import RO, \
    UserProfile, Dealer, Appointment, FlagsHistory

import csv


class Command(BaseCommand):
    """Use this command file to quickly dump and modify production data."""
    def add_arguments(self, parser):
        # Positional arguments
        parser.add_argument('--dealer', default="all", help="Dealer Code")
        parser.add_argument('--output', default="dealerstats.csv", help="Output CSV File Name")


    def handle(self, *args, **options):
        # Import Vehicle models and year
        dealer_code = options['dealer']
        output_file_name = options['output']

        self.run_command(dealer_code, output_file_name)


    def run_command(self, dealer_code, output_file_name):

        # Get todays date
        # Set dealership time zone.
        # Search for appointments created today
        # Search for flags set today.
        # Search for email sent today.
        try:
            csv_file = open(output_file_name, mode='w')
        except Exception, e:
            self.exit_error("Error opening csv file: {}. Error Message: {}".format(output_file_name, e))

        fieldnames = ["Dealer",
                      "Dealer Code",
                      "Date",
                      "Appts Sched",
                      "ROs Created",
                      "Flags Changed"]
        writer = csv.DictWriter(csv_file, fieldnames=fieldnames)
        writer.writeheader()


        dealer = None
        if dealer_code=='all':
            for dealer in Dealer.objects.all():
                self.dealer_stats(dealer, writer)
        else:
            try:
                dealer = Dealer.objects.get(dealer_code=dealer_code)
            except:
                self.exit_error("Dealer not found")
            self.dealer_stats(dealer, writer)
        csv_file.close()


    def dealer_stats(self, dealer, writer):
        # dealer.timezone()
        # datetime.now()

        if dealer.timezone:
            try:
                dealer_timezone = pytz.timezone(dealer.timezone)
            except:
                dealer_timezone = timezone.utc
            dealer_time = timezone.now().astimezone(dealer_timezone)
        else:
            dealer_time = timezone.now()

        dealer_date_current = dealer_time.date()
        dealer_start_time = dealer_time.replace(hour=0, minute=0, second=0, microsecond=0).astimezone(timezone.utc)
        dealer_end_time = dealer_time.replace(hour=23, minute=59, second=59, microsecond=9999).astimezone(timezone.utc)

        appt_count = Appointment.objects.filter(dealer=dealer,
                start_time__range=[dealer_start_time, dealer_end_time]).count()

        ros = RO.objects.filter(ro__dealer=dealer,
          ro_date__range=[dealer_start_time, dealer_end_time])
        # print (dealer_start_time, dealer_end_time)
        # for ro in RO.objects.filter(ro__dealer=dealer):
        #   print(ro.ro_number, ro.ro_date)
        ro_creation_count = ros.count()
        flags_changed = FlagsHistory.objects.filter(ro__ro__dealer=dealer,
            created_at__range=[dealer_start_time, dealer_end_time]).count()


        writer.writerow({'Dealer': dealer,
                         'Dealer Code': dealer.dealer_code,
                         'Date': dealer_date_current,
                         'Appts Sched': appt_count,
                         'ROs Created': ro_creation_count,
                         'Flags Changed': flags_changed })



    def exit_error(self, msg):
        self.stderr.write(msg)
        sys.exit(1)
