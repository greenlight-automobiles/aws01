from datetime import datetime
import re
import sys

from django.core.management.base import BaseCommand, CommandError
from django.db.models import Q
from django.core.exceptions import ObjectDoesNotExist

from dealership.models import Vehicle, VinMake, VinModel, VinYear, VinTrim, DealersVehicle, CustomerVehicle, RO, \
    UserProfile, Dealer, States

import csv


class Command(BaseCommand):
    """Use this command file to quickly dump and modify production data."""
    def add_arguments(self, parser):
        # Positional arguments
        parser.add_argument('--dealer', help="Dealer Code")
        parser.add_argument('--update', action='store_false', help="Update Database")


    def handle(self, *args, **options):
        # Import Vehicle models and year
        dealer_code = options['dealer']
        updatedb = options['update']

        dealer = None
        if dealer_code is None:
            self.exit_error("Dealer Code is not specified")
        try:
            dealer = Dealer.objects.get(dealer_code=dealer_code)
        except Exception as e:
            self.exit_error("Dealer Not Found: {}".format(e.msg))


        customer = UserProfile.objects.get(first_name="Andrew", last_name="Sidoli")
        customer.phone_number_2 = None
        customer.phone_number_3 = None
        # if updatedb:
            # customer.save()

        print "Done"


    def exit_error(self, msg):
        self.stderr.write(msg)
        sys.exit(1)
