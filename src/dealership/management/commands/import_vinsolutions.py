import operator

from django.core.management.base import BaseCommand, CommandError
from django.db.models import Q

from dealership.models import Vehicle, VinMake, VinModel, VinYear, VinTrim, DealersVehicle, CustomerVehicle, RO

import csv

class Command(BaseCommand):
    def add_arguments(self, parser):
        # Positional arguments
        parser.add_argument('vehicles',
            help = 'Specify vehicle file in csv format',
        ),

        parser.add_argument('--make',default="BMW",
                            help="Specify make of car to import")

        # Named (optional) arguments
        parser.add_argument(
            '--delete',
            action="store_false",
            help='Delete all vehicles before loading',
        )

    def handle(self, *args, **options):
        # Import Vehicle models and year
        vehicle_file = options['vehicles']



        #for vehicle in vehicles:
        #    print "Make: {}, Val:{}, Model: {}, Val:{}, Year:{}, Val:{}".format(vehicle.make.name, vehicle.make.val,
        #                                                        vehicle.model.name, vehicle.model.val,
        #                                                        vehicle.year.name, vehicle.year.val)
        make = options['make']
        self.load_car_csv(make, vehicle_file)
        print "Done"


    def load_car_csv(self, make, vehicle_file):

        if make:
            cars_query = Q(make__name__iexact=make)
        else:
            cars_query = Q()

        with open(vehicle_file) as f:
            f.next()  # skip header line

            reader = csv.reader(f, delimiter=',')
            sortedlist = sorted(reader, key=operator.itemgetter(2, 1))
            last_make = None
            alldb_makes = VinMake.objects.all()
            last_model = None
            last_year = None
            for row in sortedlist:
                model = row[2]
                year = row[1]

                if model == last_model and last_year == year:
                    # skip if already exists
                    continue

                print "{}, {}, {}".format(make, model, year),

                last_model = model
                last_year = year

                found = Vehicle.objects.filter(make__name__iexact=make,
                                               model__name__iexact=model,
                                               year__name__iexact=year).count() > 0
                if found:
                    print ": Exists".format(make, model, year)
                    continue

                db_make = VinMake.objects.get(val__iexact=make)

                db_model, created = VinModel.objects.get_or_create(val__exact=model)
                if created:
                    db_model.name = db_model.val = model
                    db_model.save()

                db_year, created = VinYear.objects.get_or_create(val__exact=year)
                if created:
                    db_year.name = db_year.val = year
                    db_year.save()

                new_vehicle, created = Vehicle.objects.get_or_create(make=db_make, model=db_model, year=db_year)
                if created:
                    new_vehicle.save()
                    print ": ***  Added".format(make, model, year)
                else:
                    print ": **** Not added. Shouldn't exist".format(make, model, year)




