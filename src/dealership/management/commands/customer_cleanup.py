from django.contrib.admin.util import NestedObjects
from django.db import DEFAULT_DB_ALIAS

from copy import deepcopy
from django.db.models import Count
from django.db.models.deletion import Collector

from django.core.management.base import BaseCommand, CommandError
from django.core.files.base import ContentFile
from dealership.models import UserProfile, Dealer, Flags, CustomerVehicle, Appointment
from django.contrib.auth.models import Group
import os
import sys


class Command(BaseCommand):
    help = 'Show/Remove customers that have no vehicle'


    def add_arguments(self, parser):
        # Add positional arguments
        parser.add_argument('--cars', nargs='*', default=[0.], type=int,
                            help="Show customers with specified number of cars")
        parser.add_argument('--show', action='store_true', dest='show', default=False,
                            help="Show zero car customers")
        parser.add_argument('--remove', action='store_true', dest='remove', default=False,
                            help="Remove zero car customers")


    def handle(self, *args, **options):

        remove_customers = True if options['remove'] else False
        show_customers = True if options['show'] else False

        if show_customers is not True and remove_customers is not True:
            self.exit_error("No Actions specified: Nothing to do")

        num_cars = options['cars'][0]
        profiles = self.get_customers(num_cars)
        if not profiles:
            self.exit_error("No Potential Customer Profiles Found")

        for profile in profiles:
            # Setup some defaults
            is_customer = True
            has_user = profile.user is not None
            num_groups = 0
            user_name = "None"

            if has_user:
                # User object can access groups
                num_groups = profile.user.groups.all().count()
                is_customer = profile.user.groups.filter(name='Customer').exists() and num_groups == 1
                user_name = profile.user.username

            if is_customer and int(profile.vehicle_count) == int(num_cars):
                cust_details = "User: {}, Name: {} {}, Groups: {}, Vehicles: {}".format(
                                                                                user_name,
                                                                                profile.first_name,
                                                                                profile.last_name,
                                                                                num_groups,
                                                                                profile.vehicle_count)
                profile_collector = Collector(using='default')
                user_collector = Collector(using='default')
                profile_collector.collect([profile])
                user_dependencies = False
                if profile.user:
                    user_collector.collect([profile.user])
                    if user_collector.dependencies:
                        user_dependencies = True


                if show_customers:
                    self.stdout.write(cust_details)
                    appointment_count = Appointment.objects.filter(customer=profile).count()
                    if appointment_count > 0:
                        print "User: {} {} has {} appointments".format(profile.first_name, profile.last_name, appointment_count)
                        appointments = Appointment.objects.filter(customer=profile)
                        for appointment in appointments:
                            collector = NestedObjects(using=DEFAULT_DB_ALIAS)
                            collector.collect([appointment])
                            print "Cascase Appointments Delete:"
                            print collector.nested()
                            if remove_customers:
                                appointment.delete()
                                print "Appointment Removed"
                            else:
                                print "Appointment Would be removed specify --remove to delete appointment"

                            # ++++ Remove ROS as well? -----

                if profile.vehicle_count != 0:
                    self.stdout.write("Profile Has Vehicles and cannot be deleted: Count == {}".format(profile.vehicle_count))

                else:
                    # Candidate for deletion
                    can_delete = True

                    if profile_collector.dependencies or user_dependencies:
                        if show_customers:
                            self.stdout.write("Profile Has Dependencies and cannot be deleted. User = {}, Profile = {}".format(
                                profile_collector.dependencies, user_dependencies))
                        can_delete = False

                    if not remove_customers:
                        can_delete = False
                        print "Will Delete when '--remove' option specified"

                    if can_delete is True:
                        try:
                            if profile.user:
                                profile.user.delete()
                                print "User Object Deleted"

                            profile.delete()
                            print "UserProfile Object Deleted"

                        except Exception as e:
                            print ("Delete Failed: {}".format(e.message))

                self.stdout.write("\n")


            elif has_user and num_groups > 1 and \
                    profile.user.groups.filter(name='Customer').exists() and \
                    profile.vehicle_count == 0:

                # Special case. The registered user has no cars but is a member of the customer group
                # AND belongs to other groups (i.e. They're more than a customer). Remove them
                # from the customer group because they don't have a car
                if remove_customers:
                    g = Group.objects.get(name='Customer')
                    g.user_set.remove(profile.user)
                    print ("Special User {} Has Zero Cars so removed from Customer Group".format(user_name))
                else:
                    print ("Will remove Special User {} from customer group as they have zero cars".format(user_name))






    def get_customers(self, cars):
        profiles = UserProfile.objects.annotate(vehicle_count=Count('customervehicle'))
        return profiles

        # def copy_file(src):
        #     """
        #     Duplicating this object including copying the file
        #     """
        #     new_file = ContentFile(src.read())
        #     new_file.name = src.name
        #     return new_file
        #
        # # copy from BMW delaer
        # # dealer_from = Dealer.objects.get(dealer_code="template")
        # dealer_from = Dealer.objects.get(dealer_code="0000")
        # flags = Flags.objects.filter(dealer=dealer_from)
        # dealer_to = Dealer.objects.get(dealer_code="0001")
        #
        # for flag in flags:
        #     flag.pk = None
        #     flag.dealer = dealer_to
        #     flag.save()

    def exit_error(self, msg):
        self.stderr.write(msg)
        sys.exit(1)
