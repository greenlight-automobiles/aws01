# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('dealership', '0020_auto_20190810_0822'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='inspectioncategories',
            options={'verbose_name_plural': 'Inspection Categories'},
        ),
        migrations.AlterModelOptions(
            name='inspectioncategoriesitems',
            options={'verbose_name_plural': 'Inspection Categories Items'},
        ),
        migrations.AlterModelOptions(
            name='inspectionitems',
            options={'verbose_name_plural': 'Inspection Items'},
        ),
    ]
