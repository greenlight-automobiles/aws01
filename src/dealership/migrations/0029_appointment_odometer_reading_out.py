# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('dealership', '0028_auto_20190827_2317'),
    ]

    operations = [
        migrations.AddField(
            model_name='appointment',
            name='odometer_reading_out',
            field=models.CharField(max_length=255, null=True, blank=True),
        ),
    ]
