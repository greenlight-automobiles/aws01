# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('dealership', '0031_auto_20190831_0057'),
    ]

    operations = [
        migrations.AddField(
            model_name='flags',
            name='role',
            field=models.CharField(default=b'CM', max_length=3, choices=[(b'CM', b'Custom'), (b'DR', b'Dealer'), (b'AR', b'Advisor'), (b'TN', b'Technician'), (b'PS', b'Parts'), (b'CR', b'Customer')]),
        ),
    ]
