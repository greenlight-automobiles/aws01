# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('dealership', '0023_dealer_default_technician_hourly_rate'),
    ]

    operations = [
        migrations.RenameField(
            model_name='dealer',
            old_name='default_technician_hourly_rate',
            new_name='technician_default_hourly_rate',
        ),
    ]
