# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('dealership', '0015_auto_20190129_0149'),
    ]

    operations = [
        migrations.RenameField(
            model_name='servicerepair',
            old_name='availablity',
            new_name='availability',
        ),
        migrations.AddField(
            model_name='dealer',
            name='disable_email_and_text',
            field=models.BooleanField(default=False),
        ),
    ]
