# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('dealership', '0016_auto_20190213_2233'),
    ]

    operations = [
        migrations.RenameField(
            model_name='dealer',
            old_name='disable_email_and_text',
            new_name='demo_dealership',
        ),
    ]
