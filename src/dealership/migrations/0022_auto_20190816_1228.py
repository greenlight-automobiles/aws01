# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import dealership.support


class Migration(migrations.Migration):

    dependencies = [
        ('dealership', '0021_auto_20190816_1215'),
    ]

    operations = [
        migrations.AddField(
            model_name='dealer',
            name='customer_portal_header_image',
            field=models.ImageField(default=None, null=True, upload_to=dealership.support.dealer_directory_path, blank=True),
        ),
        migrations.AddField(
            model_name='dealer',
            name='default_car_image',
            field=models.ImageField(default=None, null=True, upload_to=dealership.support.dealer_directory_path, blank=True),
        ),
    ]
