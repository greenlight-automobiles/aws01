# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('dealership', '0010_auto_20180802_0513'),
    ]

    operations = [
        migrations.AddField(
            model_name='appointment',
            name='customer_actionplan_typed_full_name',
            field=models.CharField(default=b'', max_length=255, blank=True),
        ),
    ]
