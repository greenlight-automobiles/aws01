# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('dealership', '0007_auto_20180801_0015'),
    ]

    operations = [
        migrations.RenameField(
            model_name='appointment',
            old_name='driver_liscens_number',
            new_name='driver_license_number',
        ),
        migrations.RenameField(
            model_name='driverliscenseisurance',
            old_name='driver_liscens_number',
            new_name='driver_license_number',
        ),
        migrations.AddField(
            model_name='appointment',
            name='customer_actionplan_signature',
            field=models.TextField(default=None, null=True),
        ),
    ]
