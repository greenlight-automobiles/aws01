# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('dealership', '0013_userprofile_additional_contacts'),
    ]

    operations = [
        migrations.RenameField(
            model_name='customervehicle',
            old_name='lisence_number',
            new_name='license_number',
        ),
        migrations.RenameField(
            model_name='customervehicle',
            old_name='milage',
            new_name='mileage',
        ),
    ]
