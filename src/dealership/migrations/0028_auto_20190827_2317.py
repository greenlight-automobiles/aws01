# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('dealership', '0027_auto_20190821_0627'),
    ]

    operations = [
        migrations.RenameField(
            model_name='appointment',
            old_name='odometer_reading',
            new_name='odometer_reading_in',
        ),
    ]
