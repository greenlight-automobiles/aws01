# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('dealership', '0032_flags_role'),
    ]

    operations = [
        migrations.AlterField(
            model_name='flags',
            name='role',
            field=models.CharField(default=b'DR', max_length=3, choices=[(b'DR', b'Dealer'), (b'AR', b'Advisor'), (b'TN', b'Technician'), (b'PS', b'Parts'), (b'CR', b'Customer'), (b'CM', b'Custom')]),
        ),
    ]
