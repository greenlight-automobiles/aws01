# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('dealership', '0003_auto_20180617_0917'),
    ]

    operations = [
        migrations.RenameField(
            model_name='appointment',
            old_name='appointment_recommandation_status',
            new_name='appointment_recommendation_status',
        ),
    ]
