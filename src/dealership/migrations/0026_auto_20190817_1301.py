# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.core.validators


class Migration(migrations.Migration):

    dependencies = [
        ('dealership', '0025_auto_20190817_0157'),
    ]

    operations = [
        migrations.AlterField(
            model_name='servicerepair',
            name='duration',
            field=models.CharField(max_length=10, null=True, validators=[django.core.validators.RegexValidator(regex=b'^([0-9][0-9]|[0-9]):[0-5][0-9]$', message=b'Invalid Time Format. Use HH:MM')]),
        ),
    ]
