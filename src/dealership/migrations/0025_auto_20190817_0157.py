# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('dealership', '0024_auto_20190816_1642'),
    ]

    operations = [
        migrations.AlterField(
            model_name='dealer',
            name='technician_default_hourly_rate',
            field=models.DecimalField(max_digits=6, decimal_places=2),
        ),
    ]
