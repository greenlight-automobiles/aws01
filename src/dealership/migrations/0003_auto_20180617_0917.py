# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('dealership', '0002_capacitycounts_total_advisor'),
    ]

    operations = [
        migrations.AlterField(
            model_name='notifications',
            name='recipient_users',
            field=models.ManyToManyField(default=None, related_name='notifications', to=settings.AUTH_USER_MODEL, blank=True),
        ),
    ]
