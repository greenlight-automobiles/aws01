# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('dealership', '0019_additionalcontactinfo_method_of_contact'),
    ]

    operations = [
        migrations.AlterField(
            model_name='appointment',
            name='alt_contact',
            field=models.OneToOneField(related_name='altcontactapt', null=True, blank=True, to='dealership.AdditionalContactInfo'),
        ),
    ]
