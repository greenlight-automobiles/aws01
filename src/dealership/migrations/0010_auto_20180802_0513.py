# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('dealership', '0009_auto_20180801_0053'),
    ]

    operations = [
        migrations.RenameField(
            model_name='appointmentrecommendation',
            old_name='recommnded_by',
            new_name='recommended_by',
        ),
    ]
