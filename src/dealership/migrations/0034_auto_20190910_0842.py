# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('dealership', '0033_auto_20190909_1106'),
    ]

    operations = [
        migrations.AddField(
            model_name='userprofile',
            name='sms_opt_in_agreed',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='userprofile',
            name='sms_opt_in_state',
            field=models.CharField(default=b'NS', max_length=3, choices=[(b'NS', b'Not Sent'), (b'ST', b'Sent'), (b'AN', b'Answered')]),
        ),
    ]
