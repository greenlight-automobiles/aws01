# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.core.validators


class Migration(migrations.Migration):

    dependencies = [
        ('dealership', '0011_appointment_customer_actionplan_typed_full_name'),
    ]

    operations = [
        migrations.CreateModel(
            name='AdditionalContactInfo',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('first_name', models.CharField(default=b'', max_length=255, null=True)),
                ('last_name', models.CharField(default=b'', max_length=255, null=True)),
                ('phone_number_1', models.CharField(default=None, max_length=2000, blank=True, validators=[django.core.validators.RegexValidator(regex=b'^\\+?1?(\\d|-){1,200}$', message=b'Invalid Phone Number')])),
                ('phone_1_type', models.CharField(default=b'Mobile', max_length=255, choices=[(b'Mobile', 'Mobile'), (b'Home', 'Home'), (b'Work', 'Work')])),
                ('email_1', models.EmailField(default=None, max_length=255, null=True)),
            ],
        ),
    ]
