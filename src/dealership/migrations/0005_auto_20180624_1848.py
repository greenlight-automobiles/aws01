# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import core.django.fields.hash


def forwards_func(apps, schema_editor):
    # We get the model from the versioned app registry;
    # if we directly import it, it'll be the wrong version
    Appointment = apps.get_model('dealership', 'Appointment')
    for appt in Appointment.objects.all():
        # This will generate hashes
        appt.save()

    RO = apps.get_model('dealership', 'RO')
    for ro in RO.objects.all():
        # This will generate hashes
        ro.save()

class Migration(migrations.Migration):

    dependencies = [
        ('dealership', '0004_auto_20180617_1902'),
    ]

    operations = [
        migrations.AddField(
            model_name='appointment',
            name='status_url_hash',
            field=core.django.fields.hash.HashURLField(db_index=True, max_length=56, editable=False, blank=True),
        ),
        migrations.AddField(
            model_name='ro',
            name='ro_url_hash',
            field=core.django.fields.hash.HashURLField(db_index=True, max_length=56, editable=False, blank=True),
        ),
        migrations.RunPython(forwards_func),
    ]
