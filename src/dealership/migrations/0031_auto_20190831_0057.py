# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import colorfield.fields


class Migration(migrations.Migration):

    dependencies = [
        ('dealership', '0030_auto_20190828_0622'),
    ]

    operations = [
        migrations.AddField(
            model_name='dealer',
            name='advisor_flag_color_default',
            field=colorfield.fields.ColorField(default=b'#B24C53', max_length=18),
        ),
        migrations.AddField(
            model_name='dealer',
            name='customer_flag_color_default',
            field=colorfield.fields.ColorField(default=b'#AEFFB0', max_length=18),
        ),
        migrations.AddField(
            model_name='dealer',
            name='dealer_flag_color_default',
            field=colorfield.fields.ColorField(default=b'#0091FF', max_length=18),
        ),
        migrations.AddField(
            model_name='dealer',
            name='parts_flag_color_default',
            field=colorfield.fields.ColorField(default=b'#8AFFF3', max_length=18),
        ),
        migrations.AddField(
            model_name='dealer',
            name='technician_flag_color_default',
            field=colorfield.fields.ColorField(default=b'#FFD2AB', max_length=18),
        ),
    ]
