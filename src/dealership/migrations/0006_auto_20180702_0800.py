# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('dealership', '0005_auto_20180624_1848'),
    ]

    operations = [
        migrations.AlterField(
            model_name='creditcardinfo',
            name='card_exp_year',
            field=models.CharField(max_length=255, null=True, choices=[(b'2017', '2017'), (b'2018', '2018'), (b'2019', '2019'), (b'2020', '2020'), (b'2021', '2021'), (b'2022', '2022'), (b'2023', '2023')]),
        ),
    ]
