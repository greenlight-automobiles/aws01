# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('dealership', '0014_auto_20190112_0253'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='InspectionCatagories',
            new_name='InspectionCategories',
        ),
        migrations.RenameField(
            model_name='InspectionCategoriesItems',
            old_name='InspectionCatagories',
            new_name='InspectionCategories'
        ),
    ]
