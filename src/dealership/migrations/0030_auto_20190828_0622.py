# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('dealership', '0029_appointment_odometer_reading_out'),
    ]

    operations = [
        migrations.AlterField(
            model_name='appointment',
            name='odometer_reading_in',
            field=models.CharField(default=b'', max_length=255, blank=True),
        ),
        migrations.AlterField(
            model_name='appointment',
            name='odometer_reading_out',
            field=models.CharField(default=b'', max_length=255, blank=True),
        ),
    ]
