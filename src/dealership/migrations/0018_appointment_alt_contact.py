# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('dealership', '0017_auto_20190214_0846'),
    ]

    operations = [
        migrations.AddField(
            model_name='appointment',
            name='alt_contact',
            field=models.OneToOneField(related_name='alt_contact', null=True, blank=True, to='dealership.AdditionalContactInfo'),
        ),
    ]
