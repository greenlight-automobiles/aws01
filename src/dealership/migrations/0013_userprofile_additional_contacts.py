# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('dealership', '0012_additionalcontactinfo'),
    ]

    operations = [
        migrations.AddField(
            model_name='userprofile',
            name='additional_contacts',
            field=models.ForeignKey(default=None, to='dealership.AdditionalContactInfo', null=True),
        ),
    ]
