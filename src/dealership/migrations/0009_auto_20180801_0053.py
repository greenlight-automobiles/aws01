# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('dealership', '0008_auto_20180801_0049'),
    ]

    operations = [
        migrations.RenameModel('DriverLiscenseIsurance', 'DriverLicenseInsurance'),
    ]
