# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('dealership', '0022_auto_20190816_1228'),
    ]

    operations = [
        migrations.AddField(
            model_name='dealer',
            name='default_technician_hourly_rate',
            field=models.FloatField(default=0.0),
        ),
    ]
