# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('dealership', '0006_auto_20180702_0800'),
    ]

    operations = [
        migrations.RenameField(
            model_name='appointment',
            old_name='customer_signatures',
            new_name='customer_checkin_signature',
        ),
    ]
