# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('dealership', '0018_appointment_alt_contact'),
    ]

    operations = [
        migrations.AddField(
            model_name='additionalcontactinfo',
            name='method_of_contact',
            field=models.CharField(default=b'Email', max_length=20, null=True, choices=[(b'Email', 'Email'), (b'Text', 'Text'), (b'Call', 'Call')]),
        ),
    ]
