from django import template
register = template.Library()

from core.utils.get_vehicle_image import get_car_image_default, get_car_image_or_default

@register.simple_tag(takes_context=True)
def get_vehicle_image(context, vehicle):
    dealer_code = context['dealer_code']
    return get_car_image_or_default(dealer_code, vehicle)

@register.simple_tag(takes_context=True)
def get_default_car_image(context):
    return get_car_image_default(context["dealer_code"])
