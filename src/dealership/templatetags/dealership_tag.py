from django import template
from dealership.models import Dealer
from dealership.factories import DealerShipServicesFactory


register = template.Library()

@register.filter(name='checkinlist')
def checkinlist(item, arg):
    print arg , item
    desired_array = [int(numeric_string) for numeric_string in item]
    return arg in desired_array


@register.assignment_tag(takes_context=True)
def dealer_capacity(context, day):
    request = context["request"]

    capacities = {}
    from datetime import timedelta
    import datetime
    from django.utils import timezone
    import pytz

    # Calculate capacity planning for the day
    dealer_factory = DealerShipServicesFactory()
    capacityservice = dealer_factory.get_instance("capacity")

    weekday = datetime.datetime.strptime(request.POST.get('date'), '%Y-%m-%d')
    dealer_id = request.session["dealer_id"]
    dealer = Dealer.objects.get(id=dealer_id)

    dealer_timezone = pytz.timezone(dealer.timezone)
    dealer_time = timezone.now().astimezone(dealer_timezone)


    if weekday.date() < dealer_time.date():
        return None

    capacity_percentage = capacityservice.get_capacity_planning_percentage(request.session["dealer_id"], weekday)
    if capacity_percentage is None:
        return None

    return int(round(capacity_percentage))



