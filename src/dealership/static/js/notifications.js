/*
Massive quick and dirty one day task to implement notifications. Needs proper intrgration with the rest of the system
and to consodate cookie processing into one place.
*/


function reset_live_notifications(url) {
    var self = this

    // set message display parameters.
    toastr.options = {
      "closeButton": true,
      "debug": false,
      "newestOnTop": false,
      "progressBar": false,
      "positionClass": "toast-bottom-right",
      "preventDuplicates": false,
      "onclick": null,
      "showDuration": "1000",
      "hideDuration": "1000",
      "timeOut": "0",
      "extendedTimeOut": "0",
      "showEasing": "swing",
      "hideEasing": "linear",
      "showMethod": "fadeIn",
      "hideMethod": "fadeOut"
    }

	this.getCookie = function(name){
	    var cookieValue = null;
	    if (document.cookie && document.cookie != '') {
	        var cookies = document.cookie.split(';');
	        for (var i = 0; i < cookies.length; i++) {
	            var cookie = jQuery.trim(cookies[i]);
	            // Does this cookie string begin with the name we want?
	            if (cookie.substring(0, name.length + 1) == (name + '=')) {
	                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
	                break;
	            }
	        }
	    }
	    return cookieValue;
	};

	this.csrfSafeMethod =  function(method){
	    // these HTTP methods do not require CSRF protection
	    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
	};


    var csrftoken = self.getCookie('csrftoken');
    // Get any live messages from server every 1 min
    var data = {};
    $.ajax({
        url: url,
        beforeSend: function(xhr, settings) {
            if (!self.csrfSafeMethod(settings.type) && !this.crossDomain) {
                xhr.setRequestHeader("X-CSRFToken", csrftoken);
            }
        },
        dataType: "json",
        type: 'GET',
        data: data,
        accept : 'application/json',
        global: false,
        success: function(data){
            // Present any live messages obtained from the server.
            // salesGauge.setValue(data.value);
            get_live_messages(url_get_notifications);
            //Setup the next poll recursively
        },

        error: function (xhr, ajaxOptions, thrownError) {
            console.log("Reset Notifications Failed");
            console.log(xhr.status);
            console.log(thrownError);
            get_live_messages(url_get_notifications); // Try anyway.

        },
    });
}


function get_live_messages(url){
    var self = this
	this.getCookie = function(name){
	    var cookieValue = null;
	    if (document.cookie && document.cookie != '') {
	        var cookies = document.cookie.split(';');
	        for (var i = 0; i < cookies.length; i++) {
	            var cookie = jQuery.trim(cookies[i]);
	            // Does this cookie string begin with the name we want?
	            if (cookie.substring(0, name.length + 1) == (name + '=')) {
	                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
	                break;
	            }
	        }
	    }
	    return cookieValue;
	};

	this.csrfSafeMethod =  function(method){
	    // these HTTP methods do not require CSRF protection
	    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
	};

   setTimeout(function(){
        var csrftoken = self.getCookie('csrftoken');
        // Get any live messages from server every 1 min
        var data = {};
        $.ajax({
            url: url,
            beforeSend: function(xhr, settings) {
                if (!self.csrfSafeMethod(settings.type) && !this.crossDomain) {
                    xhr.setRequestHeader("X-CSRFToken", csrftoken);
                }
            },
            dataType: "json",
            type: 'GET',
            data: data,
            accept : 'application/json',
            global: false,
            success: function(data){
                // Present any live messages obtained from the server.
                // salesGauge.setValue(data.value);
                if (data.result == "OK") {
                    for(var i = 0; i < data.messages.length; i++) {
                        var obj = data.messages[i];
                        toastr.info(obj.message);
                        }
                    }

                //Setup the next poll recursively
                get_live_messages(url);

            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log("Reset Notifications Failed");
                console.log(xhr.status);
                console.log(thrownError);
                //Setup the next poll recursively
                get_live_messages(url); // Try again.
            },

        });
  }, 30000);
}
