from dealership.models import Notifications
from dealership.factories import DealerShipServicesFactory
from django.http import JsonResponse
from flagging import Constants
from django.utils import timezone


def getRoleOfUser(user):
    role = user.groups.all()
    roleString = ""
    for r in role:
        roleString += r.name + ","
    return roleString


def reset_live_notifications(request):
    # Quick and dirty to send messages to client.
    # Probably should be rolled in to existing messaging system Centrifuge and

    if not request.user.is_authenticated():
        return JsonResponse({'result': "Error"})

    role = getRoleOfUser(request.user)
    if role == "":
        return JsonResponse({'result': "Error"})

    userprofile = request.user.userprofile
    userprofile.last_live_notification = timezone.now()
    userprofile.save()
    return JsonResponse({'result': "OK"})


def get_live_notifications(request):
    if not request.user.is_authenticated():
        return JsonResponse({'result': "Error"})

    dealer_factory = DealerShipServicesFactory()
    notificationservice = dealer_factory.get_instance("notification")

    user = request.user

    userprofile = request.user.userprofile
    lastmessage_timestamp = userprofile.last_live_notification
    if lastmessage_timestamp == None:
        userprofile.last_live_notification = timezone.now()

    role = getRoleOfUser(user)
    if role == "":
        return JsonResponse({'result': "Error"})

    notifications = Notifications.objects.filter(dealer__id=user.userprofile.dealer_id,
                                                 timestamp__gt=lastmessage_timestamp).order_by("timestamp")
    if notifications.count() == 0:
        # Indication time of last message
        userprofile.last_live_notification = timezone.now()
        userprofile.save()
        return JsonResponse({'result': "OK", 'messages': []})

    messages = []
    approved_recipient = False

    for notification in notifications:

        if user != notification.sender:
            # Only send messages if user logged in getting message is not the sender.
            if Constants.TECHNICIAN in role:
                if Notifications.DESTINATION_TECHNICIAN in notification.recipient_groups:
                    approved_recipient = True
            if Constants.ADVISOR in role:
                if Notifications.DESTINATION_ADVISOR in notification.recipient_groups:
                    approved_recipient = True
            if Constants.DEALER in role:
                if Notifications.DESTINATION_DEALER in notification.recipient_groups:
                    approved_recipient = True
            if Constants.PARTS in role:
                if Notifications.DESTINATION_PARTS in notification.recipient_groups:
                    approved_recipient = True

            if notification.recipient_users:
                for destination_user in notification.recipient_users.all():
                    if user == destination_user:
                        approved_recipient = True

            if approved_recipient:
                messages.append(
                    {"message": notification.message, 'timestamp': notification.timestamp, 'id': notification.id})

        lastmessage_timestamp = notification.timestamp

    # Indication time of last message
    userprofile.last_live_notification = lastmessage_timestamp
    userprofile.save()

    return JsonResponse({'result': "OK", 'messages': messages})
