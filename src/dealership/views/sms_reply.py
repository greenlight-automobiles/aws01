from django.views.generic import View
from django.views.decorators.csrf import csrf_exempt
from django.utils.decorators import method_decorator
from django.http import HttpResponse, Http404
from django.core.urlresolvers import reverse
from django.contrib.sites.models import Site
from django.conf import settings

from dealership.factories import DealerShipServicesFactory
from dealership.services.appointmentservices import AppointmentService
from dealership.services.customerservices import CustomerService
from dealership.services.userservices import UserService
from dealership.models import UserProfile



class SMSReply(View):
    """ Returns a response to the Twilio messaging service and passes on the response to interested parties.
    """
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(SMSReply, self).dispatch(request, *args, **kwargs)

    def reply_with_msg(self, msg):
        # if settings.USE_TWILIO:
        dealer_factory = DealerShipServicesFactory()
        notificationservice = dealer_factory.get_instance("notification")

        return HttpResponse(notificationservice.send_text_reply(msg))
        #else:
        #    if settings.DEBUG:
        #        print "TWilio Text Message"
        #        print msg
        #    raise Http404


    def check_sms_opt_in_response(self, customer, incoming_msg_body, from_number):

        service = UserService()

        # Only do the opt-in check on customers
        if not service.is_customer(customer):
            return None

        bool_answered = False
        bool_response_valid = False
        opt_in_answer = incoming_msg_body.lower().strip()
        if customer.sms_opt_in_state == UserProfile.SMS_OPTIN_NOT_SENT_CHOICE:
            # We Haven't sent the opt in so let this message go through first
            return None

        elif customer.sms_opt_in_state == UserProfile.SMS_OPTIN_SENT_CHOICE:
            # Check to see if we're waiting for a response from a user
            if opt_in_answer == "yes":
                bool_answered = True
                bool_response_valid = True
                customer.sms_opt_in_agreed = True
                customer.sms_opt_in_state = UserProfile.SMS_OPTIN_ANSWERED_CHOICE
                msg = "Thank you. We will continue sending you updates through text messages."
            elif opt_in_answer == "no":
                bool_answered = True
                bool_response_valid = True
                customer.sms_opt_in_state = UserProfile.SMS_OPTIN_ANSWERED_CHOICE
                customer.sms_opt_in_agreed = False
                msg = "Thank you. We will no longer send you updates through text messages. You may " \
                        "reset your opt-in settings by replying SMSRESET to this number at any time"
            else:
                # We're wating for an opt-in reposnse but we didn't understand the answer
                bool_answered = True
                msg = "We're sorry but we did not understand your response. Reply YES to keep receiving" \
                        " texts or NO to stop receiving texts"

        elif opt_in_answer == "smsreset":
                bool_answered = True
                bool_response_valid = True
                customer.sms_opt_in_state = UserProfile.SMS_OPTIN_NOT_SENT_CHOICE
                customer.sms_opt_in_agreed = False
                msg = "Text message options are reset."

        if bool_answered is True:

            # The customer answered the SMS OPTIN message so save the results
            if bool_response_valid is True:
                customer.save()
            return self.reply_with_msg(msg)


        return None

    def post(self, request):
        dealer_factory = DealerShipServicesFactory()
        notificationservice = dealer_factory.get_instance("notification")


        incoming_msg_from = request.POST.get('From', None)
        incoming_msg_to = request.POST.get('To', None)
        incoming_msg_body = request.POST.get('Body', None)

        # TODO; Move this operation to a seperate thread

        # Remove Country code
        phone_num = incoming_msg_from.replace("+1", "")

        customer_service = CustomerService()
        customers = customer_service.get_customers_by_phone(phone_num)
        customer = customers[0] if customers else None

        # Check
        response = self.check_sms_opt_in_response(customer, incoming_msg_body, incoming_msg_from)
        if response is not None:
            return response

        apt_service = AppointmentService()
        appointments = apt_service.get_any_appointments_by_phone(phone_num)
        if appointments:
            # Appointment found?, reply saying so.
            for appointment in appointments:
                # Notify relevant parties of text reply message
                notificationservice.send_text_reply_notification(customer, incoming_msg_from, incoming_msg_body,
                                                                 appointment.dealer, appointment)
            if customer.first_name:
                msg = "{}, ".format(customer.first_name)
            msg += "Thankyou for your reply. We will get back to you shortly."

        else:
            # No appointment
            if customer:
                msg = "We're sorry {}, we cannot find your current appointment details.".format(customer.first_name)
                if customer.dealer:
                    current_site = Site.objects.get_current()
                    customer_url = current_site.domain + reverse('customer:login') + \
                                   "dealer_code=" + customer.dealer.dealer_code

                    msg += "Please contact {} to create an appointment or go to {}" \
                          "to create an appointment.".format(customer.dealer.name, customer_url)
                else:
                    # No dealer associated with customer
                    msg += "Please contact your local dealership to create an appointment for you."

            else:
                # If no customer found then just output a generic message.
                msg = "We're sorry, we cannot find your current appointment details. " \
                        "Please contact your local dealership to create an appointment for you."

        # Send message back to customer.
        return self.reply_with_msg(msg)
