from django.contrib import admin
from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import User

from dealership.models import *


# Register your models here.
# Define an inline admin descriptor for Employee model
# which acts a bit like a singleton
class UserProfileInline(admin.StackedInline):
    model = UserProfile
    can_delete = False
    fk_name = 'user'
    verbose_name_plural = 'User Profile'
    if settings.DEBUG:
        exclude = ['active_email','active_email_date','active_phone_number','active_phone_number_date',
                   'phone_number_2', 'phone_number_3', 'phone_1_type', 'phone_2_type', 'phone_3_type', 'additional_contacts',
                   'email_1', 'email_2', 'state_us',
                   'token','token_expiry','number_of_chats','skip_confirmation'
                   'mode_of_sending_updates',"question","answer","mode_of_sending_updates"]
    else:
        exclude = ['active_email','active_email_date','active_phone_number','active_phone_number_date',
                   'phone_number_2', 'phone_number_3', 'phone_1_type', 'phone_2_type', 'phone_3_type',
                   'email_1', 'email_2', 'state_us',
                   'terms_agreed','token','token_expiry','number_of_chats','skip_confirmation', 'additional_contacts',
                   'mode_of_sending_updates',"question","answer","mode_of_sending_updates","skip_confirmation"]


# Define a new User admin
class UserAdmin(UserAdmin):
    inlines = (UserProfileInline, )

# Define a new User admin

class UserProfileAdmin(admin.ModelAdmin):
    list_display = ('first_name', 'last_name')

class VehicleAdmin(admin.ModelAdmin):
    list_display = ('make', 'model', 'year')

class EmailTypesAdmin(admin.ModelAdmin):
    list_display = ('subject', 'name', 'dealer')
    list_filter = ('dealer',)
    verbose_name_plural = 'Email Types'

class EmailMultimediaAdmin(admin.ModelAdmin):
    list_display = ('name', 'dealer', 'multimedia_file')
    list_filter = ('dealer',)
    verbose_name_plural = 'Email Multimedia'

class StatesAdmin(admin.ModelAdmin):
    list_display = ('state_abbr', 'name')

class DealerVehicleAdmin(admin.ModelAdmin):
    pass

class DealerAdmin(admin.ModelAdmin):
    pass

class AppointmentAdmin(admin.ModelAdmin):
    list_display = ('start_time', 'checkin_time', 'customer', 'vehicle', 'appointment_status', 'ro', 'status_url_hash')
    order_by = ('start_time')
    list_filter = ('dealer', 'appointment_status',)

class FlagsAdmin(admin.ModelAdmin):
    list_display = ('name', 'dealer', 'type')
    list_filter = ('dealer', 'type',)


class FlagsHistoryAdmin(admin.ModelAdmin):
    list_display = ('ro', 'flag', 'created_at', 'created_by',)

class AppointmentInlineAdmin(admin.TabularInline):
    model = Appointment
    extra = 0
    max_num = 1

class RoAdmin(admin.ModelAdmin):
    list_display = ('ro_date', 'ro_number', 'rfid_tag', 'ro_status', 'inspector', 'ro_completed', 'ro_url_hash')
    show_change_link = True
    inlines = (AppointmentInlineAdmin,)

class CustomerVehicleAdmin(admin.ModelAdmin):

    pass

class DealerServicesAdmin(admin.ModelAdmin):
    pass
class AppointmentServiceAdmin(admin.ModelAdmin):
    pass
class AppointmentRecommendationAdmin(admin.ModelAdmin):
    pass
class AppointmentStatusAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', )

class QuestionsAdmin(admin.ModelAdmin):
    pass

class TimeZonesAdmin(admin.ModelAdmin):
    list_display = ('name', 'timezone')

class NotificationsAdmin(admin.ModelAdmin):
    list_display = ('timestamp', 'message', 'dealer')
    list_filter = ('dealer',)

class EmailQueueAdmin(admin.ModelAdmin):
    list_display = ('created_time', 'subject', 'dealer', 'mail_to', 'status',
                    'sent_time', 'mail_error', 'mail_retries', 'mail_error_detail', )
    list_filter = ('dealer',)

class EmailQueueAdmin(admin.ModelAdmin):
    list_display = ('created_time', 'subject', 'dealer', 'mail_to', 'status',
                    'sent_time', 'mail_error', 'mail_retries', 'mail_error_detail', )
    list_filter = ('dealer',)

class InspectionPackageAdmin(admin.ModelAdmin):
    list_display = ('package', 'dealer', )
    list_filter = ('dealer', )

class InspectionCategoriesAdmin(admin.ModelAdmin):
    list_display = ('category', 'package', )
    list_filter = ('package__dealer', )

class InspectionItemsAdmin(admin.ModelAdmin):
    list_display = ('item',)

class InspectionCategoriesItemsAdmin(admin.ModelAdmin):
    list_display = ('item', 'category',)
    list_filter = ('category__package__dealer', 'category', )

# Re-register UserAdmin
admin.site.unregister(User)
admin.site.register(User, UserAdmin)
admin.site.register(UserProfile, UserProfileAdmin)

admin.site.register(Vehicle, VehicleAdmin)
admin.site.register(States, StatesAdmin)
admin.site.register(DealersVehicle, DealerVehicleAdmin)
admin.site.register(Appointment, AppointmentAdmin)
admin.site.register(Dealer, DealerAdmin)

admin.site.register(Flags, FlagsAdmin)
admin.site.register(RO, RoAdmin)
admin.site.register(CustomerVehicle, CustomerVehicleAdmin)
admin.site.register(ServiceRepair, DealerServicesAdmin)
admin.site.register(AppointmentService, AppointmentServiceAdmin)
admin.site.register(AppointmentRecommendation, AppointmentRecommendationAdmin)
admin.site.register(EmailTypes, EmailTypesAdmin)
admin.site.register(AppointmentStatus, AppointmentStatusAdmin)
admin.site.register(Questions, QuestionsAdmin)
admin.site.register(EmailMultimedia, EmailMultimediaAdmin)
admin.site.register(TimeZones, TimeZonesAdmin)
admin.site.register(Notifications, NotificationsAdmin)
admin.site.register(FlagsHistory, FlagsHistoryAdmin)
admin.site.register(EmailQueue, EmailQueueAdmin)
admin.site.register(InspectionPackage, InspectionPackageAdmin)
admin.site.register(InspectionItems, InspectionItemsAdmin)
admin.site.register(InspectionCategories, InspectionCategoriesAdmin)
admin.site.register(InspectionCategoriesItems, InspectionCategoriesItemsAdmin)


InspectionCategoriesItems
