def dealer_directory_path(instance, filename):
    import os
    # file will be uploaded to MEDIA_ROOT/dealer_<id>/<filename>
    # filename_base, file_extension = os.path.splitext(filename)
    return 'email_media/dealer_{0}/{1}'.format(instance.id, filename)

def get_email_dealer_info(dealer):
    from dealership.services.managedealer import ManageDealer
    from dealership.models import EmailMultimedia

    dealer_number = None
    dealer_email = None
    md = ManageDealer()

    # Add website
    contacts = md.getcontacts(dealer)
    if contacts:
        first_entry = contacts.first();
        dealer_number = first_entry.phone_work
        dealer_email = first_entry.email

    header=None
    if dealer.email_header_image:
        try:
            email_multimedia = EmailMultimedia.objects.get(name="header", dealer=dealer)
            header = email_multimedia.multimedia_file
        except EmailMultimedia.DoesNotExist:
            header = None

    else:
        header = None
    context = {
            "header": header,
            "dealer_number": dealer_number,
            "dealer_email": dealer_email,
            "dealer_name": dealer.name,
            "dealer_address": dealer.address_line1 + " " + dealer.address_line2
    }
    return context
