from cgitb import text
import email
import json

from django.core.mail import send_mail
from django.core.mail.message import EmailMultiAlternatives
from django.template.context import Context
from django.template.loader import get_template, render_to_string
from django.utils import timezone
from django.utils.text import phone2numeric
import phonenumbers

from django.conf import settings
from dealership.models import ReminderSettings, EmailTypes, Notifications
from dealership.services.emailservice import EmailService
from dealership.services.twilio_service import TwillioService
from dealership.models import UserProfile
from twilio.twiml.messaging_response import MessagingResponse
from dealership.services.userservices import UserService


class NotificationService():

    REMINDER_TYPES = (1,2,3,4)

    SCHEDULE_APPOINTMENT_TYPE = 1
    FLAGGING_APPOINTMENT_TYPE = 4
    frm_text= TwillioService.default_number
    def send_dealer_based_notification(self,dealer,profile,context,email_type,send_email=True,send_text=False,attachment=None):

        if dealer.demo_dealership is True:
            return True

        try:

            frm = dealer.from_email
            email = profile.active_email
            if email==None or email =="":
                email = profile.email_1
            if email and (send_email == True ):
                try:
                    emailtype = EmailTypes.objects.get(name=email_type,dealer_id=dealer.id)
                except Exception,e:
                    emailtype = None
                if emailtype:
                    emailservice = EmailService()
                    subject = emailtype.subject
                    date_time = timezone.now()
                    emailservice.savequeue(frm , email, date_time , "", "" , emailtype , subject , context,dealer.id,attachment)

            if send_text is True:
                self.send_text_notification(profile,dealer,emailtype,context)

            return True
        except Exception,e:
            print "error sending notification"
            print e
            return False

    def send_notification(self,profile,dealer,frm,template_email,template_text,subject,context,frm_text=None,send_email=True):
        if dealer.demo_dealership is True:
            return True

        try:

            email = profile.active_email
            if email==None or email =="":
                email = profile.email_1
            if email and (send_email == True ):
                self.send_email(template_email, context, email, frm, subject)

            return True
        except Exception,e:
            print "error sending notification"
            print e
            return False

    def check_opt_in_sent(self, profile, number, dealer):

        # Only do the opt-in check on customers
        service = UserService()
        if not service.is_customer(profile):
            return


        if profile.sms_opt_in_state == UserProfile.SMS_OPTIN_NOT_SENT_CHOICE:
            msg = "Hi, {} uses this communication tool with our customers. Reply YES to keep receiving" \
                  " texts or NO to stop receiving texts.".format(dealer.name)
            self.send_text_native(msg, number)
            profile.sms_opt_in_state = UserProfile.SMS_OPTIN_SENT_CHOICE
            profile.save()

    def ok_to_send_text(self, profile):

        service = UserService()

        # Only do the opt-in check on customers
        if not service.is_customer(profile):
            return True

        if profile.sms_opt_in_state == UserProfile.SMS_OPTIN_NOT_SENT_CHOICE:
            # We Haven't sent the opt in so let this message go through first
            return True

        elif profile.sms_opt_in_state == UserProfile.SMS_OPTIN_SENT_CHOICE:
            # Still waiting for a response
            return False

        elif profile.sms_opt_in_state == UserProfile.SMS_OPTIN_ANSWERED_CHOICE:
            return profile.sms_opt_in_agreed


    def send_text_notification(self,profile,dealer,emailobj,context):
        if dealer.demo_dealership is True:
            return

        # Check to see if the customer accepts texts
        if self.ok_to_send_text(profile) is not True:
            return

        phone_number = profile.active_phone_number
        template ='text/'+emailobj.template
        if phone_number== None :
            phone_number =profile.phone_number_1
        if phone_number:
            self.send_text(template,context,phone_number,self.frm_text)

        # Send opt in message after current message if not yet sent
        self.check_opt_in_sent(profile, phone_number, dealer)

    def get_scheduled_settings(self,profile):
        try:
            settings = self.get_user_remindersettings_for(self.SCHEDULE_APPOINTMENT_TYPE,profile)
            return {"text":settings.text,"email":settings.email,"phone":settings.phone}
        except Exception,e:
            print e
            return {"text":False,"email":False,"phone":False}



    def get_flagging_settings(self,profile):

        try:
            settings = self.get_user_remindersettings_for(self.FLAGGING_APPOINTMENT_TYPE,profile)
            return {"text":settings.text,"email":settings.email,"phone":settings.phone}
        except Exception,e:
            print e
            return {"text":True,"email":True,"phone":True}

    def convert_to_e164(self,raw_phone):
        if not raw_phone:
            return

        if raw_phone[0] == '+':
            # Phone number may already be in E.164 format.
            parse_type = None
        else:
            # If no country code information present, assume it's a US number
            parse_type = "US"

        phone_representation = phonenumbers.parse(raw_phone, parse_type)
        return phonenumbers.format_number(phone_representation,
            phonenumbers.PhoneNumberFormat.E164)


    def send_live_notification(self,sender,dealer,recipients=None,groups=None,message=None):
        try:
            notification = Notifications(sender=sender,dealer=dealer,recipient_groups=groups,message=message)
            notification.save()
            if recipients and len(recipients) > 0:
                for user in recipients:
                    notification.recipient_users.add(user)
                notification.save()

        except Exception, e:
            print "ERROR: Failed sending live notification"
            print e
            return False


    def send_text_reply(self,msg):
        # Sends a context to be rendered
        resp = MessagingResponse()
        msg = resp.message(msg)
        # Add dealership image here
        # msg.media("Link To Image")
        return str(resp)


    def send_text_native(self,msg,to):
        # Sends a string text message
        from django.conf import settings
        try:
            to = str(to).replace('-', '').replace(' ', "").replace('(', "").replace(')', "")
            # to = self.convert_to_e164(to)

            # TODO: Dealers need to have a country code specified. Prefix should be added to from a
            #  dealer setting or a customer country code settings. Country can be set by default.

            if len(to) <= 10:
                to = "1" + to
            # begin with US code
            if to.startswith("+") is False:
                to = "+" + to

            # msg_html = render_to_string(template, context)
            twilio = TwillioService()
            frm_text = TwillioService.default_number
            if settings.USE_TWILIO:
                # Bail if we're not using twilio
                print "Sending text"
                twilio.send_message(msg, to, frm_text)
            else:
                print ("TWILIO DEBUG")
                print ("To {}, From {}".format(str(to), str(frm_text)))
                print ("Msg {}".format(msg))
            #             send_mail(subject, msg_plain , settings.EMAIL_HOST_USER , [to] ,html_message=msg_html, fail_silently=False)

            print "message sent"
        except Exception, e:
            print "error sending text"
            print e
            return False


    def send_text(self,template,context,to,frm):
        # Sends a context to be rendered
        if isinstance(context, basestring):
            context = json.loads(context)
        msg_plain = render_to_string(template, context)
        return self.send_text_native(msg_plain,to)

    def send_email(self,template,context,to,frm,subject):
        try:
            msg_plain = render_to_string(template,context)
            msg_html = render_to_string(template, context)
            send_mail(subject, msg_plain , frm , [to] ,html_message=msg_html, fail_silently=False)
            print "Mail Sent"
        except Exception,e:
            print "error sending email"
            print e
            return False

    def save_reminder_settings(self,id,email,text,phone):
        try:
            reminder =ReminderSettings.objects.get(id=id)
            reminder.email = email
            reminder.text   = text
            reminder.phone = phone
            reminder.save()
            return True
        except Exception,e:
            print "erererer"
            print e
            return False


    def send_text_reply_notification(self, customer, from_phone, msg, dealer, appointment=None):
        """

        :param customer: The Customer who is replying to the text message
        :param dealer: The dealer
        :param appointment: (Optional) The scheduled appointment
        :return:
        """
        from dealership.services.managedealer import ManageDealer
        from dealership.services.userservices import UserService
        from dealership.factories import DealerShipServicesFactory

        managedealerservices = ManageDealer()
        userservices = UserService()
        customer_email = userservices.get_primary_email(customer)
        contacts = managedealerservices.getcontacts(dealer.id)
        template = "email/generic/text_reply.html"
        context = {
            'customer': "{} {}".format(customer.first_name, customer.last_name),
            'phone': from_phone,
            'email': customer_email,
            'msg': msg,
        }
        subject = "Text Message Reply Received from {} {}".format(customer.first_name, customer.last_name)
        msg = "Message received from {} {}. \n" \
              "Phone: {}\n" \
              "Email: {}\n\n" \
              "{}".format(customer.first_name, customer.last_name, from_phone, customer_email, msg)

        for contact in contacts:
            if contact.email:
                # Send text to dealership contact
                if contact.phone_cell:
                    self.send_text_native(msg, contact.phone_cell)
                # Send email to dealership contact
                if contact.email:
                    try:
                        msg_plain = render_to_string(template, context)
                        send_mail(subject, msg_plain, settings.EMAIL_HOST_USER, [contact.email, ], fail_silently=False)
                    except Exception, e:
                        print "send_text_reply_notification error sending email"
                        print e

        # Send text to customer advisor
        if appointment and appointment.advisor:
            advisor = appointment.advisor.userprofile
            if advisor.phone_number_1:
                self.send_text_native(msg, advisor.phone_number_1)
            advisor_email = userservices.get_primary_email(advisor)
            if advisor_email:
                try:
                    msg_plain = render_to_string(template, context)
                    send_mail(subject, msg_plain, settings.EMAIL_HOST_USER, [advisor_email,], fail_silently=False)
                except Exception, e:
                    print "send_text_reply_notification error sending email"
                    print e
            # Send a popup notification as well.
            dealer_factory = DealerShipServicesFactory()

            dealer_factory.get_instance("notification")
            notificationservice = dealer_factory.get_instance("notification")
            recipients = [advisor.user,]
            notificationservice.send_live_notification(sender=customer.user, dealer=dealer, recipients=recipients,
                                                       message=msg)

    def get_user_remindersettings(self,profile,create_if_not=False):
        try:
            reminders = ReminderSettings.objects.filter(type_id__in = self.REMINDER_TYPES,customer_id=profile.id)
            if (reminders == False or len(reminders) <4) and create_if_not:
                return self.create_reminders_default(profile)
            return reminders
        except Exception,e:
            print e
            return None


    def get_user_remindersettings_for(self,reminder_type_id,profile):
        try:
            reminder = ReminderSettings.objects.get(type_id = reminder_type_id,customer_id = profile.id)
            return reminder
        except Exception,e:
            return None

    def create_reminders_default(self,profile):
        for reminder in self.REMINDER_TYPES:
                    self.create_reminder_setting_for(reminder, profile)
        return self.get_user_remindersettings(profile)



    def create_reminder_setting_for(self,reminder_type_id,profile):
        try:
            reminder = ReminderSettings.objects.get(type_id = reminder_type_id,customer_id = profile.id)
            return reminder
        except Exception,e:
            print e
            reminder = ReminderSettings()
            reminder.type_id = reminder_type_id
            reminder.customer = profile
            reminder.save()
            return reminder


