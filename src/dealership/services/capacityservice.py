from __builtin__ import False, True
from datetime import timedelta
import datetime
import pytz
import re

from functools import wraps
import test
import urlparse

from django.contrib.auth.decorators import user_passes_test
from django.db.models.query_utils import Q
from django.http import HttpResponseRedirect
from django.http import HttpResponseRedirect
from django.shortcuts import resolve_url
from django.utils import timezone
from django.utils.decorators import available_attrs



from django.conf import settings
from dealership.models import AdvisorCapacity, AdvisorRestrictions, \
    CapacityCounts


class CapacityService():

    def setservices(self):
        from customer.factories import CustomerServicesFactory
        self.customer_service_factory = CustomerServicesFactory()
        from dealership.factories import DealerShipServicesFactory
        self.customer_service_factory = CustomerServicesFactory()
        self.dealer_service_factory = DealerShipServicesFactory()
        self.cuserservice = self.customer_service_factory.get_instance("user")
        self.userservice = self.dealer_service_factory.get_instance("user")
        self.appointment_service = self.dealer_service_factory.get_instance("appointment")
        self.vehicle_service = self
        self.repair_service = self.dealer_service_factory.get_instance("repair")
        self.dealership_service = self.dealer_service_factory.get_instance("dealership")


    def get_available_slabs_for(self,slab_day,dealer,advisor=None):

        self.setservices()
        timings = self.dealership_service.get_dealer_shop_time(slab_day, dealer.id)
        now = timezone.now()
        available_slabs = []
        if timings:
            slab = timings["open_time"]
            while slab < timings["close_time"]:
                slab_time_obj = slab_day.strftime('%Y-%m-%d ')+slab.time().strftime('%H:%M')
                slab_time_obj = timezone.make_aware(datetime.datetime.strptime(slab_time_obj, '%Y-%m-%d %H:%M'))
                if slab_time_obj > now:
                    slab_detail = {"value":slab_day.strftime('%Y-%m-%d ')+slab.time().strftime('%H:%M'),"name":slab_day.strftime('%a %b %d, ')+slab.time().strftime('%I:%M %p')}
                    if self.check_slab_availibity(slab, dealer,None ):
                        if advisor:
                                if self.check_slab_for_advisor(dealer, slab, advisor, None) :
                                    available_slabs.append(slab_detail)
                        else:
                            available_slabs.append(slab_detail)
                slab = slab + datetime.timedelta(minutes = timings["slot"])
        return   available_slabs


    def get_capacity_for_slab(self,slab_time,dealer):
        total_advisors = self.get_available_advisor_count_for_slab_db(slab_time, dealer)

        return total_advisors

    def get_advisor_capacity_for_date(self,slab_time,dealer):
        total_advisors = self.get_available_advisors_for_date(slab_time, dealer)

        shop_timings = self.dealership_service.get_dealer_shop_time(slab_time, dealer.id)

        capacity_per = 100
        hours = 9

        if shop_timings:
            capacity_per = shop_timings["capacity"]
            difference = shop_timings["close_time"] - shop_timings["open_time"]  # difference is of type timedelta

            hours = difference.seconds / 60 / 60  # convert seconds into hour

        total_capacity = (hours * len(total_advisors) * 3) * capacity_per / 100

        return total_capacity


    def get_tech_capacity_for_date(self,slab_time,dealer):
        total_techs = self.get_available_techs_for_date(slab_time, dealer)

        shop_timings = self.dealership_service.get_dealer_shop_time(slab_time,dealer.id)

        capacity_per = 100
        hours = 9

        if shop_timings:
            capacity_per = shop_timings["capacity"]
            difference = shop_timings["close_time"] - shop_timings["open_time"]  # difference is of type timedelta

            hours = difference.seconds / 60 / 60  # convert seconds into hour


        total_capacity = (hours * len(total_techs) * 3) * capacity_per/100

        return total_capacity




    def get_capacity_planning_percentage(self,dealer_id,slab_time):
        # This is a one day hack to get capacity planning implemented in an urgent fashion.
        # Everything about is wrong.  In the future this needs to be integrated with the current API and systems.
        # Capacity fields should be used that are scattered through slabs etc if possible.

        from flagging.services.RoServices import RoServices
        from dealership.models import CreditCardInfo, Appointment, AppointmentStatus, Flags, \
            WayAway, UserProfile, Dealer, RO, AppointmentRecommendation, AppointmentService, RoInspection
        from dealership.services.userservices import UserService
        import pytz

        self.setservices()
        capacity_per = 100
        shop_timings = self.dealership_service.get_dealer_shop_time(slab_time,dealer_id)

        target_date = slab_time.date()

        shop_hours = shop_timings['close_time']-shop_timings['open_time']


        dealer = self.dealership_service.get_dealer_by_id(dealer_id)
        #if dealer and dealer.timezone:
        #    timezone.activate(dealer.timezone)
        dealer_timezone = pytz.timezone(dealer.timezone)
        dealer_time = timezone.now().astimezone(dealer_timezone)
        dealer_time_current = dealer_time.time()
        open_time = shop_timings['open_time'].time()
        close_time = shop_timings['close_time'].time()

        if dealer_time_current > close_time:
            remaining_day_fraction = 0.0
        elif dealer_time_current < open_time:
            remaining_day_fraction = 1.0
        else:
            hours_gone = datetime.datetime.combine(datetime.date.today(),dealer_time_current) - datetime.datetime.combine(datetime.date.today(), open_time)
            remaining_day_fraction = 1.0 - (float(hours_gone.seconds) / float(shop_hours.seconds))


        def GetMinsFromText(hm):
            result = 0
            # We're not converting a float
            for i, x in enumerate(reversed(re.split(r'[:.]\s*', hm))):
                if x:
                    result += int(x) * 60 ** i
            return result

        def get_duration_for_apts(appointments):

            days_duration = float(0)
            for apt in appointments:

                # Get Appointment Services
                appointmentServices = AppointmentService.objects.filter(appointment=apt)
                for aptservice in appointmentServices:
                    if aptservice.service.duration:
                        days_duration += float(GetMinsFromText(aptservice.service.duration))

                # Add up Recommendations
                recommendations = AppointmentRecommendation.objects.filter(appointment=apt)
                for recommendation in recommendations:
                    if recommendation.status == "Accept":
                        days_duration += recommendation.labor_hours*60.0

                # Add MPI Labor Hours
                ro_inspection_items = RoInspection.objects.filter(ro__ro_number=apt.ro)
                for ro_inspection in ro_inspection_items:
                    days_duration += ro_inspection.laborhours*60.0

            return days_duration


        def calc_days_capcity(calc_day):

            appointments = self.appointment_service.get_active_appointment_by_date(calc_day, dealer)
            if appointments:
                appointments = appointments.exclude(ro__flag3__name__in=flags3)
            work_mins = get_duration_for_apts(appointments)

            technician_hours_available = 0
            technician_hours_available_efficiency = float(0)
            user_service = UserService()
            technicians = user_service.get_users_for_dealers(dealer_id, "Technician")
            for technician in technicians:
                technician_hours_available += technician.userprofile.employee_available_hours
                technician_hours_available_efficiency += \
                    (float(technician.userprofile.employee_efficiency)/100.0) * \
                     float(technician.userprofile.employee_available_hours) * 60.0

            return work_mins, technician_hours_available_efficiency

        # Start Here

        flags3 = [ "*Service Complete", "*In Car Wash", "*Ready For Pickup", ]

        # print ("Calculating Capacity For: {}".format(target_date))

        # If the day is earlier than today then there's nothing to calculate.
        if target_date < dealer_time.date():
            return 0

        remaining_days = (target_date-dealer_time.date()).days + 1

        old_appointments = self.appointment_service.get_active_appointment_by_previous_date(dealer_time, dealer)
        if old_appointments:
            old_appointments = old_appointments.exclude(ro__flag3__name__in=flags3)

        # Get work previous to today to carry forward
        previous_days_remaining_mins = get_duration_for_apts(old_appointments)

        calc_day = dealer_time.date()
        for currentday in range(remaining_days):
            start_of_day = datetime.datetime.combine(calc_day, datetime.datetime.min.time())
            this_days_work_mins, this_days_tech_mins = calc_days_capcity(start_of_day)
            # print ("{}: Capacity Figures For: {}".format(target_date, calc_day))
            # print ("{}: Day's Work Hours: {}".format(target_date, this_days_work_mins/60.0))
            # print ("{}: Day's Tech Hours: {}".format(target_date, this_days_tech_mins/60.0))
            # print ("{}: Previous Day's Work Hours: {}".format(target_date, previous_days_remaining_mins / 60.0))
            # print ("{}: Days Remaining To Calc: {}".format(target_date, remaining_days-currentday))

            # If doing today. Adjust available tech hours by how far through the day we are. Simple hack and
            # needs to be thought through more carefully
            if currentday==0:
                this_days_tech_mins = remaining_day_fraction * this_days_tech_mins
            # Add this days work to rolling total
            total_work_mins = previous_days_remaining_mins + this_days_work_mins

            if total_work_mins > this_days_tech_mins:
                # we're over capacity now so adjust carry forward
                previous_days_remaining_mins = total_work_mins - this_days_tech_mins
            else:
                previous_days_remaining_mins = 0

            # Calculate day's percentage
            if this_days_tech_mins:
                todays_percentage = (total_work_mins/this_days_tech_mins)*100.0
            else:
                todays_percentage = 100.0

            calc_day = calc_day+datetime.timedelta(days=1)

        return todays_percentage


    def check_slab_availibity(self,slab_time,dealer,appointment=None):
        """slab_time should be timezone aware time"""

        self.setservices()
        capacity_per = 100
        shop_timings = self.dealership_service.get_dealer_shop_time(slab_time,dealer.id)
        if shop_timings:
            capacity_per = shop_timings["capacity"]

        if appointment and appointment.advisor :
            if self.check_slab_for_advisor( dealer,slab_time,appointment.advisor)==False:
                return False



        total_capacity_slab = self.get_capacity_for_slab(slab_time, dealer)
        total_capacity = self.get_tech_capacity_for_date(slab_time,dealer)#new method to get capacity for date

#       total_capacity =  round(total_capacity * (capacity_per/100.0))#getting percentage of capacity
        total_appointments_count = 0
        total_appointments_count_slab = 0
        total_appointments_slab = self.appointment_service.get_active_appointment_by_time(slab_time, dealer,None,appointment)
        total_appointments = self.appointment_service.get_active_appointment_by_date(slab_time, dealer,None,appointment)


        if total_appointments:
            total_appointments_count = len(total_appointments)
        if total_appointments_slab:
            total_appointments_count_slab = len(total_appointments_slab)
        if total_capacity> total_appointments_count:
            #print total_capacity_slab
            #print total_appointments_count_slab
            if total_capacity_slab > total_appointments_count_slab:
                return True

        return False


    def check_slab_for_advisor(self,dealer,slab_time,advisor,appointment=None):
        self.setservices()
        if self.check_user_available_for_slab(slab_time,advisor):
            if self.appointment_service.get_active_appointment_by_time(slab_time,dealer,advisor,appointment):
                return False
            return True
        else:
            return False


    def save_tech_count_for_date_range(self,slab_start_date,slab_end_date,dealer):
        """
                slab_start_date : date object
                slab_end_date: date object
                dealer : dealer object
        """
        self.setservices()
        self.dealer = dealer

        for n in range(int ((  slab_end_date - slab_start_date).days)):
            dt = slab_start_date + timedelta(n)
            self.save_tech_count_for_date(dt, dealer)


    def save_tech_count_for_date(self,slab_date,dealer):
        timings = self.dealership_service.get_dealer_shop_time(slab_date, dealer.id)
        if timings:
            slab = timings["open_time"]

            while slab < timings["close_time"]:
                # tmp_slab_up = slab + datetime.timedelta(minutes = timings["slot"])
                # if timings["on"]:
                self.save_tech_count_for_slab(slab, dealer)
                slab = slab + datetime.timedelta(minutes = timings["slot"])



    def save_tech_count_for_slab(self,slab_time,dealer):
#         print slab_time
        count = self.get_available_techs_count_for_slab(slab_time, dealer)
        try:
            capacitycount = CapacityCounts.objects.get(time_slab=slab_time,dealer=dealer)
        except Exception,e:
            capacitycount = CapacityCounts()
            capacitycount.time_slab=slab_time
            capacitycount.dealer = dealer
        capacitycount.total_tech = count
        capacitycount.save()


    def get_available_techs_for_slab(self,slab_time,dealer):
        self.setservices()
        try:
            techs =  self.cuserservice.get_users_for_dealers(dealer.id, group_type="Technician")
        except Exception,e:
            techs =  self.cuserservice.get_users_for_dealers(dealer, group_type="Technician")
        techs_list = []
        if techs:
            for tech in techs:
                if self.check_user_available_for_slab(slab_time,tech):
                    techs_list.append(tech)
        return techs_list

    def get_available_techs_for_date(self,slab_time,dealer):
        self.setservices()
        try:
            techs =  self.cuserservice.get_users_for_dealers(dealer.id, group_type="Technician")
        except Exception,e:
            techs =  self.cuserservice.get_users_for_dealers(dealer, group_type="Technician")
        techs_list = []


        if techs:
            for tech in techs:
                user_capacity = self.check_user_available_for_day(slab_time,tech)
                if user_capacity:
                    techs_list.append(tech)

        return techs_list



    def get_available_techs_count_for_slab_db(self,slab_time,dealer):
        try:
            capacitycount = CapacityCounts.objects.get(time_slab=slab_time,dealer=dealer)
            return capacitycount.total_tech
        except Exception,e:

            return self.get_available_techs_count_for_slab(slab_time,dealer)

    def get_available_techs_count_for_slab(self,slab_time,dealer):
        techs = self.get_available_techs_for_slab(slab_time, dealer)
        return len(techs)


    def get_available_advisors_for_slab(self,slab_time,dealer):
        self.setservices()
        try:
            advisors =  self.cuserservice.get_users_for_dealers(dealer.id)
        except Exception,e:
            advisors =  self.cuserservice.get_users_for_dealers(dealer)
        advisors_list = []
        if advisors:
            for advisor in advisors:
                if self.check_user_available_for_slab(slab_time,advisor):
                    advisors_list.append(advisor)
        return advisors_list

    def get_available_advisor_count_for_slab_db(self,slab_time,dealer):
        try:
            capacitycount = CapacityCounts.objects.get(time_slab=slab_time,dealer=dealer)
            return capacitycount.total_advisor
        except Exception,e:

            return self.get_available_advisor_count_for_slab(slab_time,dealer)

    def get_available_advisor_count_for_slab(self,slab_time,dealer):
        advisors = self.get_available_advisors_for_slab(slab_time, dealer)
        return len(advisors)

    def save_advisor_count_for_slab(self, slab_time, dealer):
        #         print slab_time
        count = self.get_available_advisor_count_for_slab(slab_time, dealer)
        try:
            capacitycount = CapacityCounts.objects.get(time_slab=slab_time, dealer=dealer)
        except Exception, e:
            capacitycount = CapacityCounts()
            capacitycount.time_slab = slab_time
            capacitycount.dealer = dealer
        capacitycount.total_advisor = count
        capacitycount.save()


    def save_advisor_count_for_date_range(self,slab_start_date,slab_end_date,dealer):
        """
                slab_start_date : date object
                slab_end_date: date object
                dealer : dealer object
        """
        self.setservices()
        self.dealer = dealer
        for n in range(int ((  slab_end_date - slab_start_date).days)):
            dt = slab_start_date + timedelta(n)
            self.save_advisor_count_for_date(dt, dealer)


    def save_advisor_count_for_date(self,slab_date,dealer):
        timings = self.dealership_service.get_dealer_shop_time(slab_date, dealer.id)
        if timings:
            slab = timings["open_time"]
            slab = slab.replace(tzinfo=pytz.UTC)
            while slab < timings["close_time"]:
    #           tmp_slab_up = slab + datetime.timedelta(minutes = timings["slot"])
    #           if timings["on"]:
                self.save_advisor_count_for_slab(slab, dealer)
                slab = slab + datetime.timedelta(minutes = timings["slot"])

    def get_available_advisors_for_date(self,slab_time,dealer):
        self.setservices()
        try:
            advisors =  self.cuserservice.get_users_for_dealers(dealer.id)
        except Exception,e:
            advisors =  self.cuserservice.get_users_for_dealers(dealer)
        advisors_list = []


        if advisors:
            for advisor in advisors:
                user_capacity = self.check_user_available_for_day(slab_time, advisor)
                if user_capacity:
                    advisors_list.append(advisor)

        return advisors_list

    def check_user_available_for_slab(self,slab_time,user):
        user_capacity = self.check_user_available_for_day(slab_time,user)
        if user_capacity:
            user_away = self.check_user_away_for_time(slab_time, user)
            return not user_away
        else:
            return False

    def check_user_away_for_time(self,slab_time,user):
        filter_aargs = {"start_date__lte": slab_time.strftime("%Y-%m-%d"), "advisor": user}
        args = (Q(end_date__gte=slab_time.strftime("%Y-%m-%d")) | Q(end_date=None), )
        day = slab_time.strftime("%A").lower()
        try:
            restrictions  = AdvisorRestrictions.objects.filter(*args,**filter_aargs)
            restrictions_values = restrictions.values('id',
                                           'monday',
                                           'tuesday',
                                           'wednesday',
                                           'thursday',
                                           'friday',
                                           'saturday',
                                           "start_time",
                                           "end_time",
                                           "start_date",
                                           )
            for restriction in restrictions_values:
                start_time = restriction["start_time"] \
                    if restriction["start_time"] is not None else datetime.time()
                end_time = restriction["end_time"] \
                    if restriction["end_time"] is not None else datetime.time(hour=23, minute=59, second=59)

                #print ("Restriction Slab Time: {} - {}: {} - {}".format(slab_time.date(),
                #                                                      slab_time.time(),
                #                                                      start_time,
                #                                                      end_time))
                if (restriction.get(day,False) == True and
                    slab_time.time() >= start_time and
                    slab_time.time() <= end_time):
                    return True
            return False
        except Exception,e:
            print e
            return False



    def check_user_available_for_day(self,slab_time,user):
        day = slab_time.strftime("%A").lower()
        user_capacity = False
        try:
            user_capacity = AdvisorCapacity.objects.get(advisor=user)
#             user_capacity_values = user_capacity.values('id',
#                                            'monday',
#                                            'tuesday',
#                                            'wednesday',
#                                            'thursday',
#                                            'friday',
#                                            'saturday',
#                                            )
        except Exception,e:
            print e
        if user_capacity:
            if hasattr(user_capacity,day)  and getattr(user_capacity,day) ==False:
                return False
        return True
