'''
Created on 24-Nov-2015

@author: Shoaib Anwar
'''
from dealership.models import *
import datetime
import time
import math

from dealership import conf
from django.contrib.auth.models import User
from dealership.models import UserProfile
#from dealership.services.userservices import UserService
from dealership.services.vehicleservices import VehicleService
from django.db.models import Q
from django.db.models import Count

from django.core import serializers
from django.utils import timezone
from django.db.models.functions import Lower


class CustomerService():

    def setservices(self):
        from dealership.factories import DealerShipServicesFactory
        self.dealer_service_factory = DealerShipServicesFactory()
        self.vehicle_service = self.dealer_service_factory.get_instance("vehicle")
        self.appointment_service = self.dealer_service_factory.get_instance("appointment")
        self.customer_service = self

    def get_customer_list(self, dealer, cust_id, page = 1, limit = 10):
        self.setservices()
        start = (page*limit - limit)
        end = page*limit
        if cust_id == 0:
            # Viewing all customers. Filter by store
            # customers = UserProfile.objects.filter().order_by('first_name')
            # TODO: Find customers that have appointments with this dealer. Move to a management command later when optimizing



            # Get all vehicles the current dealer services so we can exclude customers that do not have a vehicle
            # serviced by this dealer
            dealers_vehicles = DealersVehicle.objects.filter(dealer_id=dealer.id).values_list('vehicle_id', flat=True).all()

            # -----------------------------------------------------------------
            # Now find customers that have logged into or have made an appointment with this dealer
            # Remember that queries are not evaluated until they're used. so...

            # Get Appointments made with this dealer and that have a customer assigned to the appointment
            dealers_customers = Appointment.objects.filter(dealer_id=dealer.id, customer__isnull=False)

            # If a profile is a registered user and is in the user group "Customer" or has not registered as a user so is therefore a customer
            dealers_customers = dealers_customers.filter(Q(customer__user__groups__name__in=['Customer']) | Q(customer__user__isnull=True))

            # If the last time this customer logged in was to  this dealer or had an appointment at any time with an accepted vehicle serviced by this dealer
            dealers_customers = dealers_customers.filter(Q(customer__dealer_id=dealer.id) | Q(vehicle__vehicle_id__in=dealers_vehicles))

            # Extract the list of customers and remove duplicates
            dealers_customers = dealers_customers.values_list('customer_id').distinct()

            # Now pull the actual customers from the final list
            customers = UserProfile.objects.filter(id__in=dealers_customers).order_by(Lower('first_name'))
        else:
            # Viewing specific customers. Don't filter by store
            cust_id = cust_id.split(",")
            customers = UserProfile.objects.filter(id__in = cust_id).order_by('first_name')

        # Get total count and trim to current page
        count = customers.count()
        customers = customers[start:end]

        data = []
        for cust in customers:
            appt = "None"
            vehicles = self.vehicle_service.get_customer_vehicles(cust.id, dealer)
            if vehicles is None:
                vehicles = []
                # No vehicles then don't show them.

            #if cust_id == 0:
                # If w're searching for all customers then only choose customers that have appointments with this dealer
            #    dealer_appts = self.vehicle_service.get_customer_vehicle_appts_for_dealer(cust.id, dealer)
            #    if not dealer_appts:
            #        continue

            appointments = self.appointment_service.get_customer_scheduled_appointments_for_dealer(cust.id, dealer.id)
            if appointments:
                appt = appointments[0].start_time.strftime('%A %b %d, %I:%M %p')

            advisor_name = "NO ADVISOR"
            try:
                advisor_name = "%s" % (cust.myadvisors.advisor.get_name_firstlast().upper())
            except Exception, ex:
                pass

            profile = "Partial"
            try:
                user_id = cust.user.id
                profile = "Complete"
            except Exception, ex:
                pass

            # If showing all customers just show those ones that have a vehicle serviced by this dealer
            data.append({'id':cust.id,
                         'name':"%s" % (cust.get_name_firstlast().upper()),
                         'vehicle':{'count':len(vehicles), 'list':vehicles},
                         'advisor':advisor_name,
                         'appt':appt,
                         'profile':profile})
        # count = len(data)
        return {"total": count, "page": page, "limit": limit, "data": data}

    @staticmethod
    def get_customers_by_phone(phone):
        customers = UserProfile.objects.filter(
            Q(phone_number_1__contains=phone) |
            Q(phone_number_2__contains=phone) |
            Q(phone_number_3__contains=phone))
        return customers

    def search_customer_list(self, dealer, cust_id, page = 1, limit = 10):
        pass
