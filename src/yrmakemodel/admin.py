from django.contrib import admin
from yrmakemodel.models import *

# Register your models here.

class YearMakeModelAdmin(admin.ModelAdmin):
    pass

admin.site.register(YearMakeModel,YearMakeModelAdmin)
