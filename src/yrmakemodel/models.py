from django.db import models

# Create your models here.

class YearMakeModel(models.Model):
    model_make_name = models.CharField(max_length=100)
    model_name = models.CharField(max_length=100)
    model_trim = models.CharField(max_length=100)
    model_year = models.IntegerField(default=0)
    
    def __str__(self):
        return(self.model_make_name 
            + self.model_name + self.model_trim) 
