# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='YearMakeModel',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('model_make_name', models.CharField(max_length=100)),
                ('model_name', models.CharField(max_length=100)),
                ('model_trim', models.CharField(max_length=100)),
                ('model_year', models.IntegerField(default=0)),
            ],
        ),
    ]
