from .base import *
from os.path import join

# ToDo
# # Sensitive information passwords, SECRET_KEY  etc. Must be moved from this file to a safe location
# on the system as soon as possible
#
# Set allowed_hosts to use test server host

print("Running Test Settings")
SECRET_KEY="_j+^_w)dtu=k67ky!=ya8om=z5j21-)12j&+xex-%=(a4xk019"

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True
TEMPLATES[0]["OPTIONS"]['debug'] = True


MEDIA_URL = '/app/media/'
STATIC_URL = '/app/static/'



# To send email to the console or file uncomment one of the following
# Console
# EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'

# File
# EMAIL_BACKEND = 'django.core.mail.backends.filebased.EmailBackend'

# Change to test host when setup properly
ALLOWED_HOSTS = ['*']


# Internationalization
MEDIA_ROOT = "/opt/greenlightautomotive/app/media/"
STATIC_ROOT = "/opt/greenlightautomotive/app/static/"

EMAIL_HOST = 'localhost'
EMAIL_PORT = 25

##### Move to file out of git soon!
DATABASES = {
    'default': {
        'ENGINE': "django.db.backends.mysql",
        'NAME': "pr01mydb01",
        'HOST': "mysqldst01.cunpgfdq8hpq.us-west-2.rds.amazonaws.com",
        'PORT': "3306",
        'USER': "rootdbpr01",
        'PASSWORD': "Y**waK3MGr^2",
    }
}

DEFAULT_EMAIL_FROM = "admin@greenlightautomotive.com"

SITE_MAIN_URL = "http://ec2-34-216-76-54.us-west-2.compute.amazonaws.com"
USE_TWILIO=False

XFRAME_URLS_REGEX = r'^/app/customer/.*$'
