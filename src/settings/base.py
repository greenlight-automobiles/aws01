"""
Testing Django settings for BMW project.

For more information on this file, see
https://docs.djangoproject.com/en/1.7/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.7/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
import sys
from django.core.urlresolvers import reverse_lazy

BASE_DIR = os.path.dirname(os.path.dirname(__file__))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.7/howto/deployment/checklist/


# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = False

ALLOWED_HOSTS = []

# Application definition

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.sites',
    'colorfield',
    'dealership',
    'customer',
    'flagging',
    'livechat',
    'mobilecheckin',
    'yrmakemodel',
    # 'subdomains'
)
TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [
            # insert your TEMPLATE_DIRS here
        ],
        'APP_DIRS': True,
        'OPTIONS': {
            'debug': False,
            'context_processors': [
                # Insert your TEMPLATE_CONTEXT_PROCESSORS here or use this
                # list if you haven't customized them:
                'django.contrib.auth.context_processors.auth',
                'django.template.context_processors.debug',
                'django.template.context_processors.i18n',
                'django.template.context_processors.media',
                'django.template.context_processors.static',
                'django.template.context_processors.tz',
                'django.contrib.messages.context_processors.messages',
                'django.core.context_processors.request'
            ],
        },
    },
]
MIDDLEWARE_CLASSES = (
    'core.django.middleware.XFrameAllowMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    #  'subdomains.middleware.SubdomainURLRoutingMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'customer.middelware.TimeZoneMiddleware',
)

ROOT_URLCONF = 'urls'

WSGI_APPLICATION = 'wsgi.application'

# Database
# https://docs.djangoproject.com/en/1.7/ref/settings/#databas

# Internationalization
# https://docs.djangoproject.com/en/1.7/topics/i18n/

LANGUAGE_CODE = 'en-us'

USE_TZ = True
TIME_ZONE = 'UTC'

USE_I18N = True
USE_L10N = True


STATICFILES_DIRS = (
    os.path.join(BASE_DIR, "static"),
)

# CENTRIFUGE_URL ="https://bmw-elosophy.herokuapp.com/connection";
# CENTRIFUGE_SECRET = "demo";
CENTRIFUGE_URL = "https://bmw-elosophy.herokuapp.com/connection";  # used for javascript librarires
CENTRIFUGE_API_URL = "https://bmw-elosophy.herokuapp.com/";
CENTRIFUGE_SECRET = "demo";
CENTRIFUGE_KEY = "bmw-elosophy"
MAP_KEY = "AIzaSyArTVoMmtYJ87Eb6FIveZ-IqX1WJ2HPf8Y"
APPROVAL_REQUIRED_FLAG_ID = 43
CAR_WASH_FLAG_ID = 46
UBER_ID = 46
# SITE_MAIN_URL ="http://202.165.228.25:8080"
BMW_MAKE_CODE = "u"
SITE_ID = 1

"""
ROOT_URLCONF = 'customer.urls'
# 
SUBDOMAIN_URLCONFS = {
    None: 'customer.urls',
    'customer': 'customer.urls',
    'dealership': 'dealership.urls',
    'mobilecheckin': 'mobilecheckin.urls',
    'flagging': 'flagging.urls',
}

"""

# Move these to production only
PAYPAL_CLIENT_ID = "ARfL_wcV0YdvLyobp_ic4vA-qWqKKLCku7s7Ck9VK7oGSCm0hO6vPW3jk9OIAuevLG0au6a8m8uqVNqi"
PAYPAL_SECRET = "EJXORrojmCIvcv0yqru4qgZmJ_cI4Gz8khki0eJyQCrZN5XdUmMM_N-owz1bwNL0OkWH88MXofD_uf8M"
PAYPAL_MODE = "sandbox"

# TWILIO_SID = "ACc5c01b5f1d1ad8d21c3779a586b52ab6"
# TWILIO_AUT = "925b1ad0554cae0fbfaf7098ea4f7d13"
# TWILIO_DEFAULT = "+1 602-544-6125"

TWILIO_SID="ACc5c01b5f1d1ad8d21c3779a586b52ab6"
TWILIO_AUT="925b1ad0554cae0fbfaf7098ea4f7d13"
TWILIO_DEFAULT="+1415 980-5260"

# Refresh RO List
RO_LIST_REFRESH = True

# Live notifications defaults
LIVE_NOTIFICATIONS = True
LIVE_NOTIFICATIONS_REFRESH_RATE = 30000

XFRAME_URLS_REGEX = r'^/customer/.*$'
# XFRAME_URLS_REGEX = r'[\s\S]*' # ALL
# XFRAME_URLS_REGEX = r'$^' # NONE

