from .base import *
from os.path import join


# Load the environment specific local settings
from .loadlocalsettings import load_localsettings
env = load_localsettings()

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = env('SECRET_KEY')

print("Running Development Settings")

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True
TEMPLATES[0]["OPTIONS"]['debug'] = True

# Turn off debug while imported by Celery with a workaround
# See http://stackoverflow.com/a/4806384
import sys
if "celery" in sys.argv[0]:
    DEBUG = False


MEDIA_URL = '/media/'
STATIC_URL = '/static/'


if env('DEV_EMAIL_DEBUG')=="console":
    # Show emails in console in DEBUG mode
    EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'
    print "Debug Settings: Using console output for email"

if env('DEV_EMAIL_DEBUG')=="file":
    # Send emails to a file in DEBUG mode
    EMAIL_BACKEND = 'django.core.mail.backends.filebased.EmailBackend'
    EMAIL_FILE_PATH = env('DEV_EMAIL_FILE_PATH')
    print "Debug Settings: Using file output for email"

# Show thumbnail generation errors
THUMBNAIL_DEBUG = True

ALLOWED_HOSTS = ['*',]

#===============================================================================
# Debug Toolbar
#===============================================================================
# INTERNAL_IPS = ('127.0.0.1',)

MIDDLEWARE_CLASSES += (
    'debug_toolbar.middleware.DebugToolbarMiddleware',
)

# Django Debug Toolbar
#INSTALLED_APPS += (
#    'debug_toolbar.apps.DebugToolbarConfig',)

#DEBUG_TOOLBAR_CONFIG = {
#    'INTERCEPT_REDIRECTS': False,
    #===========================================================================
    # 'SHOW_TOOLBAR_CALLBACK': custom_show_toolbar,
    # 'EXTRA_SIGNALS': ['myproject.signals.MySignal'],
    # 'HIDE_DJANGO_SQL': False,
    # 'TAG': 'div',
    # 'ENABLE_STACKTRACES' : True,
    #===========================================================================
#}

#DEBUG_TOOLBAR_PANELS = (
#    'debug_toolbar.panels.version.VersionDebugPanel',
#    'debug_toolbar.panels.timer.TimerDebugPanel',
#    'debug_toolbar.panels.settings_vars.SettingsVarsDebugPanel',
#    'debug_toolbar.panels.headers.HeaderDebugPanel',
#    'debug_toolbar.panels.request_vars.RequestVarsDebugPanel',
#    'debug_toolbar.panels.template.TemplateDebugPanel',
#    'debug_toolbar.panels.sql.SQLDebugPanel',
#    'debug_toolbar.panels.signals.SignalDebugPanel',
#    'debug_toolbar.panels.logger.LoggingPanel',
#)

# Internationalization
MEDIA_ROOT = join(BASE_DIR, 'media')

#  Set Nessage Level
from django.contrib.messages import constants as message_constants
# MESSAGE_LEVEL = message_constants.DEBUG
# MESSAGE_LEVEL = message_constants.WARNING

#INSTALLED_APPS += (
#    "sslserver",
#    'debug_toolbar',
#)


DATABASES = {
    'default': {
        'ENGINE': env('DATABASE_TYPE'),
        'NAME': env('DATABASE_NAME'),
        'HOST': env('DATABASE_HOST'),
        'PORT': env('DATABASE_PORT'),
        'USER': env('DATABASE_USER'),
        'PASSWORD': env('DATABASE_PASS'),
    }
}

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = env('SECRET_KEY')

EMAIL_USE_TLS = True if env('EMAIL_USE_TLS') == "True" else False
EMAIL_HOST = env('EMAIL_HOST')
EMAIL_HOST_USER = env('EMAIL_HOST_USER')
EMAIL_HOST_PASSWORD = env('EMAIL_HOST_PASSWORD')

EMAIL_PORT=int(env('EMAIL_PORT')) if env('EMAIL_PORT') else ""
DEFAULT_EMAIL_FROM = env('DEFAULT_EMAIL_FROM')
SITE_MAIN_URL = env("SITE_URL")
USE_TWILIO = True if env("USE_TWILIO") == "True" else False

RO_LIST_REFRESH = True if env('RO_LIST_REFRESH') == "True" else False
LIVE_NOTIFICATIONS = True if env('LIVE_NOTIFICATIONS') == "True" else False
LIVE_NOTIFICATIONS_REFRESH_RATE = int(env('LIVE_NOTIFICATIONS_REFRESH_RATE')) \
    if env('LIVE_NOTIFICATIONS_REFRESH_RATE') else LIVE_NOTIFICATIONS_REFRESH_RATE

join(BASE_DIR, 'media')
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'verbose': {
            'format': '%(asctime)s %(asctime)s %(levelname)s %(module)s %(process)d %(thread)d %(message)s'
        },
        'simple': {
            'format': '%(asctime)s %(name)s %(levelname)s %(message)s'
        },
    },
    'filters': {
        # 'special': {
        #    '()': 'project.logging.SpecialFilter',
        #    'foo': 'bar',
        # 3 },
        'require_debug_true': {
            '()': 'django.utils.log.RequireDebugTrue',
        },
    },
    'handlers': {
        'console': {
            'level': 'INFO',
            'filters': ['require_debug_true'],
            'class': 'logging.StreamHandler',
            'formatter': 'simple'
        },
        'mail_admins': {
            'level': 'ERROR',
            'class': 'django.utils.log.AdminEmailHandler',
            # 'filters': ['special']
        }
    },
    'loggers': {
        'django': {
            'handlers': ['console'],
            'propagate': True,
        },
        'django.request': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': False,
        },
        #'greenlight.custom': {
        #    'handlers': ['console', 'mail_admins'],
        #    'level': 'INFO',
        #    'filters': ['special']
        #}
    }
}
