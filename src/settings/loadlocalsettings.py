# Import local settings from environment file
import os
import environ
from os.path import dirname, join, exists


def load_localsettings():
    env = environ.Env()
    localenv_file = os.environ.get('LOCALENV')
    if localenv_file is None:
        localenv_file = "local.env"

    print("localenv_file: {}".format(localenv_file))
    env_file = join(dirname(__file__), localenv_file)
    if exists(env_file):
        environ.Env.read_env(str(env_file))
    return env

