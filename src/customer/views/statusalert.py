import json

from django.core.mail import send_mail
from django.http import HttpResponse, HttpResponseBadRequest
from django.http.response import HttpResponseRedirect
from django.shortcuts import render, redirect
from django.template.loader import render_to_string
from django.utils import timezone
from django.views.decorators.csrf import csrf_exempt
from django.http import Http404
from customer.factories import CustomerServicesFactory
from customer.services.paypal import paypal_payment
from dealership.models import *
from django.contrib.sites.shortcuts import get_current_site
from dealership.factories import DealerShipServicesFactory

def index(request,appointment_id):
    customer_factory = CustomerServicesFactory()

    service = customer_factory.get_instance("user")

    templates = "customer/statusalert/index.html"
    try:
        appt_details = Appointment.objects.get(id = appointment_id)
    except Appointment.DoesNotExist:
        raise Http404
    flags = Flags.objects.filter(type=3, customer_facing=True, dealer_id = appt_details.dealer.id)
    try:
        ordered_flags = list((flags.get(name__icontains="In Queue"),
                             flags.get(name__icontains="Working on Vehicle"),
                             flags.get(name__icontains="Approval Needed"),
                             flags.get(name__icontains="In Car Wash"),
                             flags.get(name__icontains="Service Complete"),
                             flags.get(name__icontains="Ready For Pickup")),
                             )
    except Exception as e:
        ordered_flags = None
        print str(e)

    recommendations = AppointmentRecommendation.objects.filter(appointment_id = appointment_id)

    total = 0
    for obj in recommendations:
        total += obj.price

    appt_services = AppointmentService.objects.filter(appointment_id=appointment_id)
    if appt_details:
        userprofile = appt_details.customer
#     userprofile =service.get_userprofile(request.user)
    service_total = 0
    for obj in appt_services:
        service_total += obj.price
    approved_recomm = AppointmentRecommendation.objects.filter(appointment_id=appointment_id , status = "Accept")

    for obj in approved_recomm:
        service_total += obj.price
    dealer = appt_details.dealer
    dealer_code=None
    if dealer:
        dealer_code = dealer.dealer_code

    # If there are pending itmes approval still needs to be given by the customer.
    # pending_recomm = AppointmentRecommendation.objects.filter(appointment_id=appointment_id, status="Pending")
    # if pending_recomm.count() > 0:
    #     appt_details.appointment_recommendation_status = False



    context  = {'flags':ordered_flags , 'appt' : appt_details , 'recommendations' : recommendations , 'total':total,
                                     'appt_services' : appt_services , 'approved_rec' : approved_recomm , 's_total':service_total,'dealer_code':dealer_code}
    service.set_centrifuge_context(request,dealer_code,userprofile, context,chatposition="top")
    return render(request,templates,context)

def index_hash(request,appointment_hash):
    try:
        appt_details = Appointment.objects.get(status_url_hash = appointment_hash)
        return index(request, appt_details.id)
    except Appointment.DoesNotExist:
        raise Http404


@csrf_exempt
def approve_recommendations(request,appointment_id):
    if request.method == "POST":
        email_data =[]
        data = json.loads(request.POST['data'])


        for obj in data:
            app_rec = AppointmentRecommendation.objects.get(id = obj['id'])
            app_rec.status = obj['val']
            app_rec.save()
            if app_rec.title:
                title = app_rec.title
            else:
                title = app_rec.notes

            status_dic={"name": title ,"part_price":app_rec.parts,"labour_price":app_rec.labor,"price":app_rec.price, "status":app_rec.status}
            email_data.append(status_dic)

        appnt = Appointment.objects.get(id=appointment_id)

        # TODO: Check signature is valid
        full_name = request.POST['full_name']
        if full_name == "":
            return HttpResponseBadRequest({'message': 'Please type your full name'})

        appnt.customer_actionplan_typed_full_name = full_name
        signature_drawn_is_blank = False if request.POST['signature_drawn_is_blank'] == 'false' else True

        # Don't save this signature if blank
        if not signature_drawn_is_blank:
            signature_drawn = request.POST['signature_drawn']
            appnt.customer_actionplan_signature = signature_drawn

        appnt.appointment_recommendation_status = True
        appnt.save()

        customer_name = appnt.customer.get_name_firstlast()
        customer_vehicle = "Undefined"
        cv = CustomerVehicle.objects.get(id=appnt.vehicle.id)
        if cv:
            customer_vehicle = "{}, {}".format(cv.vehicle.make,
                                               cv.vehicle.model)
        message = "Customer '{}' with Vehicle '{}' replied to inspection results".format(customer_name, customer_vehicle)

        dealer_factory = DealerShipServicesFactory()
        notificationservice = dealer_factory.get_instance("notification")

        recipients = []
        if appnt.advisor:
            recipients.append(appnt.advisor)

        if appnt.ro.inspector:
            recipients.append(appnt.ro.inspector)

        notificationservice.send_live_notification(sender=request.user, dealer=appnt.dealer, recipients=recipients,
                                                   message=message)

        if request.POST['email_check'] == "true":
            context = {'status' : email_data }
            context.update(get_email_dealer_info(appnt.dealer))

            params = json.dumps(context)
            try:
                notificationservice.send_dealer_based_notification(appnt.dealer,appnt.customer,params,"recommendation_status_details",send_email=True,send_text=False)
            except Exception,e:
                print e
            #msg_plain = render_to_string('customer/emails/approve_details.txt',context)
            #msg_html = render_to_string('customer/emails/approve_details.html', context)
            #send_mail('Recommendation Details', msg_plain , settings.EMAIL_HOST_USER , [appnt.customer.email_1 , appnt.customer.email_2] ,html_message=msg_html, fail_silently=False)


        return HttpResponse(json.dumps({'message' : 'Recommendations Updated'}))


@csrf_exempt
def reply(request,appointment_id):
    if request.method == "POST":
        try:
            appt = Appointment.objects.get(id = appointment_id)
        except Appointment.DoesNotExist:
            raise Http404
        if not appt.dealer:
            raise Http404
        service_total = 0
        appt_services = AppointmentService.objects.filter(appointment_id=appointment_id)
        list_service = []
        list_recommendation = []
        for obj in appt_services:
            service_total += obj.price
            list_service.append(obj)
        approved_recomm = AppointmentRecommendation.objects.filter(appointment_id=appointment_id , status = "Accept")
        for obj in approved_recomm:
            service_total += obj.price
            list_recommendation.append(obj)
        payment = paypal_payment(appt.dealer)
        message = None
        url = None
        domain = get_current_site(request).domain
        if appt.creditcard_id:
            result = payment.paywith_creditcard_token(appt.creditcard_id,list_service,list_recommendation,service_total)
            if 'id' in result:
                appt.payment_id = result['id']
                appt.payment_status = True
                appt.save()
                flag = Flags.objects.get(id=appt.dealer.prestagevehicle_flag_id)
                date = timezone.now()
                appt.ro.flag3 = flag
                appt.ro.flag3_updated_time = date
                appt.ro.save()
                message = "Thankyou For your Payment ,your payment id is " + result['id']+". Your Auto will be ready for pick-up soon."
            else:
                url = payment.paywith_paypal(list_service,list_recommendation,service_total,appt,domain)
                message = "Your payment encounter some error" + result['error']+ ". <a href="+url+">Click Here</a> to process your payment manually."
        else:
            url = payment.paywith_paypal(list_service,list_recommendation,service_total,appt,domain)

    return HttpResponse(json.dumps({'message' : message , 'data' : url }))



def payment(request,appointment_id):
    templates = "customer/statusalert/payment.html"
    payment_id = request.GET['paymentId']
    payer_id = request.GET['PayerID']
    try:
        appt = Appointment.objects.get(id= appointment_id)
    except Appointment.DoesNotExist:
        raise Http404
    if not appt.dealer:
        raise Http404
    payment = paypal_payment(appt.dealer)
    if payment_id and payer_id :
        pay_id  = payment.execute(payment_id, payer_id)
        if 'id' in pay_id:
            appt.payment_id = pay_id['id']
            appt.payment_status = True
            appt.save()
            flag = Flags.objects.get(id=appt.dealer.prestagevehicle_flag_id)
            date = timezone.now()
            appt.ro.flag3 = flag
            appt.ro.flag3_updated_time = date
            appt.ro.save()
            message = "Thankyou For your Payment ,your payment id is " + pay_id['id']+". Your Auto will be ready for pick-up soon."
        else:
            message = "Your payment encounter some error" + pay_id['error']
    else:
        message= "Invalid Request"
    return render(request,templates,{'message' : message})
