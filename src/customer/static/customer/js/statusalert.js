$(document).ready(function() {

    /* Only include approval triggers if required */
    if ($('.submitapproval').length) {
        $('#signature-drawn').sketch();


        $(document).on('click','#erase-signature' , function(event){
            element = document.getElementById ("signature-drawn");
            context=element.getContext("2d");
            context.clearRect (0, 0, element.width, element.height);
            $('#signature-drawn').sketch().actions = [];
        });

        $(document).on('click','.submitapproval',function() {

            var canvas = document.getElementById ("signature-drawn");
            var blank = document.createElement('canvas');
            var canvas_data_url = canvas.toDataURL();

            // Reset errors
            $('#full-name').parent().removeClass("has-error");
            full_name = $('#full-name').val()
            if ( full_name=="" ) {
                $('#full-name').parent().addClass("has-error");
                alert("Please type your full name");
            }
            else{
                var signature_drawn_is_blank = true;
                if ( !isCanvasBlank(canvas) ) {
                    signature_drawn_is_blank = false;
                }

                $('#loading_page').show();
                var data = [];
                $(':radio:checked').each(function(){
                    var rec = {};
                    rec['id'] = $(this).attr("name");
                    rec['val'] = $(this).val();
                    data.push(rec);
                })
                data = JSON.stringify(data);
                $.post(approve_status ,{'data': data , 'full_name': full_name, 'signature_drawn_is_blank' : signature_drawn_is_blank, 'signature_drawn' : canvas_data_url, email_check: $("#status_email").prop('checked')}, function(data){
                    $('#loading_page').hide();
                    $('.status_successbox').html("Recommendation Status Set Successfully");
                    $('#status_notfy').html('<Strong class="text-danger">Your recommended service response has been received. Your Auto will be ready for pick-up soon.</strong>')
                    $('.status_successbox').show();
                }).fail(function(){
                    $('#loading_page').hide();
                    $('.status_errorbox').show();
                    $('.status_errorbox').fadeOut(5000);
                });
            }
        });
    }
});


/*
$(document).on('click','.acceptappointment',function(){
	var canvas = document.getElementById ("customer_checkin_signature");
	var blank = document.createElement('canvas');
    blank.width = canvas.width;
    blank.height = canvas.height;
	if ($('#signature').val() == ""){
		alert("Customer Name is Required");
	}
	else if(canvas.toDataURL()== blank.toDataURL()){
		alert("Customer Signature is Required");
	}
	else{
		$('#loading_page').show();
		$.post(accept_appointment,{'tagno': $('#tagno').val(), 'sign' : canvas.toDataURL() , 'send_email' : $('#preorderemail').is(':checked')},function(data){
			$('#loading_page').hide();
			window.location.href = main_page;
		}).fail(function(){
			$('#loading_page').hide();
	    	$('.errorbox').show();
	    	$('.errorbox').fadeOut(5000);
	    	window.location.href = main_page;
		});
	}
});
*/

$(document).on('click','.creditinfo',function(){
	$('#loading_page').show();
	$.post(reply_status ,{'data':""}, function(data){
		$('#loading_page').hide();
		var obj = JSON.parse(data)
		if (obj.message){
			$('#reply_div').html(obj.message);
			$('.payment_details').modal('toggle');
		}
		else{
			window.location.href = obj.data.redirect;
		}

	}).fail(function(){
		$('#loading_page').hide();
    	$('.status_errorbox').show();
    	$('.status_errorbox').fadeOut(5000);
	});
	return false;
});

$(document).on('click', '.cancelpayment', function(){
	$('.payment_details').modal('toggle');
})

function isCanvasBlank(canvas) {
    var blank = document.createElement('canvas');
    blank.width = canvas.width;
    blank.height = canvas.height;

    return canvas.toDataURL() == blank.toDataURL();
}

function getCanvasBlank() {
    var blank = document.createElement('canvas');
    blank.width = canvas.width;
    blank.height = canvas.height;

    return blank;
}

