(function() {
    var shopcontacts = {

        loadData:function(){
        	return $.ajax({
                type: "get",
                url: get_shopcontacts,
                dataType: "json",
                success: function(data){
                    return data;
                },
                error: function(){
                    alert("Error cannot load table");
                }
            });
        },

        insertItem: function(insertingClient) {
        	$('#loading_page').show();
        	console.log(insertingClient);
        	$.post(add_shopcontacts,insertingClient,function(data){
        		$("#maincontactgrid").jsGrid("loadData")
        		$('#loading_page').hide();
        		BootstrapDialog.alert("Contact Saved Successfully");
           }).fail(function(){
        	   $('#loading_page').hide();
        	   BootstrapDialog.alert("Error Occured While Saving");
           });
        },

        updateItem: function(updatingClient) {
        	$('#loading_page').show();
        	console.log(updatingClient);
        	$.post(update_shopcontacts,updatingClient,function(data){
        		$('#loading_page').hide();
        		BootstrapDialog.alert("Contact Updated");
            }).fail(function(){
        	   $('#loading_page').hide();
        	   BootstrapDialog.alert("Error Occured While Saving");
           });
        },

        deleteItem: function(deletingClient) {
        	$('#loading_page').show();
        	$.post(delete_shopcontacts,deletingClient,function(data){
        		$('#loading_page').hide();
        		BootstrapDialog.alert("Contact Deleted");
            }).fail(function(){
        	   $('#loading_page').hide();
        	   BootstrapDialog.alert("Error Occured While Deleteing");
           });
        }

    };

    window.shopcontacts = shopcontacts;

}());
