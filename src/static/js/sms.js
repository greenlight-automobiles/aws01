(function() {
    var sms = {

        loadData:function(){
        	return $.ajax({
                type: "get",
                url: get_sms,
                dataType: "json",
                success: function(data){
                    return data;
                },
                error: function(){
                    alert("Error cannot load table");
                }
            });
        },

        insertItem: function(insertingClient) {
        	$('#loading_page').show();
        	console.log(insertingClient);
        	$.post(add_sms,insertingClient,function(data){
        		 $("#smsgrid").jsGrid("loadData");
        		 $('#loading_page').hide();
     		 	BootstrapDialog.alert("SMS Number Saved");
          }).fail(function(){
      	   $('#loading_page').hide();
      	   BootstrapDialog.alert("Error Occured While Saving");
         });
        },

        updateItem: function(updatingClient) {
        	console.log(updatingClient);
        	$('#loading_page').show();
        	$.post(update_sms,updatingClient,function(data){
        		$('#loading_page').hide();
    		 	BootstrapDialog.alert("SMS Number Saved");
         }).fail(function(){
     	   $('#loading_page').hide();
     	   BootstrapDialog.alert("Error Occured While Saving");
        });
        },

        deleteItem: function(deletingClient) {
        	$('#loading_page').show();
        	$.post(delete_sms,deletingClient,function(data){
        		$('#loading_page').hide();
    		 	BootstrapDialog.alert("SMS Number Deleted");
         }).fail(function(){
     	   $('#loading_page').hide();
     	   BootstrapDialog.alert("Error Occured While Delete");
        });
        }

    };

    window.sms = sms;

}());
