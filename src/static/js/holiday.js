(function() {
    var holiday = {

        loadData:function(){
        	return $.ajax({
                type: "get",
                url: get_holiday,
                dataType: "json",
                success: function(data){
                    return data;
                },
                error: function(){
                    alert("Error cannot load table");
                }
            });
        },

        insertItem: function(insertingClient) {
        	console.log(insertingClient);
        	insertingClient.Date = getcorrectdate(insertingClient.Date);;
        	$.post(add_holiday,insertingClient,function(data){
        	    $("#holidays").jsGrid("loadData");
        	    $('#loading_page').hide();
 		 		BootstrapDialog.alert("Holiday Saved");
	          }).fail(function(){
	      	   $('#loading_page').hide();
	      	   BootstrapDialog.alert("Error Occured While Saving");
	         });
        },

        updateItem: function(updatingClient) {
        	console.log(updatingClient.Date);
        	updatingClient.Date = getcorrectdate(updatingClient.Date);
        	$.post(update_holiday,updatingClient,function(data){
        		$('#loading_page').hide();
 		 		BootstrapDialog.alert("Holiday Saved");
	          }).fail(function(){
	      	   $('#loading_page').hide();
	      	   BootstrapDialog.alert("Error Occured While Saving");
	         });
        },

        deleteItem: function(deletingClient) {
        	$.post(delete_holiday,deletingClient,function(data){
        		$('#loading_page').hide();
 		 		BootstrapDialog.alert("Holiday Saved");
	          }).fail(function(){
	      	   $('#loading_page').hide();
	      	   BootstrapDialog.alert("Error Occured While Saving");
	         });
        }

    };

    window.holiday = holiday;

}());

function getcorrectdate(date){
	var newdate = new Date(date);
	newdate.setDate(newdate.getDate()); 
	var d = newdate.getDate();
	var m = newdate.getMonth()+1;
	var y = newdate.getFullYear();
	if (d   < 10) {d   = "0"+d;}
    if (m < 10) {m = "0"+m;}
	formatdate = y +'-'+ m +'-'+ d;
	console.log(formatdate);
	return formatdate
}