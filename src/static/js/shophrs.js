(function() {
    var shophrs = {

        loadData:function(){
        	return $.ajax({
                type: "get",
                url: get_shphrs,
                dataType: "json",
                success: function(data){
                    return data;
                },
                error: function(){
                    alert("Error cannot load table");
                }
            });
        },

        insertItem: function(insertingClient) {
        	console.log(insertingClient);
        	$('#loading_page').show();
        	$.post(add_shphrs,insertingClient,function(data){
        		 $("#shophrsgrid").jsGrid("loadData");
        		 var obj = JSON.parse(data);
        		 $('#loading_page').hide();
     		 		BootstrapDialog.alert(obj.message);
		          }).fail(function(){
		      	   $('#loading_page').hide();
		      	   BootstrapDialog.alert("Error Occured While Saving");
		         });
        },

        updateItem: function(updatingClient) {
        	console.log(updatingClient);
        	$('#loading_page').show();
        	$.post(update_shphrs,updatingClient,function(data){
        		$('#loading_page').hide();
 		 		BootstrapDialog.alert("Shop Hours Saved");
	          }).fail(function(){
	      	   $('#loading_page').hide();
	      	   BootstrapDialog.alert("Error Occured While Saving");
	         });
        },

        deleteItem: function(deletingClient) {
        	$('#loading_page').show();
        	$.post(delete_shphrs,deletingClient,function(data){
        		$('#loading_page').hide();
 		 		BootstrapDialog.alert("Shop Hours Deleted");
	          }).fail(function(){
	      	   $('#loading_page').hide();
	      	   BootstrapDialog.alert("Error Occured While Delete");
	         });
        }

    };

    window.shophrs = shophrs;
    
    shophrs.days = [
                    { Name: "Monday", Id: "Monday" },
                    { Name: "Tuesday", Id: "Tuesday" },
                    { Name: "Wednesday", Id: "Wednesday" },
                    { Name: "Thursday", Id: "Thursday" },
                    { Name: "Friday", Id: "Friday" },
                    { Name: "Saturday", Id: "Saturday" }
                ];

}());
