(function() {
    var marketing = {

        loadData:function(){
        	return $.ajax({
                type: "get",
                url: get_marketing,
                dataType: "json",
                success: function(data){
                    return data;
                },
                error: function(){
                    alert("Error cannot load table");
                }
            });
        },

        insertItem: function(insertingClient) {
        	$('#loading_page').show();
        	console.log(insertingClient);
        	$.post(add_marketing,insertingClient,function(data){
        		 $("#memailgrid").jsGrid("loadData");
        		 $('#loading_page').hide();
        		 BootstrapDialog.alert("Email Saved");
           }).fail(function(){
        	   $('#loading_page').hide();
        	   BootstrapDialog.alert("Error Occured While Saving");
           });
        },

        updateItem: function(updatingClient) {
        	$('#loading_page').show();
        	console.log(updatingClient);
        	$.post(update_email,updatingClient,function(data){
        		$('#loading_page').hide();
       		 	BootstrapDialog.alert("Email Saved");
            }).fail(function(){
        	   $('#loading_page').hide();
        	   BootstrapDialog.alert("Error Occured While Saving");
           });
        },

        deleteItem: function(deletingClient) {
        	$('#loading_page').show();
        	$.post(delete_email,deletingClient,function(data){
        		$('#loading_page').hide();
       		 	BootstrapDialog.alert("Email Deleted");
            }).fail(function(){
        	   $('#loading_page').hide();
        	   BootstrapDialog.alert("Error Occured While Delete");
           });
        }
    };

    window.marketing = marketing;

}());
