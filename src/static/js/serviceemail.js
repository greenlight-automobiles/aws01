(function() {
    var serviceemail = {

        loadData:function(){
        	return $.ajax({
                type: "get",
                url: get_serviceemail,
                dataType: "json",
                success: function(data){
                    return data;
                },
                error: function(){
                    alert("Error cannot load table");
                }
            });
        },

        insertItem: function(insertingClient) {
        	console.log(insertingClient);
        	$('#loading_page').show();
        	$.post(add_serviceemail,insertingClient,function(data){
        		 $("#semailgrid").jsGrid("loadData");
        		 $('#loading_page').hide();
     		 	BootstrapDialog.alert("Email Saved");
		          }).fail(function(){
		      	   $('#loading_page').hide();
		      	   BootstrapDialog.alert("Error Occured While Saving");
		      });
        },

        updateItem: function(updatingClient) {
        	console.log(updatingClient);
        	$('#loading_page').show();
        	$.post(update_email,updatingClient,function(data){
        		$('#loading_page').hide();
    		 	BootstrapDialog.alert("Email Saved");
         }).fail(function(){
     	   $('#loading_page').hide();
     	   BootstrapDialog.alert("Error Occured While Saving");
        });
        },

        deleteItem: function(deletingClient) {
        	$('#loading_page').show();
        	$.post(delete_email,deletingClient,function(data){
        		$('#loading_page').hide();
    		 	BootstrapDialog.alert("Email Deleted");
         }).fail(function(){
     	   $('#loading_page').hide();
     	   BootstrapDialog.alert("Error Occured While Delete");
        });
        }

    };

    window.serviceemail = serviceemail;

}());
