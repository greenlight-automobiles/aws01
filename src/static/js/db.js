(function() {
    var db = {

        loadData:function(){
        	return $.ajax({
                type: "get",
                url: get_resource,
                dataType: "json",
                success: function(data){
                    return data;
                },
                error: function(){
                    alert("Error cannot load table");
                }
            });
        },

        insertItem: function(insertingClient) {
        	console.log(insertingClient);
        	$.post(add_resource,insertingClient,function(data){
        		$('#loading_page').show();
        		 $("#resourcegrid").jsGrid("loadData");
        		 $('#loading_page').hide();
  		 		BootstrapDialog.alert("Resource Saved");
 	          }).fail(function(){
 	      	   $('#loading_page').hide();
 	      	   BootstrapDialog.alert("Error Occured While Saving");
 	         });
        },

        updateItem: function(updatingClient) {
        	$('#loading_page').show();
        	console.log(updatingClient);
        	$.post(update_resource,updatingClient,function(data){
        		$('#loading_page').hide();
 		 		BootstrapDialog.alert("Resource Saved");
	          }).fail(function(){
	      	   $('#loading_page').hide();
	      	   BootstrapDialog.alert("Error Occured While Saving");
	         });
        },

        deleteItem: function(deletingClient) {
        	$('#loading_page').show();
        	$.post(delete_resource,deletingClient,function(data){
        		$('#loading_page').hide();
 		 		BootstrapDialog.alert("Resource Deleted");
	          }).fail(function(){
	      	   $('#loading_page').hide();
	      	   BootstrapDialog.alert("Error Occured While Delete");
	         });
        }

    };

    window.db = db;

}());
