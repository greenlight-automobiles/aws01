import re
from django import http
from django.conf import settings

class XFrameAllowMiddleware(object):

    def process_request(self, request):
        """
        If CORS preflight header, then create an
        empty body response (200 OK) and return it

        Django won't bother calling any other request
        view/exception middleware along with the requested view;
        it will call any response middlewares
        """
        if (self.is_enabled(request) and
                request.method == 'OPTIONS' and
                "HTTP_ACCESS_CONTROL_REQUEST_METHOD" in request.META):
            response = http.HttpResponse()
            return response
        return None

    def process_response(self, request, response):
        if self.is_enabled(request):
            response['X-Frame-Options'] = 'ALLOWALL'
        return response

    def is_enabled(self, request):


        return re.match(settings.XFRAME_URLS_REGEX, request.path)
