from django.db import models
from core.utils.hash import generate_hash, seed_generator
from django.core.exceptions import FieldError


class HashURLField(models.CharField):
    description = "A randomly seeded hash that can be used as a URL to locate this model"

    def __init__(self, *args, **kwargs):
        kwargs['max_length'] = 56
        kwargs['blank'] = True
        kwargs.setdefault('db_index', True)
        kwargs.setdefault('editable', False)
        super(HashURLField, self).__init__(*args, **kwargs)

    def calculate_hash(self, model_instance):
        i = 0
        max_tries  = 5
        for i in range(max_tries):
            h = generate_hash(seed_generator())
            if not self.model.objects.filter(**{self.attname: h}).exists():
                break

        if i == max_tries-1:
            # If we've tried 5 times and we're still getting collisions then something is very wrong!
            raise FieldError("Unable to generate unique URL for Field {} in Model: {}. Tried {} times".format(self.attname, self.model, max_tries))

        return h

    def pre_save(self, model_instance, add):
        if add or getattr(model_instance, self.attname) == str() or getattr(model_instance, self.attname) is None:
            # Calculate hash on first model save or if hash is blank
            h = self.calculate_hash(model_instance)
            setattr(model_instance, self.attname, h)

        return super(HashURLField, self).pre_save(model_instance, add)
