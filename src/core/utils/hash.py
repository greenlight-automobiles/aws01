import hashlib
import random
import string


def generate_hash(seed):
    """
    Generate hash from seed as hex string
    :param seed: The hash seed
    :return: Hex string containing hash
    """
    # TODO: Replace this with the secrets module in python 3
    m = hashlib.sha224()
    m.update(seed)
    url_hash = m.hexdigest().upper() # get the hash for the url

    # Remove unwanted characters from string
    # return hash.replace('=','').replace('/','_')  # some cleaning

    return url_hash


def seed_generator(size=16, chars=string.ascii_uppercase + string.digits):
    """
    Generate a seed (random string) from a specified set of characters
    :param size: Length of the seed
    :param chars: The set of characters to use to generate the seed
    :return: Seed containing generated ID
    """
    return ''.join(random.SystemRandom().choice(chars) for _ in range(size))
