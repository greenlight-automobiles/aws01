def is_bright(r, g, b, threshhold=123):
    brightval =  (r * 299 + g * 587 + b * 114) / 1000
    if brightval > threshhold:
        return True
    return False

def getContrastingForegroundColor(color):

    if color is None or color is '' or color is u'':
        return None

    h = color[1:] if color[0] == '#' else color
    RGB = tuple(int(h[i:i + 2], 16) for i in (0, 2, 4))
    if not is_bright(RGB[0], RGB[1], RGB[2], 100):
        return "#FFF"
    return "#444"

def getContrastingBackgoundColor(color):

    if color is None or color is u'':
        return None

    h = color[1:] if color[0] == '#' else color
    RGB = tuple(int(h[i:i + 2], 16) for i in (0, 2, 4))
    if is_bright(RGB[0], RGB[1], RGB[2]):
        return "#444"
    return "#FFF"
