# Use this file for running from the cmd line and testing hash functions.

from core.utils.hash import generate_hash, seed_generator
from core.utils.time_support import profiler_begin, profiler_end


def main():
    lookup = dict()
    c = 0
    profile = profiler_begin()
    for i in range(100000000):
            url = generate_hash(seed_generator())
            print("Generating hash {} at {}".format(url, i))

            if url in lookup:
                c += 1
                print("Collision found at {}".format(c))
            else:
                lookup[url] = None
    elapsed_time = profiler_end(profile)
    print("Collisions: {} from {} in {}: len {}".format(c, i, elapsed_time, len(url)))


if __name__ == "__main__":
    main()
