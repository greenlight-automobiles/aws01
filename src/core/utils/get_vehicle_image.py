from django.conf import settings
from dealership.models import Dealer

def get_car_image_or_default(dealer_code, vehicle):
    # Try specific vehicle image first
    if vehicle:
        # Vehicle might come through as a model object or dictionary
        if isinstance(vehicle, dict):
            if vehicle['mainimage']:
                return vehicle['mainimage'].url
        else:
            if vehicle.mainimage:
                return vehicle.mainimage.url


    if dealer_code:
        dealer = Dealer.objects.get(dealer_code=dealer_code)
        if dealer.default_car_image:
            return dealer.default_car_image.url

    # Last fall back to the website default image.
    return settings.STATIC_URL + "images/default/default_car.jpg"

def get_car_image_default(dealer_code):
    if dealer_code:
        dealer = Dealer.objects.get(dealer_code=dealer_code)
        if dealer.default_car_image:
            return dealer.default_car_image.url

    # Last fall back to the website default image.
    return settings.STATIC_URL + "images/default/default_car.jpg"
