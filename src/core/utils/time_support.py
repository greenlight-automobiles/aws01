import time


def get_hhmmss(seconds):
    """
    Concert seconds to hours, minutes, seconds.
    :param seconds: The seconds to convert to hours, minutes, seconds
    :return: Hours, Minutes, Seconds
    """
    minutes, seconds = divmod(seconds, 60)
    hours, minutes = divmod(minutes, 60)
    return hours, minutes, seconds


def str_hhmmss(hours,minutes,seconds):
    """
    Convert hours, minutes, seconds from int to string
    :param hours:
    :param minutes:
    :param seconds:
    :return:
    """
    periods = [('hours', hours), ('minutes', minutes), ('seconds', seconds)]
    return ', '.join('{} {}'.format(value, name) for name, value in periods if value)


def profiler_begin():
    """
    Convert hours, minutes, seconds from int to string
    :return: profile data
    """
    t0 = time.time()
    return t0


def profiler_end(profile):
    """
    Convert hours, minutes, seconds from int to string
    :return: profile in hours, minutes, seconds
    """
    t1 = time.time()
    t = t1 - profile
    h, m, s = get_hhmmss(t)
    return str_hhmmss(h, m, s)
