#!/bin/bash

# ===================
# author: Rupesh Shah
# ===================

cd /tmp

sudo yum update -y
sudo yum install -y wget
sudo yum install -y ruby

sudo wget https://aws-codedeploy-us-west-2.s3.amazonaws.com/latest/install
sudo chmod +x ./install
sudo ./install auto

# install other dependencies
sudo yum install -y mailx
sudo yum install -y httpd
sudo yum install -y mod24_ssl

# install development tools
sudo yum groupinstall -y "Development Tools"

# create default page in httpd to instance get health check
sudo cat > /var/www/html/index.html << EOF1
This is a welcome page!
EOF1

# web server permission changes
sudo usermod -a -G apache ec2-user
sudo chown -R ec2-user:apache /var/www
sudo chmod 2775 /var/www
find /var/www -type d -exec sudo chmod 2775 {} \;
find /var/www -type f -exec sudo chmod 0664 {} \;

sudo cat > /etc/httpd/conf.d/greenlightautomotive.conf << EOF3

<VirtualHost *:80>
        # ServerAdmin aamir.ansari5151@gmail.com
        #DocumentRoot /opt/greenlightautomotive/bmw
        DocumentRoot /opt/greenlightautomotive/www
        ServerName greenlightautomotive.com
        ServerAlias www.greenlightautomotive.com
        # RewriteEngine On
        # RewriteCond %{HTTPS} !=on
        # RewriteRule ^/?(.*) https://%{SERVER_NAME}/ [R,L]
        #<Directory "/opt/greenlightautomotive/bmw">
        #        Options -Indexes
        #        AllowOverride None
        #        Allow from all
        #</Directory>
        #Alias /bmw/static /data/www/bmw/BMW/static
        #<Directory /data/www/bmw/BMW/static>
        #        Require all granted
        #</Directory>

        <Directory "/opt/greenlightautomotive/www">
                Options -Indexes
                AllowOverride None
                Order Allow,Deny
                Allow from all
        </Directory>

        <Directory /opt/greenlightautomotive/app/src>
                <Files wsgi.py>
                        Order Deny,Allow
                        Allow from all
                </Files>
        </Directory>

        Alias /app/static /opt/greenlightautomotive/app/static
        <Directory /opt/greenlightautomotive/app/static>
                Options -Indexes
                Order Deny,Allow
                Allow from all
        </Directory>

        Alias /app/media /opt/greenlightautomotive/app/media
        <Directory /opt/greenlightautomotive/app/media>
                Options -Indexes
                Order Deny,Allow
                Allow from all
        </Directory>

        SetEnv DJANGO_SETTINGS_MODULE settings.REPLACE
        WSGIDaemonProcess app python-path=/opt/greenlightautomotive/app/src:/opt/greenlightautomotive/greenlightautomotive_env/lib/python2.7/site-packages:/opt/greenlightautomotive/greenlightautomotive_env/lib/python2.7/dist-packages
        WSGIProcessGroup app
        WSGIScriptAlias /app /opt/greenlightautomotive/app/src/wsgi.py
</VirtualHost>

WSGISocketPrefix /var/run/wsgi

EOF3

#chmod 777 /etc/httpd/greenlightautomotive.conf

# start the service so instance starts receiving signal from web site
#sudo apachectl start
