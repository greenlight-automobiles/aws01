#!/bin/bash

# ===================
# author: Rupesh Shah
# ===================


# ================================
# Setting environment variable
# ================================



if [ "$DEPLOYMENT_GROUP_NAME" == "PROD" ]
then

	# cp -f /opt/greenlightautomotive/conf/prod.settings.py /opt/greenlightautomotive/app/src/settings/settings.py
	# rm -rf /opt/greenlightautomotive/conf/
    ## sed -i -e 's/Listen 80/Listen 9090/g' /etc/httpd/conf/httpd.conf

	DJANGO_SETTINGS_MODULE=settings.production
	export DJANGO_SETTINGS_MODULE

    sudo chmod +x /opt/greenlightautomotive/management/backup/backup-greenlight


elif [ "$DEPLOYMENT_GROUP_NAME" == "SAT" ]
then

	# cp -f /opt/greenlightautomotive/conf/sat.settings.py /opt/greenlightautomotive/app/src/settings/settings.py
	# rm -rf /opt/greenlightautomotive/conf/
    ## sed -i -e 's/Listen 80/Listen 9090/g' /etc/httpd/conf/httpd.conf

	DJANGO_SETTINGS_MODULE=settings.test
	export DJANGO_SETTINGS_MODULE

else

	DJANGO_SETTINGS_MODULE=settings.test
	export DJANGO_SETTINGS_MODULE

fi

# ================================
# install app
# ================================
# create virtual environment
cd /opt/greenlightautomotive
/usr/local/bin/virtualenv -p /usr/bin/python greenlightautomotive_env
source greenlightautomotive_env/bin/activate
pip install -U pip

if [ "$DEPLOYMENT_GROUP_NAME" == "PROD" ]
then
    pip install -r app/src/requirements/production.txt
elif [ "$DEPLOYMENT_GROUP_NAME" == "SAT" ]
then
    pip install -r app/src/requirements/test.txt
else
    pip install -r app/src/requirements/test.txt
fi

cd app/src
python manage.py migrate --noinput
python manage.py collectstatic -c --noinput
deactivate

sudo chown -R ec2-user:apache /opt/greenlightautomotive/app/static
sudo chown -R ec2-user:apache /opt/greenlightautomotive/app/src
