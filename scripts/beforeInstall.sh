#!/bin/bash

# ===================
# author: Rupesh Shah
# ===================


# ================================
# move dependencies here
# ================================
sudo yum update -y
sudo yum install -y python-pip
sudo pip install virtualenv==13.1.2
#sudo yum install -y python-virtualenv
#sudo yum install -y mod24_ssl
sudo yum install -y mysql-devel
sudo yum install -y mod_wsgi-python27.x86_64

# ================================
# Setup folders
# ================================
if ! [ -d /opt/greenlightautomotive ]
then
  sudo mkdir -p /opt/greenlightautomotive
fi

# create app folder
if ! [ -d /opt/greenlightautomotive/app ]
then
  sudo mkdir -p /opt/greenlightautomotive/app
fi

# create app folder
if ! [ -d /opt/greenlightautomotive/www ]
then
  sudo mkdir -p /opt/greenlightautomotive/www
fi

# remove src folder
if [ -d /opt/greenlightautomotive/greenlightautomotive_env ]
then
  sudo rm -rf /opt/greenlightautomotive/greenlightautomotive_env
fi

# remove src folder
if [ -d /opt/greenlightautomotive/app/src ]
then
  sudo rm -rf /opt/greenlightautomotive/app/src
fi

# remove conf folder
if [ -d /opt/greenlightautomotive/conf ]
then
  sudo rm -rf /opt/greenlightautomotive/conf
fi

# create media folder
if ! [ -d /opt/greenlightautomotive/app/media ]
then
  sudo mkdir -p /opt/greenlightautomotive/app/media
  sudo chmod 777 /opt/greenlightautomotive/app/media
fi

# create static folder
if ! [ -d /opt/greenlightautomotive/app/static ]
then
  sudo mkdir -p /opt/greenlightautomotive/app/static
fi

# create management
if ! [ -d /opt/greenlightautomotive/management ]
then
  sudo mkdir -p /opt/greenlightautomotive/management
  sudo chmod 777 /opt/greenlightautomotive/management
fi

# ================================
# apache config file
# ================================

if [ "$DEPLOYMENT_GROUP_NAME" == "PROD" ]
then

	echo "This is PROD deployment."
    sed -i -e 's/settings.REPLACE/settings.production/g' /etc/httpd/conf.d/greenlightautomotive.conf

elif [ "$DEPLOYMENT_GROUP_NAME" == "SAT" ]
then

	echo "This is SAT deployment."
    sed -i -e 's/settings.REPLACE/settings.test/g' /etc/httpd/conf.d/greenlightautomotive.conf

else

	echo "We will make this a SAT deployment by default."
    sed -i -e 's/settings.REPLACE/settings.test/g' /etc/httpd/conf.d/greenlightautomotive.conf

fi

# ================================
# change permissions
# ================================
sudo chown -R ec2-user:apache /opt/greenlightautomotive
sudo chmod 2775 /opt/greenlightautomotive
find /opt/greenlightautomotive -type d -exec sudo chmod 2775 {} \;
find /opt/greenlightautomotive -type f -exec sudo chmod 0664 {} \;


# create default page in httpd if it doesn't exist.
sudo cat > /opt/greenlightautomotive/www/index.html << EOF1
<!DOCTYPE html>
<html lang="en">
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
<title>Greenlight Automotive</title>
<meta name="description" content="Greenlight Automotive Home Page">
<style type="text/css">
    body
    {
        margin:   0;
        overflow: hidden;
    }

    #iframe-fullscreen
    {
        height:   100%;
        left:     0px;
        position: absolute;
        top:      0px;
        width:    100%;
    }
</style>
</head>
<body>
    <iframe id="iframe-fullscreen" src="https://greenlightautomotive.weebly.com" frameborder="0"></iframe>
</body>
</html>
EOF1


