#!/bin/bash

# =============================
# author: Rupesh Shah
# =============================

# =============================
# service stop
# =============================

# service=apachectl

# if (( $(ps -ef | grep -v grep | grep $service | wc -l) > 0 ))
# then
#   sudo apachectl stop
# else
#   echo "$service is not running!!!"
# fi

sudo apachectl stop

# another way to stop
# sudo service httpd stop

# =============================
# Backup of current code if any
# =============================

if [ -d /tmp/backup_greenlightautomotive ]
then
  sudo rm -rf /tmp/backup_greenlightautomotive
fi

if [ -d /opt/greenlightautomotive/conf ]
then
  sudo rm -rf /opt/greenlightautomotive/conf
fi

if [ -d /opt/greenlightautomotive/app/src ]
then
  sudo cp -rf /opt/greenlightautomotive/app/src /tmp/backup_greenlightautomotive
fi
