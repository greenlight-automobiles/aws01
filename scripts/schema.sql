-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema greenlight
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema greenlight
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `greenlight` DEFAULT CHARACTER SET latin1 ;
USE `greenlight` ;

-- -----------------------------------------------------
-- Table `greenlight`.`auth_group`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `greenlight`.`auth_group` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(80) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `name` (`name` ASC))
ENGINE = InnoDB
AUTO_INCREMENT = 6
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `greenlight`.`django_content_type`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `greenlight`.`django_content_type` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `app_label` VARCHAR(100) NOT NULL,
  `model` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `django_content_type_app_label_45f3b1d93ec8c61c_uniq` (`app_label` ASC, `model` ASC))
ENGINE = InnoDB
AUTO_INCREMENT = 78
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `greenlight`.`auth_permission`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `greenlight`.`auth_permission` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) NOT NULL,
  `content_type_id` INT(11) NOT NULL,
  `codename` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `content_type_id` (`content_type_id` ASC, `codename` ASC),
  CONSTRAINT `auth__content_type_id_508cf46651277a81_fk_django_content_type_id`
    FOREIGN KEY (`content_type_id`)
    REFERENCES `greenlight`.`django_content_type` (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 232
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `greenlight`.`auth_group_permissions`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `greenlight`.`auth_group_permissions` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `group_id` INT(11) NOT NULL,
  `permission_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `group_id` (`group_id` ASC, `permission_id` ASC),
  INDEX `auth_group__permission_id_1f49ccbbdc69d2fc_fk_auth_permission_id` (`permission_id` ASC),
  CONSTRAINT `auth_group__permission_id_1f49ccbbdc69d2fc_fk_auth_permission_id`
    FOREIGN KEY (`permission_id`)
    REFERENCES `greenlight`.`auth_permission` (`id`),
  CONSTRAINT `auth_group_permission_group_id_689710a9a73b7457_fk_auth_group_id`
    FOREIGN KEY (`group_id`)
    REFERENCES `greenlight`.`auth_group` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `greenlight`.`auth_user`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `greenlight`.`auth_user` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `password` VARCHAR(128) NOT NULL,
  `last_login` DATETIME NULL DEFAULT NULL,
  `is_superuser` TINYINT(1) NOT NULL,
  `username` VARCHAR(30) NOT NULL,
  `first_name` VARCHAR(30) NOT NULL,
  `last_name` VARCHAR(30) NOT NULL,
  `email` VARCHAR(254) NOT NULL,
  `is_staff` TINYINT(1) NOT NULL,
  `is_active` TINYINT(1) NOT NULL,
  `date_joined` DATETIME NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `username` (`username` ASC))
ENGINE = InnoDB
AUTO_INCREMENT = 213
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `greenlight`.`auth_user_groups`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `greenlight`.`auth_user_groups` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `user_id` INT(11) NOT NULL,
  `group_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `user_id` (`user_id` ASC, `group_id` ASC),
  INDEX `auth_user_groups_group_id_33ac548dcf5f8e37_fk_auth_group_id` (`group_id` ASC),
  CONSTRAINT `auth_user_groups_group_id_33ac548dcf5f8e37_fk_auth_group_id`
    FOREIGN KEY (`group_id`)
    REFERENCES `greenlight`.`auth_group` (`id`),
  CONSTRAINT `auth_user_groups_ibfk_1`
    FOREIGN KEY (`user_id`)
    REFERENCES `greenlight`.`auth_user` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
AUTO_INCREMENT = 250
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `greenlight`.`auth_user_user_permissions`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `greenlight`.`auth_user_user_permissions` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `user_id` INT(11) NOT NULL,
  `permission_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `user_id` (`user_id` ASC, `permission_id` ASC),
  INDEX `auth_user_u_permission_id_384b62483d7071f0_fk_auth_permission_id` (`permission_id` ASC),
  CONSTRAINT `auth_user_u_permission_id_384b62483d7071f0_fk_auth_permission_id`
    FOREIGN KEY (`permission_id`)
    REFERENCES `greenlight`.`auth_permission` (`id`),
  CONSTRAINT `auth_user_user_permissi_user_id_7f0938558328534a_fk_auth_user_id`
    FOREIGN KEY (`user_id`)
    REFERENCES `greenlight`.`auth_user` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `greenlight`.`dealership_questions`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `greenlight`.`dealership_questions` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `question_text` VARCHAR(200) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 6
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `greenlight`.`dealership_states`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `greenlight`.`dealership_states` (
  `id` INT(11) NOT NULL,
  `name` VARCHAR(96) NOT NULL,
  `state_abbr` VARCHAR(24) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `greenlight`.`dealership_dealer`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `greenlight`.`dealership_dealer` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(100) NULL DEFAULT NULL,
  `dealer_code` VARCHAR(100) NULL DEFAULT NULL,
  `address_line1` LONGTEXT NULL DEFAULT NULL,
  `address_line2` LONGTEXT NULL DEFAULT NULL,
  `city` VARCHAR(2000) NULL DEFAULT NULL,
  `consumer_access` TINYINT(1) NOT NULL,
  `created_at` DATETIME NOT NULL,
  `created_by_id` INT(11) NULL DEFAULT NULL,
  `description` LONGTEXT NULL DEFAULT NULL,
  `dms_access` TINYINT(1) NOT NULL,
  `frameset_url` VARCHAR(250) NULL DEFAULT NULL,
  `message_of_the_day` LONGTEXT NULL DEFAULT NULL,
  `privacy_policy` LONGTEXT NULL DEFAULT NULL,
  `privacy_polilcy` VARCHAR(100) NULL DEFAULT NULL,
  `service_url` VARCHAR(250) NULL DEFAULT NULL,
  `state_us_id` INT(11) NULL DEFAULT NULL,
  `timezone` VARCHAR(255) NULL DEFAULT NULL,
  `webkey` VARCHAR(250) NULL DEFAULT NULL,
  `zipcode` VARCHAR(2000) NULL DEFAULT NULL,
  `default_advisor_id` INT(11) NULL DEFAULT NULL,
  `country` VARCHAR(2) NULL DEFAULT NULL,
  `logo` VARCHAR(100) NULL DEFAULT NULL,
  `client_id` LONGTEXT NULL DEFAULT NULL,
  `mode` VARCHAR(250) NULL DEFAULT NULL,
  `secret` LONGTEXT NULL DEFAULT NULL,
  `ico_logo` VARCHAR(100) NULL DEFAULT NULL,
  `approval_needed_flag_id` INT(11) NULL DEFAULT NULL,
  `carwash_flag_id` INT(11) NULL DEFAULT NULL,
  `price_unit` VARCHAR(10) NOT NULL,
  `from_email` VARCHAR(2000) NOT NULL,
  `number` VARCHAR(2000) NOT NULL,
  `prestagevehicle_flag_id` INT(11) NULL DEFAULT NULL,
  `pickup_flag_id` INT(11) NULL DEFAULT NULL,
  `queue_flag_id` INT(11) NULL DEFAULT NULL,
  `servicecomplete_flag_id` INT(11) NULL DEFAULT NULL,
  `workingonvehicle_flag_id` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `dealership_dealer_dealer_code_e66d441e0b9adc1_uniq` (`dealer_code` ASC),
  INDEX `dealership_dealer_e93cb7eb` (`created_by_id` ASC),
  INDEX `dealership_dealer_1cf7da2f` (`state_us_id` ASC),
  INDEX `dealership_dealer_127c58d5` (`default_advisor_id` ASC),
  CONSTRAINT `dealership__state_us_id_18cc515ec3d802ea_fk_dealership_states_id`
    FOREIGN KEY (`state_us_id`)
    REFERENCES `greenlight`.`dealership_states` (`id`),
  CONSTRAINT `dealership_d_default_advisor_id_59e88a5becd5b3d1_fk_auth_user_id`
    FOREIGN KEY (`default_advisor_id`)
    REFERENCES `greenlight`.`auth_user` (`id`),
  CONSTRAINT `dealership_dealer_created_by_id_ff266aa32145d1a_fk_auth_user_id`
    FOREIGN KEY (`created_by_id`)
    REFERENCES `greenlight`.`auth_user` (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 6
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `greenlight`.`dealership_userprofile`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `greenlight`.`dealership_userprofile` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `answer` VARCHAR(255) NULL DEFAULT NULL,
  `question_id` INT(11) NULL DEFAULT NULL,
  `user_id` INT(11) NULL DEFAULT NULL,
  `token` VARCHAR(255) NULL DEFAULT NULL,
  `token_expiry` DATE NULL DEFAULT NULL,
  `active_email` VARCHAR(255) NULL DEFAULT NULL,
  `terms_agreed` LONGBLOB NOT NULL,
  `active_email_date` DATE NULL DEFAULT NULL,
  `active_phone_number` VARCHAR(2000) NULL DEFAULT NULL,
  `active_phone_number_date` DATE NULL DEFAULT NULL,
  `available_for_chat` BINARY(1) NULL DEFAULT '0',
  `number_of_chats` INT(11) NULL DEFAULT '0',
  `skip_confirmation` TINYINT(1) NOT NULL,
  `mode_of_sending_updates` VARCHAR(20) NOT NULL,
  `avatar` VARCHAR(100) NULL DEFAULT NULL,
  `city` VARCHAR(2000) NULL DEFAULT NULL,
  `country` VARCHAR(2) NOT NULL,
  `zipcode` VARCHAR(2000) NULL DEFAULT NULL,
  `address_line1` LONGTEXT NULL DEFAULT NULL,
  `address_line2` LONGTEXT NULL DEFAULT NULL,
  `email_1` VARCHAR(255) NULL DEFAULT NULL,
  `email_2` VARCHAR(255) NULL DEFAULT NULL,
  `method_of_contact` VARCHAR(20) NULL DEFAULT NULL,
  `phone_1_type` VARCHAR(255) NOT NULL,
  `phone_2_type` VARCHAR(255) NOT NULL,
  `phone_3_type` VARCHAR(255) NOT NULL,
  `phone_number_1` VARCHAR(2000) NOT NULL,
  `phone_number_2` VARCHAR(2000) NULL DEFAULT NULL,
  `phone_number_3` VARCHAR(2000) NULL DEFAULT NULL,
  `state_us_id` INT(11) NULL DEFAULT NULL,
  `dealer_id` INT(11) NULL DEFAULT NULL,
  `first_name` VARCHAR(255) NULL DEFAULT NULL,
  `last_name` VARCHAR(255) NULL DEFAULT NULL,
  `customer_notes` LONGTEXT NULL DEFAULT NULL,
  `special_offer_notify` TINYINT(1) NOT NULL,
  `employee_no` VARCHAR(255) NULL DEFAULT NULL,
  `consumer_reserver` TINYINT(1) NOT NULL,
  `carrier_choices` VARCHAR(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `user_id` (`user_id` ASC),
  INDEX `dealership_userprofile_1cf7da2f` (`state_us_id` ASC),
  INDEX `dealership_userprofile_fad77b25` (`dealer_id` ASC),
  INDEX `dealersh_question_id_33f92dfbe9cdd6fc_fk_dealership_questions_id` (`question_id` ASC),
  CONSTRAINT `dealersh_question_id_33f92dfbe9cdd6fc_fk_dealership_questions_id`
    FOREIGN KEY (`question_id`)
    REFERENCES `greenlight`.`dealership_questions` (`id`),
  CONSTRAINT `dealership__state_us_id_45f93d28f900c10e_fk_dealership_states_id`
    FOREIGN KEY (`state_us_id`)
    REFERENCES `greenlight`.`dealership_states` (`id`),
  CONSTRAINT `dealership_use_dealer_id_8653738e4578e71_fk_dealership_dealer_id`
    FOREIGN KEY (`dealer_id`)
    REFERENCES `greenlight`.`dealership_dealer` (`id`),
  CONSTRAINT `dealership_userprofile_ibfk_1`
    FOREIGN KEY (`user_id`)
    REFERENCES `greenlight`.`auth_user` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
AUTO_INCREMENT = 1338
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `greenlight`.`dealership_vinmodel`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `greenlight`.`dealership_vinmodel` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(200) NOT NULL,
  `val` VARCHAR(200) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 4860
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `greenlight`.`dealership_vinmake`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `greenlight`.`dealership_vinmake` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(200) NOT NULL,
  `val` VARCHAR(200) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 262
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `greenlight`.`dealership_vinyear`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `greenlight`.`dealership_vinyear` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(200) NOT NULL,
  `val` VARCHAR(200) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 54
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `greenlight`.`dealership_vehicle`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `greenlight`.`dealership_vehicle` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `model_id` INT(11) NOT NULL,
  `year_id` INT(11) NOT NULL,
  `mainimage` VARCHAR(100) NULL DEFAULT NULL,
  `make_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `dealership_vehicle_make_id_3591bdc63c8d06be_uniq` (`make_id` ASC),
  INDEX `dealership_vehicle_model_id_3b2a232c16adf8de_uniq` (`model_id` ASC),
  INDEX `dealership_vehicle_year_id_6b175c7544a0b81b_uniq` (`year_id` ASC),
  CONSTRAINT `dealership_v_model_id_3b2a232c16adf8de_fk_dealership_vinmodel_id`
    FOREIGN KEY (`model_id`)
    REFERENCES `greenlight`.`dealership_vinmodel` (`id`),
  CONSTRAINT `dealership_veh_make_id_3591bdc63c8d06be_fk_dealership_vinmake_id`
    FOREIGN KEY (`make_id`)
    REFERENCES `greenlight`.`dealership_vinmake` (`id`),
  CONSTRAINT `dealership_veh_year_id_6b175c7544a0b81b_fk_dealership_vinyear_id`
    FOREIGN KEY (`year_id`)
    REFERENCES `greenlight`.`dealership_vinyear` (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 8260
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `greenlight`.`dealership_customervehicle`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `greenlight`.`dealership_customervehicle` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `lisence_number` VARCHAR(20) NULL DEFAULT NULL,
  `milage` INT(11) NULL DEFAULT NULL,
  `vin_number` VARCHAR(50) NULL DEFAULT NULL,
  `created_at` DATETIME NOT NULL,
  `user_id` INT(11) NULL DEFAULT NULL,
  `vehicle_id` INT(11) NULL DEFAULT NULL,
  `color` VARCHAR(20) NULL DEFAULT NULL,
  `vin_image` VARCHAR(100) NULL DEFAULT NULL,
  `vin_process` TINYINT(1) NOT NULL,
  `trim_id` INT(11) NULL DEFAULT NULL,
  `vin_data` LONGTEXT NULL DEFAULT NULL,
  `customer_vehicle_desc` LONGTEXT NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `dealership_customervehicle_1e621090` (`vehicle_id` ASC),
  INDEX `dealership_user_id_7f9726d88051223f_fk_dealership_userprofile_id` (`user_id` ASC),
  INDEX `dealership_customervehicle_d05c2a5e` (`trim_id` ASC),
  CONSTRAINT `dealership_customervehicle_ibfk_1`
    FOREIGN KEY (`user_id`)
    REFERENCES `greenlight`.`dealership_userprofile` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `dealership_customervehicle_ibfk_2`
    FOREIGN KEY (`vehicle_id`)
    REFERENCES `greenlight`.`dealership_vehicle` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
AUTO_INCREMENT = 1644
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `greenlight`.`dealership_flags`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `greenlight`.`dealership_flags` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) NOT NULL,
  `type` INT(11) NOT NULL,
  `customer_facing` TINYINT(1) NOT NULL,
  `notes` VARCHAR(255) NULL DEFAULT NULL,
  `color` VARCHAR(20) NOT NULL,
  `dealer_id` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `dealer_id` (`dealer_id` ASC),
  CONSTRAINT `dealership_flags_ibfk_1`
    FOREIGN KEY (`dealer_id`)
    REFERENCES `greenlight`.`dealership_dealer` (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 176
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `greenlight`.`dealership_ro`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `greenlight`.`dealership_ro` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `ro_number` VARCHAR(20) NOT NULL,
  `rfid_tag` VARCHAR(20) NOT NULL,
  `ro_date` DATETIME NULL DEFAULT NULL,
  `inspection_status` VARCHAR(20) NOT NULL,
  `ro_status` TINYINT(1) NOT NULL,
  `flag1_id` INT(11) NULL DEFAULT NULL,
  `flag2_id` INT(11) NULL DEFAULT NULL,
  `flag3_id` INT(11) NULL DEFAULT NULL,
  `flag1_updated_time` DATETIME NULL DEFAULT NULL,
  `flag2_updated_time` DATETIME NULL DEFAULT NULL,
  `flag3_updated_time` DATETIME NULL DEFAULT NULL,
  `inspector_id` INT(11) NULL DEFAULT NULL,
  `flag1_updated_by_id` INT(11) NULL DEFAULT NULL,
  `flag2_updated_by_id` INT(11) NULL DEFAULT NULL,
  `flag3_updated_by_id` INT(11) NULL DEFAULT NULL,
  `shop_notes` VARCHAR(255) NOT NULL,
  `ro_completed` DATETIME NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `ro_number` (`ro_number` ASC),
  UNIQUE INDEX `rfid_tag` (`rfid_tag` ASC),
  INDEX `dealership_ro_flag1_id_52e414d767741214_uniq` (`flag1_id` ASC),
  INDEX `dealership_ro_flag2_id_6d0288a12cb91b8b_uniq` (`flag2_id` ASC),
  INDEX `dealership_ro_flag3_id_43d03245249b7aaa_uniq` (`flag3_id` ASC),
  INDEX `dealership_ro_39ffa7b5` (`inspector_id` ASC),
  INDEX `dealership_ro_b1e45cd9` (`flag1_updated_by_id` ASC),
  INDEX `dealership_ro_9cf5cefa` (`flag2_updated_by_id` ASC),
  INDEX `dealership_ro_2868e264` (`flag3_updated_by_id` ASC),
  CONSTRAINT `dealership__flag1_updated_by_id_654826116d193d69_fk_auth_user_id`
    FOREIGN KEY (`flag1_updated_by_id`)
    REFERENCES `greenlight`.`auth_user` (`id`),
  CONSTRAINT `dealership__flag2_updated_by_id_5f298094663ed22e_fk_auth_user_id`
    FOREIGN KEY (`flag2_updated_by_id`)
    REFERENCES `greenlight`.`auth_user` (`id`),
  CONSTRAINT `dealership__flag3_updated_by_id_690cd82a2b56a5ef_fk_auth_user_id`
    FOREIGN KEY (`flag3_updated_by_id`)
    REFERENCES `greenlight`.`auth_user` (`id`),
  CONSTRAINT `dealership_ro_flag1_id_52e414d767741214_fk_dealership_flags_id`
    FOREIGN KEY (`flag1_id`)
    REFERENCES `greenlight`.`dealership_flags` (`id`),
  CONSTRAINT `dealership_ro_flag2_id_6d0288a12cb91b8b_fk_dealership_flags_id`
    FOREIGN KEY (`flag2_id`)
    REFERENCES `greenlight`.`dealership_flags` (`id`),
  CONSTRAINT `dealership_ro_flag3_id_43d03245249b7aaa_fk_dealership_flags_id`
    FOREIGN KEY (`flag3_id`)
    REFERENCES `greenlight`.`dealership_flags` (`id`),
  CONSTRAINT `dealership_ro_inspector_id_672580a51e3e5880_fk_auth_user_id`
    FOREIGN KEY (`inspector_id`)
    REFERENCES `greenlight`.`auth_user` (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 5135
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `greenlight`.`dealership_appointmentstatus`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `greenlight`.`dealership_appointmentstatus` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(100) NULL DEFAULT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 13
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `greenlight`.`dealership_wayaway`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `greenlight`.`dealership_wayaway` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(100) NULL DEFAULT NULL,
  `description` VARCHAR(2000) NULL DEFAULT NULL,
  `show_description` TINYINT(1) NOT NULL,
  `show_dl` TINYINT(1) NOT NULL,
  `popup_description` VARCHAR(2000) NOT NULL,
  `reserve_wayaway` TINYINT(1) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 6
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `greenlight`.`dealership_appointment`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `greenlight`.`dealership_appointment` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `start_time` DATETIME NULL DEFAULT NULL,
  `end_time` DATETIME NULL DEFAULT NULL,
  `advisor_id` INT(11) NULL DEFAULT NULL,
  `appointment_status_id` INT(11) NULL DEFAULT NULL,
  `customer_id` INT(11) NULL DEFAULT NULL,
  `way_away_id` INT(11) NULL DEFAULT NULL,
  `vehicle_id` INT(11) NULL DEFAULT NULL,
  `ro_id` INT(11) NULL DEFAULT NULL,
  `confirmation_code` VARCHAR(2000) NULL DEFAULT NULL,
  `contact_me` TINYINT(1) NOT NULL,
  `driver_liscens_number` VARCHAR(255) NULL DEFAULT NULL,
  `insurance_card_number` VARCHAR(255) NULL DEFAULT NULL,
  `insurance_company_name` VARCHAR(255) NULL DEFAULT NULL,
  `state_wayaway_id` INT(11) NULL DEFAULT NULL,
  `maintenance` TINYINT(1) NOT NULL,
  `service_notes` LONGTEXT NULL DEFAULT NULL,
  `comments` LONGTEXT NULL DEFAULT NULL,
  `contact_time` VARCHAR(20) NOT NULL,
  `dealer_id` INT(11) NULL DEFAULT NULL,
  `odometer_reading` VARCHAR(255) NULL DEFAULT NULL,
  `checkin_time` DATETIME NULL DEFAULT NULL,
  `customer_signatures` LONGTEXT NULL DEFAULT NULL,
  `creditcard_id` VARCHAR(2000) NULL DEFAULT NULL,
  `payment_status` TINYINT(1) NOT NULL,
  `payment_id` VARCHAR(2000) NULL DEFAULT NULL,
  `reserve_wayaway` TINYINT(1) NOT NULL,
  `appointment_reminder_status` TINYINT(1) NOT NULL,
  `appointment_recommandation_status` TINYINT(1) NOT NULL,
  `odometer_data` LONGTEXT NOT NULL,
  `odometer_image` VARCHAR(100) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `dealership_appointment_5c1f7fab` (`appointment_status_id` ASC),
  INDEX `dealership_appointment_2208ead3` (`customer_id` ASC),
  INDEX `dealership_appointment_bc29ee61` (`way_away_id` ASC),
  INDEX `dealership_appointment_35ec04dc` (`vehicle_id` ASC),
  INDEX `dealership_appointment_45403822` (`ro_id` ASC),
  INDEX `dealership_appointmen_advisor_id_56fd5f3f4af76dc_fk_auth_user_id` (`advisor_id` ASC),
  INDEX `dealership_appointment_d5582625` (`state_wayaway_id` ASC),
  INDEX `dealership_appointment_fad77b25` (`dealer_id` ASC),
  CONSTRAINT `dea_vehicle_id_149aa4a865782e04_fk_dealership_customervehicle_id`
    FOREIGN KEY (`vehicle_id`)
    REFERENCES `greenlight`.`dealership_customervehicle` (`id`),
  CONSTRAINT `dealer_state_wayaway_id_297ca8fcf2307c99_fk_dealership_states_id`
    FOREIGN KEY (`state_wayaway_id`)
    REFERENCES `greenlight`.`dealership_states` (`id`),
  CONSTRAINT `dealership_ap_dealer_id_4287e32c69730042_fk_dealership_dealer_id`
    FOREIGN KEY (`dealer_id`)
    REFERENCES `greenlight`.`dealership_dealer` (`id`),
  CONSTRAINT `dealership_appointmen_advisor_id_56fd5f3f4af76dc_fk_auth_user_id`
    FOREIGN KEY (`advisor_id`)
    REFERENCES `greenlight`.`auth_user` (`id`),
  CONSTRAINT `dealership_appointmen_ro_id_3ee8cc2406b77460_fk_dealership_ro_id`
    FOREIGN KEY (`ro_id`)
    REFERENCES `greenlight`.`dealership_ro` (`id`),
  CONSTRAINT `dealership_appointment_ibfk_1`
    FOREIGN KEY (`appointment_status_id`)
    REFERENCES `greenlight`.`dealership_appointmentstatus` (`id`),
  CONSTRAINT `dealership_way_away_id_318ae0bb34294233_fk_dealership_wayaway_id`
    FOREIGN KEY (`way_away_id`)
    REFERENCES `greenlight`.`dealership_wayaway` (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 6333
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `greenlight`.`customer_credentialsmodel`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `greenlight`.`customer_credentialsmodel` (
  `id_id` INT(11) NOT NULL,
  `credential` LONGTEXT NULL DEFAULT NULL,
  PRIMARY KEY (`id_id`),
  CONSTRAINT `customer_credentialsmodel_ibfk_2`
    FOREIGN KEY (`id_id`)
    REFERENCES `greenlight`.`dealership_appointment` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `greenlight`.`customer_flowmodel`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `greenlight`.`customer_flowmodel` (
  `id_id` INT(11) NOT NULL,
  `flow` LONGTEXT NULL DEFAULT NULL,
  PRIMARY KEY (`id_id`),
  CONSTRAINT `customer_flowmodel_ibfk_2`
    FOREIGN KEY (`id_id`)
    REFERENCES `greenlight`.`dealership_appointment` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `greenlight`.`dealership_advisorcapacity`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `greenlight`.`dealership_advisorcapacity` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `monday` TINYINT(1) NOT NULL,
  `tuesday` TINYINT(1) NOT NULL,
  `wednesday` TINYINT(1) NOT NULL,
  `thursday` TINYINT(1) NOT NULL,
  `friday` TINYINT(1) NOT NULL,
  `saturday` TINYINT(1) NOT NULL,
  `advisor_id` INT(11) NOT NULL,
  `shop_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `dealership_advisorca_advisor_id_13ee46637b38bff7_fk_auth_user_id` (`advisor_id` ASC),
  INDEX `dealership_advi_shop_id_16de970e0e1d9164_fk_dealership_dealer_id` (`shop_id` ASC),
  CONSTRAINT `dealership_advi_shop_id_16de970e0e1d9164_fk_dealership_dealer_id`
    FOREIGN KEY (`shop_id`)
    REFERENCES `greenlight`.`dealership_dealer` (`id`),
  CONSTRAINT `dealership_advisorca_advisor_id_13ee46637b38bff7_fk_auth_user_id`
    FOREIGN KEY (`advisor_id`)
    REFERENCES `greenlight`.`auth_user` (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 48
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `greenlight`.`dealership_advisorrestrictions`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `greenlight`.`dealership_advisorrestrictions` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `monday` TINYINT(1) NOT NULL,
  `tuesday` TINYINT(1) NOT NULL,
  `wednesday` TINYINT(1) NOT NULL,
  `thursday` TINYINT(1) NOT NULL,
  `friday` TINYINT(1) NOT NULL,
  `saturday` TINYINT(1) NOT NULL,
  `start_date` DATE NULL DEFAULT NULL,
  `end_date` DATE NULL DEFAULT NULL,
  `start_time` TIME NULL DEFAULT NULL,
  `end_time` TIME NULL DEFAULT NULL,
  `repeat` TINYINT(1) NOT NULL,
  `type` VARCHAR(100) NULL DEFAULT NULL,
  `advisor_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `dealership_advisorre_advisor_id_2fe436503c940f5e_fk_auth_user_id` (`advisor_id` ASC),
  CONSTRAINT `dealership_advisorre_advisor_id_2fe436503c940f5e_fk_auth_user_id`
    FOREIGN KEY (`advisor_id`)
    REFERENCES `greenlight`.`auth_user` (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 42
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `greenlight`.`dealership_amenities`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `greenlight`.`dealership_amenities` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(250) NULL DEFAULT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `greenlight`.`dealership_availability_status`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `greenlight`.`dealership_availability_status` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(250) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 2
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `greenlight`.`dealership_servicerepair`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `greenlight`.`dealership_servicerepair` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(250) NULL DEFAULT NULL,
  `dms_opcode` VARCHAR(20) NULL DEFAULT NULL,
  `duration` VARCHAR(10) NULL DEFAULT NULL,
  `price` FLOAT(11,2) NULL DEFAULT '0.00',
  `price_unit` VARCHAR(10) NOT NULL,
  `flag_service` TINYINT(1) NOT NULL,
  `description` LONGTEXT NULL DEFAULT NULL,
  `image` VARCHAR(100) NULL DEFAULT NULL,
  `type` VARCHAR(1) NOT NULL,
  `created_at` DATETIME NOT NULL,
  `created_by_id` INT(11) NULL DEFAULT NULL,
  `dealer_id` INT(11) NULL DEFAULT NULL,
  `labor` DOUBLE NOT NULL,
  `parts` DOUBLE NOT NULL,
  `availablity_id` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `dealership_servicerepair_e93cb7eb` (`created_by_id` ASC),
  INDEX `dealership_servicerepair_fad77b25` (`dealer_id` ASC),
  INDEX `dealership_servicerepair_1a479d9d` (`availablity_id` ASC),
  CONSTRAINT `D83947235704cf59642ebe11f65764cc`
    FOREIGN KEY (`availablity_id`)
    REFERENCES `greenlight`.`dealership_availability_status` (`id`),
  CONSTRAINT `dealership_se_dealer_id_58d176c00fb0cc5b_fk_dealership_dealer_id`
    FOREIGN KEY (`dealer_id`)
    REFERENCES `greenlight`.`dealership_dealer` (`id`),
  CONSTRAINT `dealership_service_created_by_id_c0bd2c9325d9b8e_fk_auth_user_id`
    FOREIGN KEY (`created_by_id`)
    REFERENCES `greenlight`.`auth_user` (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 95
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `greenlight`.`dealership_appointmentrecommendation`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `greenlight`.`dealership_appointmentrecommendation` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `status` VARCHAR(10) NOT NULL,
  `appointment_id` INT(11) NOT NULL,
  `service_id` INT(11) NULL DEFAULT NULL,
  `technician_id` INT(11) NULL DEFAULT NULL,
  `recommnded_by_id` INT(11) NULL DEFAULT NULL,
  `result` VARCHAR(10) NOT NULL,
  `date_advised` VARCHAR(200) NOT NULL,
  `notes` LONGTEXT NOT NULL,
  `price` DOUBLE NOT NULL,
  `labor` DOUBLE NOT NULL,
  `parts` DOUBLE NOT NULL,
  `price_unit` VARCHAR(10) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `dea_appointment_id_272b7dfa40b689e0_fk_dealership_appointment_id` (`appointment_id` ASC),
  INDEX `deale_service_id_2fd6058781d06f7a_fk_dealership_servicerepair_id` (`service_id` ASC),
  INDEX `dealership_appoin_technician_id_5e04a89223a990b8_fk_auth_user_id` (`technician_id` ASC),
  INDEX `dealership_appointmentrecommendation_f08a49e5` (`recommnded_by_id` ASC),
  CONSTRAINT `deale_service_id_2fd6058781d06f7a_fk_dealership_servicerepair_id`
    FOREIGN KEY (`service_id`)
    REFERENCES `greenlight`.`dealership_servicerepair` (`id`),
  CONSTRAINT `dealership_app_recommnded_by_id_16e2f434f75e3ad1_fk_auth_user_id`
    FOREIGN KEY (`recommnded_by_id`)
    REFERENCES `greenlight`.`auth_user` (`id`),
  CONSTRAINT `dealership_appoin_technician_id_5e04a89223a990b8_fk_auth_user_id`
    FOREIGN KEY (`technician_id`)
    REFERENCES `greenlight`.`auth_user` (`id`),
  CONSTRAINT `dealership_appointmentrecommendation_ibfk_1`
    FOREIGN KEY (`appointment_id`)
    REFERENCES `greenlight`.`dealership_appointment` (`id`)
    ON DELETE CASCADE)
ENGINE = InnoDB
AUTO_INCREMENT = 28
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `greenlight`.`dealership_appointmentservice`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `greenlight`.`dealership_appointmentservice` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `appointment_id` INT(11) NOT NULL,
  `service_id` INT(11) NOT NULL,
  `technician_id` INT(11) NULL DEFAULT NULL,
  `note` LONGTEXT NULL DEFAULT NULL,
  `price` FLOAT(11,2) NULL DEFAULT '0.00',
  `desc` LONGTEXT NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `dealership_appointmentservice_54c91d3b` (`appointment_id` ASC),
  INDEX `dealership_appointmentservice_b0dc1e29` (`service_id` ASC),
  INDEX `dealership_appointmentservice_4e976f27` (`technician_id` ASC),
  CONSTRAINT `deale_service_id_1538306364b31a91_fk_dealership_servicerepair_id`
    FOREIGN KEY (`service_id`)
    REFERENCES `greenlight`.`dealership_servicerepair` (`id`),
  CONSTRAINT `dealership_appoin_technician_id_21e84448eaca58bd_fk_auth_user_id`
    FOREIGN KEY (`technician_id`)
    REFERENCES `greenlight`.`auth_user` (`id`),
  CONSTRAINT `dealership_appointmentservice_ibfk_1`
    FOREIGN KEY (`appointment_id`)
    REFERENCES `greenlight`.`dealership_appointment` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
AUTO_INCREMENT = 7814
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `greenlight`.`dealership_bmwresourcelink`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `greenlight`.`dealership_bmwresourcelink` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(250) NULL DEFAULT NULL,
  `rank` VARCHAR(250) NULL DEFAULT NULL,
  `url` VARCHAR(250) NULL DEFAULT NULL,
  `shop_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `dealership_bmwresourcelink_9f32e8f6` (`shop_id` ASC),
  CONSTRAINT `dealership_bmwr_shop_id_78e01bac0159fc4e_fk_dealership_dealer_id`
    FOREIGN KEY (`shop_id`)
    REFERENCES `greenlight`.`dealership_dealer` (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 2
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `greenlight`.`dealership_capacitycounts`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `greenlight`.`dealership_capacitycounts` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `total_tech` INT(11) NOT NULL,
  `time_slab` DATETIME NULL DEFAULT NULL,
  `dealer_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `dealership_capacitycounts_fad77b25` (`dealer_id` ASC),
  CONSTRAINT `dealership_ca_dealer_id_694a914ec59a5efa_fk_dealership_dealer_id`
    FOREIGN KEY (`dealer_id`)
    REFERENCES `greenlight`.`dealership_dealer` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `greenlight`.`dealership_creditcardinfo`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `greenlight`.`dealership_creditcardinfo` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `date_added` DATE NULL DEFAULT NULL,
  `first_name` VARCHAR(255) NULL DEFAULT NULL,
  `last_name` VARCHAR(255) NULL DEFAULT NULL,
  `card_number` VARCHAR(255) NULL DEFAULT NULL,
  `card_type` VARCHAR(255) NULL DEFAULT NULL,
  `card_exp_year` VARCHAR(255) NULL DEFAULT NULL,
  `card_exp_month` VARCHAR(255) NULL DEFAULT NULL,
  `user_id` INT(11) NULL DEFAULT NULL,
  `card_ver_number` VARCHAR(255) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `user_id` (`user_id` ASC))
ENGINE = InnoDB
AUTO_INCREMENT = 2850
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `greenlight`.`dealership_customeradvisor`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `greenlight`.`dealership_customeradvisor` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `advisor_id` INT(11) NOT NULL,
  `customer_id` INT(11) NOT NULL,
  `dealer_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `dealership_customera_advisor_id_3ae00a2e80bbde5b_fk_auth_user_id` (`advisor_id` ASC),
  INDEX `dealership_customeradvisor_cb24373b` (`customer_id` ASC),
  INDEX `dealership_customeradvisor_fad77b25` (`dealer_id` ASC),
  CONSTRAINT `dealer_customer_id_7d0f9e557e729890_fk_dealership_userprofile_id`
    FOREIGN KEY (`customer_id`)
    REFERENCES `greenlight`.`dealership_userprofile` (`id`),
  CONSTRAINT `dealership_cu_dealer_id_68e430efd4fce769_fk_dealership_dealer_id`
    FOREIGN KEY (`dealer_id`)
    REFERENCES `greenlight`.`dealership_dealer` (`id`),
  CONSTRAINT `dealership_customera_advisor_id_3ae00a2e80bbde5b_fk_auth_user_id`
    FOREIGN KEY (`advisor_id`)
    REFERENCES `greenlight`.`auth_user` (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 14
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `greenlight`.`dealership_dealercapacity`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `greenlight`.`dealership_dealercapacity` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `dealer_id` INT(11) NOT NULL,
  `timeslot` DATETIME NOT NULL,
  `available_technician` INT(11) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `greenlight`.`dealership_favorites`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `greenlight`.`dealership_favorites` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(250) NOT NULL,
  `url_name` VARCHAR(250) NULL DEFAULT NULL,
  `url_qstring` VARCHAR(500) NULL DEFAULT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 12
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `greenlight`.`dealership_dealerfavorites`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `greenlight`.`dealership_dealerfavorites` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `dealer_id` INT(11) NOT NULL,
  `favorites_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `dealership_de_dealer_id_4971a8b2fb62d323_fk_dealership_dealer_id` (`dealer_id` ASC),
  INDEX `dealership_dealerfavorites_1fe2d476` (`favorites_id` ASC),
  CONSTRAINT `dealers_favorites_id_6b2b24451f2d9400_fk_dealership_favorites_id`
    FOREIGN KEY (`favorites_id`)
    REFERENCES `greenlight`.`dealership_favorites` (`id`),
  CONSTRAINT `dealership_de_dealer_id_4971a8b2fb62d323_fk_dealership_dealer_id`
    FOREIGN KEY (`dealer_id`)
    REFERENCES `greenlight`.`dealership_dealer` (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 4
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `greenlight`.`dealership_dealersvehicle`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `greenlight`.`dealership_dealersvehicle` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `created_at` DATETIME NOT NULL,
  `created_by_id` INT(11) NULL DEFAULT NULL,
  `dealer_id` INT(11) NOT NULL,
  `vehicle_id` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `dealership_dealer_created_by_id_3b9a6b7d6ee351a5_fk_auth_user_id` (`created_by_id` ASC),
  INDEX `dealership_de_dealer_id_2b1e519aa68b419c_fk_dealership_dealer_id` (`dealer_id` ASC),
  INDEX `dealership__vehicle_id_4f4a73f19b448256_fk_dealership_vehicle_id` (`vehicle_id` ASC),
  CONSTRAINT `dealership__vehicle_id_4f4a73f19b448256_fk_dealership_vehicle_id`
    FOREIGN KEY (`vehicle_id`)
    REFERENCES `greenlight`.`dealership_vehicle` (`id`),
  CONSTRAINT `dealership_de_dealer_id_2b1e519aa68b419c_fk_dealership_dealer_id`
    FOREIGN KEY (`dealer_id`)
    REFERENCES `greenlight`.`dealership_dealer` (`id`),
  CONSTRAINT `dealership_dealer_created_by_id_3b9a6b7d6ee351a5_fk_auth_user_id`
    FOREIGN KEY (`created_by_id`)
    REFERENCES `greenlight`.`auth_user` (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 4186
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `greenlight`.`dealership_driverliscenseisurance`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `greenlight`.`dealership_driverliscenseisurance` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `driver_liscens_number` VARCHAR(255) NULL DEFAULT NULL,
  `insurance_company_name` VARCHAR(255) NULL DEFAULT NULL,
  `insurance_card_number` VARCHAR(255) NULL DEFAULT NULL,
  `date_added` DATE NULL DEFAULT NULL,
  `state_id` INT(11) NULL DEFAULT NULL,
  `user_id` INT(11) NULL DEFAULT NULL,
  `ins_exp_month` VARCHAR(255) NULL DEFAULT NULL,
  `ins_exp_year` VARCHAR(255) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `user_id` (`user_id` ASC),
  INDEX `dealership_dri_state_id_12babeb0424516af_fk_dealership_states_id` (`state_id` ASC),
  CONSTRAINT `dealership_dri_state_id_12babeb0424516af_fk_dealership_states_id`
    FOREIGN KEY (`state_id`)
    REFERENCES `greenlight`.`dealership_states` (`id`),
  CONSTRAINT `dealership_driverliscenseisurance_ibfk_1`
    FOREIGN KEY (`user_id`)
    REFERENCES `greenlight`.`dealership_userprofile` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
AUTO_INCREMENT = 2849
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `greenlight`.`dealership_emailmultimedia`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `greenlight`.`dealership_emailmultimedia` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `multimedia_file` VARCHAR(255) NOT NULL,
  `dealer_id` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `dealership_em_dealer_id_7d9fc46174c0afc8_fk_dealership_dealer_id` (`dealer_id` ASC),
  CONSTRAINT `dealership_em_dealer_id_7d9fc46174c0afc8_fk_dealership_dealer_id`
    FOREIGN KEY (`dealer_id`)
    REFERENCES `greenlight`.`dealership_dealer` (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 12
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `greenlight`.`dealership_emailtypes`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `greenlight`.`dealership_emailtypes` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) NOT NULL,
  `template` LONGTEXT NOT NULL,
  `html_template` LONGTEXT NULL DEFAULT NULL,
  `subject` LONGTEXT NULL DEFAULT NULL,
  `signature` LONGTEXT NULL DEFAULT NULL,
  `dealer_id` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `dealership_emailtypes_fad77b25` (`dealer_id` ASC),
  CONSTRAINT `dealership_em_dealer_id_1671ddcc234bfbab_fk_dealership_dealer_id`
    FOREIGN KEY (`dealer_id`)
    REFERENCES `greenlight`.`dealership_dealer` (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 19
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `greenlight`.`dealership_emailqueue`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `greenlight`.`dealership_emailqueue` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `created_time` DATETIME NULL DEFAULT NULL,
  `subject` VARCHAR(255) NOT NULL,
  `cc` LONGTEXT NULL DEFAULT NULL,
  `bcc` LONGTEXT NULL DEFAULT NULL,
  `params` LONGTEXT NULL DEFAULT NULL,
  `status` INT(11) NOT NULL,
  `mail_time` DATETIME NULL DEFAULT NULL,
  `sent_time` DATETIME NULL DEFAULT NULL,
  `mail_error` INT(11) NOT NULL,
  `mail_retries` INT(11) NOT NULL,
  `mail_failuire_date` DATETIME NULL DEFAULT NULL,
  `mail_detail` LONGTEXT NULL DEFAULT NULL,
  `mail_from` LONGTEXT NOT NULL,
  `mail_to` LONGTEXT NOT NULL,
  `mail_error_detail` LONGTEXT NULL DEFAULT NULL,
  `type_id` INT(11) NOT NULL,
  `dealer_id` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `api_applica_type_id_6ddc9c9e7270bd97_fk_dealership_emailtypes_id` (`type_id` ASC),
  INDEX `dealership_emailqueue_fad77b25` (`dealer_id` ASC),
  CONSTRAINT `api_applica_type_id_6ddc9c9e7270bd97_fk_dealership_emailtypes_id`
    FOREIGN KEY (`type_id`)
    REFERENCES `greenlight`.`dealership_emailtypes` (`id`),
  CONSTRAINT `dealership_em_dealer_id_12c8954c7f1b3213_fk_dealership_dealer_id`
    FOREIGN KEY (`dealer_id`)
    REFERENCES `greenlight`.`dealership_dealer` (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 1540
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `greenlight`.`dealership_flagshistory`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `greenlight`.`dealership_flagshistory` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `notes` VARCHAR(255) NOT NULL,
  `created_at` DATETIME NOT NULL,
  `created_by_id` INT(11) NULL DEFAULT NULL,
  `flag_id` INT(11) NOT NULL,
  `ro_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `dealership_flagsh_created_by_id_6bc562b2ff67fd14_fk_auth_user_id` (`created_by_id` ASC),
  INDEX `dealership_flagshistory_6b31b6fd` (`flag_id` ASC),
  INDEX `dealership_flagshistory_cfad0df9` (`ro_id` ASC),
  CONSTRAINT `dealership_flags_flag_id_170f1605f6f9b84c_fk_dealership_flags_id`
    FOREIGN KEY (`flag_id`)
    REFERENCES `greenlight`.`dealership_flags` (`id`),
  CONSTRAINT `dealership_flagsh_created_by_id_6bc562b2ff67fd14_fk_auth_user_id`
    FOREIGN KEY (`created_by_id`)
    REFERENCES `greenlight`.`auth_user` (`id`),
  CONSTRAINT `dealership_flagshistory_ibfk_1`
    FOREIGN KEY (`ro_id`)
    REFERENCES `greenlight`.`dealership_ro` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
AUTO_INCREMENT = 526
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `greenlight`.`dealership_inspectioncatagories`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `greenlight`.`dealership_inspectioncatagories` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `category` VARCHAR(50) NOT NULL,
  `package_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 65
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `greenlight`.`dealership_inspectioncategoriesitems`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `greenlight`.`dealership_inspectioncategoriesitems` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `category_id` INT(11) NOT NULL,
  `item_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `dealership_inspectioncategoriesitems_b583a629` (`category_id` ASC))
ENGINE = InnoDB
AUTO_INCREMENT = 232
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `greenlight`.`dealership_inspectionitems`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `greenlight`.`dealership_inspectionitems` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `item` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 197
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `greenlight`.`dealership_inspectionpackage`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `greenlight`.`dealership_inspectionpackage` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `package` VARCHAR(255) NOT NULL,
  `dealer_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `dealership_inspectionpackage_fad77b25` (`dealer_id` ASC),
  CONSTRAINT `dealership_in_dealer_id_16b6593b06fa8407_fk_dealership_dealer_id`
    FOREIGN KEY (`dealer_id`)
    REFERENCES `greenlight`.`dealership_dealer` (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 19
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `greenlight`.`dealership_inspectionpackageitems`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `greenlight`.`dealership_inspectionpackageitems` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `greenlight`.`dealership_notes`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `greenlight`.`dealership_notes` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `comment` VARCHAR(10240) NOT NULL,
  `created_at` DATETIME NOT NULL,
  `created_by_id` INT(11) NOT NULL,
  `ro_id` INT(11) NOT NULL,
  `current_flag` VARCHAR(10) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `deal_created_by_id_66c44b2749bc7a25_fk_dealership_userprofile_id` (`created_by_id` ASC),
  INDEX `dealership_notes_ro_id_5ddd4b96386b3a6_fk_dealership_ro_id` (`ro_id` ASC),
  CONSTRAINT `deal_created_by_id_66c44b2749bc7a25_fk_dealership_userprofile_id`
    FOREIGN KEY (`created_by_id`)
    REFERENCES `greenlight`.`dealership_userprofile` (`id`),
  CONSTRAINT `dealership_notes_ro_id_5ddd4b96386b3a6_fk_dealership_ro_id`
    FOREIGN KEY (`ro_id`)
    REFERENCES `greenlight`.`dealership_ro` (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 63
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `greenlight`.`dealership_remindertype`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `greenlight`.`dealership_remindertype` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(100) NOT NULL,
  `description` VARCHAR(20000) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 5
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `greenlight`.`dealership_remindersettings`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `greenlight`.`dealership_remindersettings` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `email` TINYINT(1) NOT NULL,
  `text` TINYINT(1) NOT NULL,
  `phone` TINYINT(1) NOT NULL,
  `customer_id` INT(11) NOT NULL,
  `type_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `dealer_customer_id_3b20e5bac36d0880_fk_dealership_userprofile_id` (`customer_id` ASC),
  INDEX `dealership_remindersettings_94757cae` (`type_id` ASC),
  CONSTRAINT `dealershi_type_id_15bfde2d447683fc_fk_dealership_remindertype_id`
    FOREIGN KEY (`type_id`)
    REFERENCES `greenlight`.`dealership_remindertype` (`id`),
  CONSTRAINT `dealership_remindersettings_ibfk_1`
    FOREIGN KEY (`customer_id`)
    REFERENCES `greenlight`.`dealership_userprofile` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
AUTO_INCREMENT = 1079
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `greenlight`.`dealership_roinspection`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `greenlight`.`dealership_roinspection` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `observation` VARCHAR(100) NULL DEFAULT NULL,
  `recommendations` VARCHAR(100) NULL DEFAULT NULL,
  `specs` VARCHAR(100) NULL DEFAULT NULL,
  `image` VARCHAR(100) NULL DEFAULT NULL,
  `status` VARCHAR(10) NOT NULL,
  `inspection_id` INT(11) NOT NULL,
  `ro_id` INT(11) NOT NULL,
  `inspector_id` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `D8c53e1c6c6883ef257f23ef1448733a` (`inspection_id` ASC),
  INDEX `dealership_roinspecti_ro_id_5824c3c65c994733_fk_dealership_ro_id` (`ro_id` ASC),
  INDEX `dealership_roinspection_39ffa7b5` (`inspector_id` ASC),
  CONSTRAINT `dealership_roinspe_inspector_id_631063775cb20fdc_fk_auth_user_id`
    FOREIGN KEY (`inspector_id`)
    REFERENCES `greenlight`.`auth_user` (`id`),
  CONSTRAINT `dealership_roinspection_ibfk_1`
    FOREIGN KEY (`ro_id`)
    REFERENCES `greenlight`.`dealership_ro` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `e96b3d38a08d1313a86a143d4aa7e43a`
    FOREIGN KEY (`inspection_id`)
    REFERENCES `greenlight`.`dealership_inspectioncategoriesitems` (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 1857
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `greenlight`.`dealership_shopamenities`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `greenlight`.`dealership_shopamenities` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `amenities_id` INT(11) NOT NULL,
  `shop_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `dealers_amenities_id_2c37d0c95ec210e7_fk_dealership_amenities_id` (`amenities_id` ASC),
  INDEX `dealership_shopamenities_9f32e8f6` (`shop_id` ASC),
  CONSTRAINT `dealers_amenities_id_2c37d0c95ec210e7_fk_dealership_amenities_id`
    FOREIGN KEY (`amenities_id`)
    REFERENCES `greenlight`.`dealership_amenities` (`id`),
  CONSTRAINT `dealership_shop_shop_id_6210b26a2e6e3a81_fk_dealership_dealer_id`
    FOREIGN KEY (`shop_id`)
    REFERENCES `greenlight`.`dealership_dealer` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `greenlight`.`dealership_shopholidays`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `greenlight`.`dealership_shopholidays` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `description` LONGTEXT NULL DEFAULT NULL,
  `date` DATE NULL DEFAULT NULL,
  `shop_id` INT(11) NOT NULL,
  `updated_date` DATETIME NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `dealership_shopholidays_9f32e8f6` (`shop_id` ASC),
  CONSTRAINT `dealership_shop_shop_id_31c0608cbea2bb4a_fk_dealership_dealer_id`
    FOREIGN KEY (`shop_id`)
    REFERENCES `greenlight`.`dealership_dealer` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `greenlight`.`dealership_shophours`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `greenlight`.`dealership_shophours` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `day` VARCHAR(250) NULL DEFAULT NULL,
  `time_from` TIME NULL DEFAULT NULL,
  `time_to` TIME NULL DEFAULT NULL,
  `shop_id` INT(11) NOT NULL,
  `updated_date` DATETIME NULL DEFAULT NULL,
  `capacity_percent` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `dealership_shophours_9f32e8f6` (`shop_id` ASC),
  CONSTRAINT `dealership_shop_shop_id_6035cf0800c2f843_fk_dealership_dealer_id`
    FOREIGN KEY (`shop_id`)
    REFERENCES `greenlight`.`dealership_dealer` (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 12
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `greenlight`.`dealership_shopotheremails`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `greenlight`.`dealership_shopotheremails` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `email` LONGTEXT NULL DEFAULT NULL,
  `type` LONGTEXT NULL DEFAULT NULL,
  `updated_date` DATETIME NULL DEFAULT NULL,
  `shop_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `dealership_shop_shop_id_1e6a9acbd0e96b61_fk_dealership_dealer_id` (`shop_id` ASC),
  CONSTRAINT `dealership_shop_shop_id_1e6a9acbd0e96b61_fk_dealership_dealer_id`
    FOREIGN KEY (`shop_id`)
    REFERENCES `greenlight`.`dealership_dealer` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `greenlight`.`dealership_shopscontact`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `greenlight`.`dealership_shopscontact` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` LONGTEXT NULL DEFAULT NULL,
  `email` LONGTEXT NULL DEFAULT NULL,
  `phone_work` LONGTEXT NULL DEFAULT NULL,
  `phone_cell` LONGTEXT NULL DEFAULT NULL,
  `shop_id` INT(11) NOT NULL,
  `updated_date` DATETIME NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `dealership_shopscontact_9f32e8f6` (`shop_id` ASC),
  CONSTRAINT `dealership_shop_shop_id_4f667856731d1874_fk_dealership_dealer_id`
    FOREIGN KEY (`shop_id`)
    REFERENCES `greenlight`.`dealership_dealer` (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 5
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `greenlight`.`dealership_shopsms`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `greenlight`.`dealership_shopsms` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `sms_no` LONGTEXT NULL DEFAULT NULL,
  `update_date` DATETIME NULL DEFAULT NULL,
  `shop_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `dealership_shop_shop_id_420416361617cbf7_fk_dealership_dealer_id` (`shop_id` ASC),
  CONSTRAINT `dealership_shop_shop_id_420416361617cbf7_fk_dealership_dealer_id`
    FOREIGN KEY (`shop_id`)
    REFERENCES `greenlight`.`dealership_dealer` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `greenlight`.`dealership_team`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `greenlight`.`dealership_team` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(100) NULL DEFAULT NULL,
  `created_at` DATETIME NOT NULL,
  `created_by_id` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `dealership_team_created_by_id_525661f1b23ee9d8_fk_auth_user_id` (`created_by_id` ASC),
  CONSTRAINT `dealership_team_created_by_id_525661f1b23ee9d8_fk_auth_user_id`
    FOREIGN KEY (`created_by_id`)
    REFERENCES `greenlight`.`auth_user` (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 5
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `greenlight`.`dealership_teamadvisors`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `greenlight`.`dealership_teamadvisors` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `created_at` DATETIME NOT NULL,
  `advisor_id` INT(11) NOT NULL,
  `created_by_id` INT(11) NULL DEFAULT NULL,
  `team_id_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `dealership_teamad_created_by_id_7dc818a8f5af8a99_fk_auth_user_id` (`created_by_id` ASC),
  INDEX `dealership_tea_team_id_id_5b564bb50d0538b5_fk_dealership_team_id` (`team_id_id` ASC),
  INDEX `dealership_teamadvis_advisor_id_74feafb07b155066_fk_auth_user_id` (`advisor_id` ASC),
  CONSTRAINT `dealership_tea_team_id_id_5b564bb50d0538b5_fk_dealership_team_id`
    FOREIGN KEY (`team_id_id`)
    REFERENCES `greenlight`.`dealership_team` (`id`),
  CONSTRAINT `dealership_teamad_created_by_id_7dc818a8f5af8a99_fk_auth_user_id`
    FOREIGN KEY (`created_by_id`)
    REFERENCES `greenlight`.`auth_user` (`id`),
  CONSTRAINT `dealership_teamadvis_advisor_id_74feafb07b155066_fk_auth_user_id`
    FOREIGN KEY (`advisor_id`)
    REFERENCES `greenlight`.`auth_user` (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 5
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `greenlight`.`dealership_timezones`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `greenlight`.`dealership_timezones` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(250) NULL DEFAULT NULL,
  `timezone` VARCHAR(250) NULL DEFAULT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 3
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `greenlight`.`dealership_userdealer`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `greenlight`.`dealership_userdealer` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `dealer_id` INT(11) NOT NULL,
  `user_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `dealership_us_dealer_id_376c7f5a48c685bc_fk_dealership_dealer_id` (`dealer_id` ASC),
  INDEX `dealership_userdealer_user_id_6625859c187e0744_fk_auth_user_id` (`user_id` ASC),
  CONSTRAINT `dealership_us_dealer_id_376c7f5a48c685bc_fk_dealership_dealer_id`
    FOREIGN KEY (`dealer_id`)
    REFERENCES `greenlight`.`dealership_dealer` (`id`),
  CONSTRAINT `dealership_userdealer_user_id_6625859c187e0744_fk_auth_user_id`
    FOREIGN KEY (`user_id`)
    REFERENCES `greenlight`.`auth_user` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `greenlight`.`dealership_vehicleimages`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `greenlight`.`dealership_vehicleimages` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `image` VARCHAR(100) NULL DEFAULT NULL,
  `default` TINYINT(1) NOT NULL,
  `vehicle_id` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `dealership__vehicle_id_4780ae883f7ef121_fk_dealership_vehicle_id` (`vehicle_id` ASC),
  CONSTRAINT `dealership__vehicle_id_4780ae883f7ef121_fk_dealership_vehicle_id`
    FOREIGN KEY (`vehicle_id`)
    REFERENCES `greenlight`.`dealership_vehicle` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `greenlight`.`dealership_vehicleparts`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `greenlight`.`dealership_vehicleparts` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `part_name` VARCHAR(255) NULL DEFAULT NULL,
  `vehicle_id` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `dealership__vehicle_id_4e9c7c91c4b57754_fk_dealership_vehicle_id` (`vehicle_id` ASC),
  CONSTRAINT `dealership__vehicle_id_4e9c7c91c4b57754_fk_dealership_vehicle_id`
    FOREIGN KEY (`vehicle_id`)
    REFERENCES `greenlight`.`dealership_vehicle` (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 21
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `greenlight`.`dealership_vehicletirewidth`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `greenlight`.`dealership_vehicletirewidth` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `width` VARCHAR(255) NULL DEFAULT NULL,
  `vehicle_id` INT(11) NULL DEFAULT NULL,
  `type` VARCHAR(2) NULL DEFAULT NULL,
  `safe` TINYINT(1) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `dealership__vehicle_id_4c5c1508e6384558_fk_dealership_vehicle_id` (`vehicle_id` ASC),
  CONSTRAINT `dealership__vehicle_id_4c5c1508e6384558_fk_dealership_vehicle_id`
    FOREIGN KEY (`vehicle_id`)
    REFERENCES `greenlight`.`dealership_vehicle` (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 111
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `greenlight`.`dealership_vintrim`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `greenlight`.`dealership_vintrim` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(200) NOT NULL,
  `val` VARCHAR(200) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 16386
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `greenlight`.`dealership_walkaroundinitials`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `greenlight`.`dealership_walkaroundinitials` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `type` VARCHAR(255) NULL DEFAULT NULL,
  `initials` LONGTEXT NOT NULL,
  `appointment_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `dea_appointment_id_32b4d8514eb8f69c_fk_dealership_appointment_id` (`appointment_id` ASC),
  CONSTRAINT `dealership_walkaroundinitials_ibfk_1`
    FOREIGN KEY (`appointment_id`)
    REFERENCES `greenlight`.`dealership_appointment` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
AUTO_INCREMENT = 53
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `greenlight`.`dealership_walkaroundnotes`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `greenlight`.`dealership_walkaroundnotes` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `type` VARCHAR(255) NULL DEFAULT NULL,
  `notes` LONGTEXT NULL DEFAULT NULL,
  `appointment_id` INT(11) NOT NULL,
  `image` VARCHAR(100) NULL DEFAULT NULL,
  `image_name` VARCHAR(255) NULL DEFAULT NULL,
  `LF_id` INT(11) NULL DEFAULT NULL,
  `LR_id` INT(11) NULL DEFAULT NULL,
  `RF_id` INT(11) NULL DEFAULT NULL,
  `RR_id` INT(11) NULL DEFAULT NULL,
  `other_category` VARCHAR(255) NULL DEFAULT NULL,
  `other_type` VARCHAR(255) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `appointment_id` (`appointment_id` ASC),
  INDEX `dealership_walkaroundnotes_LF_id_508b1300c6687c51_uniq` (`LF_id` ASC),
  INDEX `dealership_walkaroundnotes_LR_id_375795642dc7973_uniq` (`LR_id` ASC),
  INDEX `dealership_walkaroundnotes_RF_id_720b66dca2860911_uniq` (`RF_id` ASC),
  INDEX `dealership_walkaroundnotes_RR_id_59bd08f3c359203b_uniq` (`RR_id` ASC),
  CONSTRAINT `dealers_LF_id_508b1300c6687c51_fk_dealership_vehicletirewidth_id`
    FOREIGN KEY (`LF_id`)
    REFERENCES `greenlight`.`dealership_vehicletirewidth` (`id`),
  CONSTRAINT `dealers_RF_id_720b66dca2860911_fk_dealership_vehicletirewidth_id`
    FOREIGN KEY (`RF_id`)
    REFERENCES `greenlight`.`dealership_vehicletirewidth` (`id`),
  CONSTRAINT `dealers_RR_id_59bd08f3c359203b_fk_dealership_vehicletirewidth_id`
    FOREIGN KEY (`RR_id`)
    REFERENCES `greenlight`.`dealership_vehicletirewidth` (`id`),
  CONSTRAINT `dealersh_LR_id_375795642dc7973_fk_dealership_vehicletirewidth_id`
    FOREIGN KEY (`LR_id`)
    REFERENCES `greenlight`.`dealership_vehicletirewidth` (`id`),
  CONSTRAINT `dealership_walkaroundnotes_ibfk_1`
    FOREIGN KEY (`appointment_id`)
    REFERENCES `greenlight`.`dealership_appointment` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
AUTO_INCREMENT = 257
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `greenlight`.`dealership_walkaroundvehicleimage`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `greenlight`.`dealership_walkaroundvehicleimage` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `image` VARCHAR(100) NULL DEFAULT NULL,
  `vehicle_id` INT(11) NULL DEFAULT NULL,
  `dealer_id` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `dealership__vehicle_id_3e222add7931bca7_fk_dealership_vehicle_id` (`vehicle_id` ASC),
  CONSTRAINT `dealership__vehicle_id_3e222add7931bca7_fk_dealership_vehicle_id`
    FOREIGN KEY (`vehicle_id`)
    REFERENCES `greenlight`.`dealership_vehicle` (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 21
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `greenlight`.`dealership_walkaroundvehiclemap`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `greenlight`.`dealership_walkaroundvehiclemap` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `type` VARCHAR(255) NULL DEFAULT NULL,
  `coords` LONGTEXT NULL DEFAULT NULL,
  `vehicleimage_id` INT(11) NOT NULL,
  `name` VARCHAR(255) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `b5d853d46f6c054b8034986546995d3b` (`vehicleimage_id` ASC),
  CONSTRAINT `ad4239d56065a75feb305667c1bcbbac`
    FOREIGN KEY (`vehicleimage_id`)
    REFERENCES `greenlight`.`dealership_walkaroundvehicleimage` (`id`),
  CONSTRAINT `b5d853d46f6c054b8034986546995d3b`
    FOREIGN KEY (`vehicleimage_id`)
    REFERENCES `greenlight`.`dealership_walkaroundvehicleimage` (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 144
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `greenlight`.`dealership_wayawaydealer`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `greenlight`.`dealership_wayawaydealer` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `description` VARCHAR(2000) NULL DEFAULT NULL,
  `dealer_id` INT(11) NOT NULL,
  `wayaway_id` INT(11) NOT NULL,
  `popup_description` VARCHAR(2000) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `dealership_wa_dealer_id_331a9de0554f70cb_fk_dealership_dealer_id` (`dealer_id` ASC),
  INDEX `dealership_wayawaydealer_e04e121d` (`wayaway_id` ASC),
  CONSTRAINT `dealership__wayaway_id_1af7bb24de9abdd2_fk_dealership_wayaway_id`
    FOREIGN KEY (`wayaway_id`)
    REFERENCES `greenlight`.`dealership_wayaway` (`id`),
  CONSTRAINT `dealership_wa_dealer_id_331a9de0554f70cb_fk_dealership_dealer_id`
    FOREIGN KEY (`dealer_id`)
    REFERENCES `greenlight`.`dealership_dealer` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `greenlight`.`django_admin_log`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `greenlight`.`django_admin_log` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `action_time` DATETIME NOT NULL,
  `object_id` LONGTEXT NULL DEFAULT NULL,
  `object_repr` VARCHAR(200) NOT NULL,
  `action_flag` SMALLINT(5) UNSIGNED NOT NULL,
  `change_message` LONGTEXT NOT NULL,
  `content_type_id` INT(11) NULL DEFAULT NULL,
  `user_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `djang_content_type_id_697914295151027a_fk_django_content_type_id` (`content_type_id` ASC),
  INDEX `django_admin_log_user_id_52fdd58701c5f563_fk_auth_user_id` (`user_id` ASC),
  CONSTRAINT `djang_content_type_id_697914295151027a_fk_django_content_type_id`
    FOREIGN KEY (`content_type_id`)
    REFERENCES `greenlight`.`django_content_type` (`id`),
  CONSTRAINT `django_admin_log_user_id_52fdd58701c5f563_fk_auth_user_id`
    FOREIGN KEY (`user_id`)
    REFERENCES `greenlight`.`auth_user` (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 302
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `greenlight`.`django_migrations`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `greenlight`.`django_migrations` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `app` VARCHAR(255) NOT NULL,
  `name` VARCHAR(255) NOT NULL,
  `applied` DATETIME NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 254
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `greenlight`.`django_session`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `greenlight`.`django_session` (
  `session_key` VARCHAR(40) NOT NULL,
  `session_data` LONGTEXT NOT NULL,
  `expire_date` DATETIME NOT NULL,
  PRIMARY KEY (`session_key`),
  INDEX `django_session_de54fa62` (`expire_date` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `greenlight`.`django_site`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `greenlight`.`django_site` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `domain` VARCHAR(100) NOT NULL,
  `name` VARCHAR(50) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 2
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `greenlight`.`livechat_channels`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `greenlight`.`livechat_channels` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `channel` VARCHAR(2000) NOT NULL,
  `guest_user` VARCHAR(2000) NOT NULL,
  `chat_dt` DATETIME NOT NULL,
  `advisor_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `livechat_channels_advisor_id_74b0270fd38d1d59_fk_auth_user_id` (`advisor_id` ASC),
  CONSTRAINT `livechat_channels_advisor_id_74b0270fd38d1d59_fk_auth_user_id`
    FOREIGN KEY (`advisor_id`)
    REFERENCES `greenlight`.`auth_user` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `greenlight`.`livechat_upload`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `greenlight`.`livechat_upload` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `pic` VARCHAR(100) NOT NULL,
  `upload_date` DATETIME NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 41
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `greenlight`.`yrmakemodel_yearmakemodel`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `greenlight`.`yrmakemodel_yearmakemodel` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `model_make_name` VARCHAR(100) NOT NULL,
  `model_name` VARCHAR(100) NOT NULL,
  `model_trim` VARCHAR(100) NOT NULL,
  `model_year` INT(11) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 71344
DEFAULT CHARACTER SET = latin1;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

