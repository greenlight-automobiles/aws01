#!/bin/bash

# ===================
# author: Rupesh Shah
# ===================

# ====================
# service stop
# ====================
sudo apachectl start

# another way to start
# sudo service httpd start
