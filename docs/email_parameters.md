# Customers can use these parameters in an email when creating custom templates.

Customer Name : {{name}}
Dealer table ID: {{dealer_id}}
Dealer Name: {{dealer_name}}
Appointment Time: {{start_time}}
Email context link: {{ main_url }}
Extra Notes: {{notes}}
Vehicle flagged as; {{flag_name}}
Dealer Website: {{main_site_url}}
Dealer address: {{address}}
Dealer Address: {{dealer_address}}
Dealer number: {{number}}
Domain Link: {{domain}}
Change appointment url:{{appointment_change_url}}
Unsubscribe: {{notification_url}}

Service Object: {{obj}} 
Service Name: {{obj.name}} 
Customer Note: {{obj.note}}
Advisor Note: {{obj.desc}}
Service Cost: {{obj.cost}}
Total Cost: {{totalcost|floatformat:2}}

Loop through inspection recommendations
{% for obj in status %}
{{obj.name}}
{{obj.part_price}}
{{obj.labour_price}}
{{obj.price}}
{{obj.status}}


Events

status_url              "subject": "Status Check"
mobilecheckin_review    "subject": "Walkin Service Review"
appointment_cancel      "subject": "Appointment Cancelled"
appointment_schedules   "subject": "Service Appointment Scheduled"
flags_change            "subject": "Service Flag Updated"
car_wash                "subject": "Service Car at Car Wash"
approval_required       "subject": "Service Approval Required"
reminder                "subject": "Service Appointment Reminder"
recommendation_status_details    "subject": "Recommendation Status Details"
action_plan             "subject": "Action Plan"


Maps Url
"http://maps.google.com/?q={{address}}"
